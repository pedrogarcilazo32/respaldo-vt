const routesLoader = require('./src/routes');
const userController = require('./src/controllers/usersController');

module.exports = (app) => {
  routesLoader(app);
};

const express = require('express');
const bodyParser = require('body-parser');
const { getConnection } = require('./src/database/connection');
const init = require('./init');

const app = express();
const port = process.env.PORT || 3001;

app.listen(port, async () => {
  console.log('server running on port', port);

  const pool = await getConnection();
  if (pool) {
    console.log('DB connection stablished')
    init(app)
  } else {
    console.error('Fallo en la conexion');
  }
});

// use middleware to parse the request body
app.use(bodyParser.json({ limit: '15mb', extended: true }));
app.use(bodyParser.urlencoded({ extended: true }));

// allow cross-domain
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization, Language');
  next();
});

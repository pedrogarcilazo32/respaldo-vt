/****** Object:  StoredProcedure [dbo].[vt_sp_cat_categorias]    Script Date: 05/12/2022 09:35:10 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[vt_sp_cat_categorias]

@proceso int,@json VARCHAR (MAX) 
AS
	DECLARE 
		@total_elementos INT,
		@pagina INT,
		@marca_id INT, 
		@nombre VARCHAR(500),
		@volumen DECIMAL(10,2),
		@precio DECIMAL(10,2),
		@usuario_id INT,
		@id INT,
		@search VARCHAR(500),
		@f_registro_inicio VARCHAR(500),
		@f_registro_fin VARCHAR(500),
		@imageUrl VARCHAR(100)

	--OBTENER CATEGORIAS
	IF @proceso=2
	BEGIN
		
		SELECT
			id_categoria as id,
			nombre as v_nombre
		FROM 
			Categorias
		WHERE
			activo_opc=1
	END
	

	
	

	
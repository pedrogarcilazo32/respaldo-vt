/****** Object:  StoredProcedure [dbo].[vt_sp_ordenes]    Script Date: 12/12/2022 06:29:32 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[vt_sp_ordenes]

@proceso int,@json VARCHAR (MAX) 
AS
	DECLARE 
	
		@id_usuario INT,
		@estatus BIT,
		@idDetalleOrden INT,
		@idOrden INT
		--SOLICITAR ORDEN
	IF @proceso=2
		BEGIN
			SET @id_usuario=JSON_VALUE(@json, '$.id_usuario')
			BEGIN TRAN
			EXECUTE crearOrden @id_usuario
			COMMIT TRAN
		SELECT 
			r.idOrden,
			r.idDetalleOrden,
			r.nombre_producto,
			r.nombre_provedor,
			r.cantidad,
			r.id_estante,
			r.id_seccion,
			r.estatus,
			r.estaenBodega,
			r.id_usuario,
			o.fecha_inicio as fecha_registro
		FROM
			RutaPendiente r
		INNER JOIN 
			Ordenes o on r.idOrden=o.folio_venta
		WHERE 
			r.id_usuario=@id_usuario
		END
			--OBTENER ORDEN PENDIENTE
	IF @proceso=3
		BEGIN
			SET @id_usuario=JSON_VALUE(@json, '$.id_usuario')
			SELECT 
			r.idOrden,
			r.idDetalleOrden,
			r.nombre_producto,
			r.nombre_provedor,
			r.cantidad,
			r.id_estante,
			r.id_seccion,
			r.estatus,
			r.estaenBodega,
			r.id_usuario,
			o.fecha_inicio as fecha_registro
		FROM
			RutaPendiente r
		INNER JOIN 
			Ordenes o on r.idOrden=o.folio_venta
		WHERE 
			r.id_usuario=@id_usuario
		END
		--CAMBIAR ESTADO
	IF @proceso=4
		BEGIN
			SET @idDetalleOrden=JSON_VALUE(@json, '$.idDetalleOrden')
			SET @estatus=JSON_VALUE(@json, '$.estatus')
			UPDATE
				RutaPendiente
			SET
				estatus=@estatus
			WHERE
				idDetalleOrden=@idDetalleOrden
		END
			--FINALIZAR ORDEN
	IF @proceso=5
		BEGIN
			SET @idOrden=JSON_VALUE(@json, '$.idOrden')
		BEGIN TRAN
			BEGIN TRY
		--FINALIZO LA VENTA 
		UPDATE 
			Ventas
		SET
			id_estatus=3
		WHERE 
			folio_venta=@idOrden
		--ACTUALIZO LA ORDEN
		UPDATE
			Ordenes
		SET
			fecha_fin=GETDATE()
		WHERE
			folio_venta=@idOrden
		--MOVEMOS DE COMPROMETIDO A SALIDA
		UPDATE 
			Lotes
		SET 
			comprometido=comprometido-rp.cantidad,
			salida_pro=rp.cantidad
		 FROM 
			RutaPendiente rp
		INNER JOIN Estantes e on e.id_estante=rp.id_estante
		INNER JOIN Seccion s on s.id_estante=e.id_estante and s.num_seccion=rp.id_seccion
		INNER JOIN Lote_Seccion ls on ls.id_seccion=s.id_seccion
		INNER JOIN Lotes l on l.id_lote=ls.id_lote
		WHERE 
			rp.idOrden=@idOrden
			--MOVEMOS  A SALIDA LOS LOTES EN BODEGA
		UPDATE 
			Lotes
		SET 
			comprometido=comprometido-rs.cantidad,
			salida_pro=rs.cantidad
		FROM 
			RutaPendiente rs
		INNER JOIN DetalleRuta dr on dr.id_venta=rs.idOrden and dr.id_detalle_venta= rs.idDetalleOrden
		INNER JOIN Bodega b on b.id_lote=dr.id_lote
		INNER JOIN Lotes l on l.id_lote=b.id_lote
		WHERE 
			rs.estaEnBodega=1
		AND
			rs.idOrden=@idOrden

		--RESTAMOS UBICACION SECCION
		UPDATE 
			Lote_Seccion
		SET 
			cantidad=ls.cantidad-rp.cantidad
	 	 FROM
			RutaPendiente rp
		INNER JOIN Estantes e on e.id_estante=rp.id_estante
		INNER JOIN Seccion s on s.id_estante=e.id_estante and s.num_seccion=rp.id_seccion
		INNER JOIN Lote_Seccion ls on ls.id_seccion=s.id_seccion
		WHERE 
			rp.idOrden=@idOrden

		--RESTAMOS UBICACION BODEGA
		UPDATE 
			Bodega
		SET 
			cantidad=b.cantidad-rs.cantidad
		FROM 
			RutaPendiente rs
		INNER JOIN DetalleRuta dr on dr.id_venta=rs.idOrden and dr.id_detalle_venta= rs.idDetalleOrden
		INNER JOIN Bodega b on b.id_lote=dr.id_lote
		WHERE 
			rs.estaEnBodega=1
		AND
			rs.idOrden=@idOrden
		--Borramos la ruta
		DELETE
			RutaPendiente
		WHERE 
			idOrden=@idOrden

		DELETE
			DetalleRuta
		WHERE 
			id_venta=@idOrden
		COMMIT TRAN
			END TRY
			BEGIN CATCH
			ROLLBACK TRAN
			END CATCH
		END

		--OBTENER ORDENES FINALIZADAS
		IF @proceso=6
		BEGIN
		SET @id_usuario=JSON_VALUE(@json, '$.id_usuario')
			SELECT
			V.folio_venta,
			'Admin' as nombreEmpleado,
			O.fecha_inicio,
			O.fecha_fin
			FROM
			Ordenes O
			INNER JOIN Ventas V ON V.folio_venta = O.folio_venta
			WHERE 
			V.id_estatus = 3 AND O.id_usuario = @id_usuario

		END




	
	

	
/****** Object:  StoredProcedure [dbo].[vt_sp_cat_marcas]    Script Date: 05/12/2022 09:36:14 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[vt_sp_cat_marcas]

@proceso int, @json VARCHAR(MAX)
AS 
    DECLARE
        @total_elementos INT,
        @nombre VARCHAR(500),
        @f_registro_inicio VARCHAR(500),
        @f_registro_fin VARCHAR(500),
        @usuario_id INT,
        @id INT
--INSERTAR MARCAS
    IF @proceso = 1
    BEGIN
        SET @nombre=JSON_VALUE(@json, '$.v_nombre')
        SET @usuario_id=JSON_VALUE(@json, '$.i_usuario_id')

        BEGIN
            INSERT INTO Marcas VALUES(
            @nombre,
            GETDATE(),
            1
            )
        END
    END
--OBTENER MARCAS
    ELSE IF @proceso=2
    BEGIN

        SELECT
            id_marca as id,
            nombre as v_nombre,
			fecha_registro,
			activo_opc
        FROM 
            Marcas
        WHERE 
            activo_opc=1
    END
	
--ACTUALIZAR MARCAS
    ELSE IF @proceso = 3
    BEGIN
        SET @nombre=JSON_VALUE(@json, '$.v_nombre')
        SET @usuario_id=JSON_VALUE(@json, '$.i_usuario_id')
        SET @id=JSON_VALUE(@json, '$.id')

        UPDATE
            Marcas
        SET
            nombre=@nombre
        WHERE
            id_marca=@id
    END
--BORRAR MARCAS
    ELSE IF @proceso=4
    BEGIN

        SET @id = JSON_VALUE(@json, '$.id')

        UPDATE 
            Marcas
        SET 
            activo_opc=0
        WHERE
            id_marca=@id

    END
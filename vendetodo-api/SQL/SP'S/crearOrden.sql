/****** Object:  StoredProcedure [dbo].[crearOrden]    Script Date: 12/12/2022 06:29:47 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[crearOrden]

@id_usuario INT 
AS
DECLARE 
	@numeroProductos INT,
	@id_producto INT,
	@cantidad_solicitada INT,
	@numero_lotes INT,
	@id_lote INT,
	@cantidad_lote INT,
	@contador INT,
	@cantidadHay INT,
	@idDetalleVenta INT,
	@folioVenta INT
	--OBTENEMOS LA VENTA MAS ANTIGUA
	BEGIN TRAN
			BEGIN TRY
	SELECT TOP 1 @folioVenta=folio_venta FROM Ventas with (UPDLOCK) WHERE id_estatus=1 order by fecha asc
	SELECT @contador=1,@cantidadHay=0

	SELECT @numeroProductos= COUNT(0) FROM DetalleRuta  WHERE id_venta=@folioVenta
	UPDATE 
		Ventas 
	SET 
		id_estatus=2
	WHERE 
		folio_venta=@folioVenta
		
	INSERT INTO Ordenes(folio_venta,id_usuario,fecha_inicio)
	VALUES
	(
		@folioVenta,
		@id_usuario,
		GETDATE()
	)
	WHILE(@contador<= @numeroProductos)
	BEGIN
	--OBTENEMOS LOS DATOS DE CADA DETALLE RUTA	
	SELECT 
		@id_lote=id_lote,
		@cantidad_solicitada=cantidad,
		@idDetalleVenta=id_detalle_venta
	FROM 
		DetalleRuta 
	WHERE
		id_venta =@folioVenta
	ORDER BY id_lote
	offset(@contador-1)*1 ROWS 
	FETCH NEXT 1 ROWS only
	IF NOT EXISTS(SELECT 1 FROM Lote_Seccion WHERE id_lote=@id_lote)
		BEGIN
		--esta todo en bodega
		--inserta ruta 
			INSERT INTO RutaPendiente
			SELECT 
					@folioVenta AS idOrden,
					@idDetalleVenta AS idDetalleOrden,
					p.nombre as nombre_producto,
					pr.nombre as nombre_provedor,
					@cantidad_solicitada as cantidadSol,
					0 as id_estante,
					0 as id_seccion,
					0 as estatus,
					1 as estaEnBodega,
					@id_usuario as id_Usuario
				FROM 
					Bodega b
				INNER JOIN 
					Lotes l on l.id_lote=b.id_lote
				INNER JOIN 
					Producto p on p.id_producto=l.id_producto
				INNER JOIN 
				Proveedor pr on pr.id_proveedor=l.id_proveedor
				WHERE 
					b.id_lote=@id_lote
			
			

		END
	ELSE
		BEGIN
			SELECT 
				@cantidad_lote=cantidad
			FROM 
				Lote_Seccion 
			WHERE 
				id_lote=@id_lote
			IF @cantidad_lote >= @cantidad_solicitada
			BEGIN
				INSERT INTO RutaPendiente
				SELECT 
					@folioVenta AS idOrden,
					@idDetalleVenta AS idDetalleOrden,
					p.nombre as nombre_producto,
					pr.nombre as nombre_provedor,
					@cantidad_solicitada as cantidadSol,
					e.id_estante,
					s.num_seccion,
					0 as estatus,
					0 as estaEnBodega,
					@id_usuario as id_Usuario
					--agrego a la ruta 
				FROM 
					Lote_Seccion ls 
				INNER JOIN 
					Lotes l on l.id_lote=ls.id_lote
				INNER  JOIN 
					Seccion s on s.id_seccion=ls.id_seccion
				INNER JOIN 
					Estantes e on e.id_estante=s.id_estante
				INNER JOIN 
					Producto p on p.id_producto=l.id_producto
				INNER JOIN 
				Proveedor pr on pr.id_proveedor=l.id_proveedor
				WHERE 
					ls.id_lote=@id_lote
			END
			ELSE
			BEGIN
			--SACO LO QUE HAY EN SECCION
				SELECT 
					@cantidadHay=cantidad
				FROM 
					Lote_Seccion 
				WHERE 
					id_lote=@id_lote
					--agrego a la ruta 
				INSERT INTO RutaPendiente
				SELECT 
					@folioVenta AS idOrden,
					@idDetalleVenta AS idDetalleOrden,
					p.nombre as nombre_producto,
					pr.nombre as nombre_provedor,
					@cantidadHay as cantidadSol,
					e.id_estante,
					s.num_seccion,
					0 as estatus,
					0 as estaEnBodega,
					@id_usuario as id_Usuario
				FROM 
					Lote_Seccion ls 
				INNER JOIN 
					Lotes l on l.id_lote=ls.id_lote
				INNER  JOIN 
					Seccion s on s.id_seccion=ls.id_seccion
				INNER JOIN 
					Estantes e on e.id_estante=s.id_estante
				INNER JOIN 
					Producto p on p.id_producto=l.id_producto
				INNER JOIN 
				Proveedor pr on pr.id_proveedor=l.id_proveedor
				WHERE 
					ls.id_lote=@id_lote
			

				SET  @cantidad_solicitada=@cantidad_solicitada-@cantidadHay

			--LO QUE FALTA LO SACO DE BODEGA
		
			--inserto ruta con canitdad solicitada
				INSERT INTO RutaPendiente
				SELECT 
					@folioVenta AS idOrden,
					@idDetalleVenta AS idDetalleOrden,
					p.nombre as nombre_producto,
					pr.nombre as nombre_provedor,
					@cantidad_solicitada as cantidadSol,
					0 as id_estante,
					0 as id_seccion,
					0 as estatus,
					1 as estaEnBodega,
					@id_usuario as id_Usuario
				FROM 
					Bodega b
				INNER JOIN 
					Lotes l on l.id_lote=b.id_lote
				INNER JOIN 
					Producto p on p.id_producto=l.id_producto
				INNER JOIN 
				Proveedor pr on pr.id_proveedor=l.id_proveedor
				WHERE 
					b.id_lote=@id_lote
			END
		END
	SET @contador=@contador+1
	END
	COMMIT TRAN
			END TRY
			BEGIN CATCH
			ROLLBACK TRAN
			END CATCH


	
	


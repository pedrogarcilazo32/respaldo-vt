/****** Object:  StoredProcedure [dbo].[vt_sp_lotes]    Script Date: 06/12/2022 08:57:06 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[vt_sp_lotes]

@proceso int,@json VARCHAR (MAX) 
AS
	DECLARE 
	
		@usuario_id INT,
		@idLote INT,
		@idProducto INT,
		@idProvedor INT,
		@entrada INT,
		@entrada_aux INT

	IF @proceso=1
		BEGIN
			SET @idProducto=JSON_VALUE(@json, '$.id_producto')
			SET @idProvedor=JSON_VALUE(@json, '$.id_provedor')
			SET @entrada=JSON_VALUE(@json, '$.entrada_can')

			BEGIN
			INSERT INTO Lotes VALUES(
			@idProvedor,
			@idProducto,
			@entrada,
			0,
			0,
			@entrada,
			GETDATE(),
			1
			)
			SELECT @idLote=@@IDENTITY
			INSERT INTO 
				Bodega 
			VALUES
				(
					@idLote,
					@entrada
				)
			--Si la relacion producto-provedor ya existe sumamos la entrada a esa cantidad si no la creamos
			IF EXISTS (SELECT * FROM Producto_Proveedor WHERE id_proveedor=@idProvedor AND id_producto=@idProducto)
				BEGIN
					SELECT 
						@entrada_aux= disponible_total
					FROM 
						Producto_Proveedor 
					WHERE
						id_proveedor=@idProvedor AND id_producto=@idProducto
					UPDATE 
						Producto_Proveedor 
					SET
						disponible_total=@entrada_aux+@entrada 
					WHERE
						id_proveedor=@idProvedor AND id_producto=@idProducto

					SELECT @entrada_aux = cantidad FROM Bodega WHERE id_lote=@idLote
					
					
				END
			ELSE
				BEGIN
						INSERT INTO Producto_Proveedor
						VALUES
						(
						@idProvedor,
						@idProducto,
						@entrada
						)
						
					
				END
		
			END

		END
		--OBTENER LOTES DE UN PRODUCTO
	IF @proceso=5
		BEGIN
			SET @idProducto=JSON_VALUE(@json, '$.id')
			SELECT 
				l.id_lote,
				l.id_proveedor,
				l.id_producto,
				l.cantidad_ent,
				l.comprometido,
				l.salida_pro,
				l.disponible,
				l.fecha_registro,
				p.nombre as v_nombreProvedor
			FROM 
				Lotes l
			INNER JOIN 
				Proveedor p on l.id_proveedor=p.id_proveedor
			WHERE
				id_producto=@idProducto
			AND 
				l.disponible >0
		END

	
	

	
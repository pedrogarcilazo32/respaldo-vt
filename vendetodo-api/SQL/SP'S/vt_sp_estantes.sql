/****** Object:  StoredProcedure [dbo].[vt_sp_estantes]    Script Date: 12/12/2022 06:29:04 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[vt_sp_estantes]

@proceso int,@json VARCHAR (MAX) 
AS
	DECLARE 
	
		@id_estante INT,
		@seccion_actual INT,
		@id_producto INT,
		@cantidad_solicitada INT,
		@estante_id INT,
		@num_seccion INT,
		@numero_lotes INT,
		@bodegaHay INT, 
		@bodegaSacamos INT,
		@cantidad_lote INT,
		@cantidad_otorgada int,
		@id_lote INT,
		@operacion Int,
		@total_elementos INT,
		@pagina INT
		--OBTENER ESTANTE ACTUAL
	IF @proceso=5
		BEGIN
			SET @id_estante=JSON_VALUE(@json, '$.id_estante')
			SELECT 
			 p.id_producto,
			 s.num_seccion,
			 s.id_seccion,
			 p.nombre as nombre_producto,
			 p.image_url,
			 e.ultimo_ordenamiento,
			 CONVERT(int,(2/p.volumen)) as capacidad_seccion,
			 SUM(ls.cantidad) AS cantidad_otorgada
			 --SUM(l.disponible) AS cantidad_otorgada
		FROM 
			Lote_Seccion ls
		INNER JOIN 
			Lotes l on l.id_lote=ls.id_lote
		INNER JOIN
			Producto p on p.id_producto=l.id_producto
		INNER JOIN 
			Seccion s on s.id_seccion=ls.id_seccion
		INNER JOIN
			Estantes e ON E.id_estante=S.id_estante
		WHERE
			s.id_estante=@id_estante
		GROUP BY
			 p.id_producto,s.num_seccion,
			 s.id_seccion,
			 p.nombre,
			 p.image_url,
			 e.ultimo_ordenamiento,
			 p.volumen
		 ORDER BY
			s.num_seccion
		END
			--OBTENER ESTANTE PENDIENTE
	IF @proceso=6
		BEGIN
			SET @id_estante=JSON_VALUE(@json, '$.id_estante')
			DROP TABLE IF EXISTS #productosExist
			
			CREATE TABLE #productosExist(
			id_producto int,
			disponible int
			)
			INSERT INTO #productosExist(id_producto,disponible)
			SELECT 
				l.id_producto,SUM(l.disponible) as disponible
			FROM 
				Lotes l
			INNER JOIN 
				Producto p on p.id_producto=l.id_producto
			WHERE 
				 p.activo_opc=1
			GROUP BY 
				l.id_producto

			 SELECT 
				 o.id_producto,
				 o.num_seccion,
				 o.nombre_producto,
				 o.image_url,
				 o.capacidad_seccion,
				 o.ultimo_ordenamiento,
				 (CASE WHEN o.capacidad_seccion>pe.disponible THEN pe.disponible ELSE o.capacidad_seccion END) as cantidad_otorgada
			 FROM 
				 OrdenamientoPendiente o
			INNER JOIN #productosExist pe ON pe.id_producto=o.id_producto

			 WHERE
				 id_estante=@id_estante
			ORDER BY 
				num_seccion

		END
		--SOLICITAR REORDEN
		IF @proceso=7
		BEGIN
		BEGIN TRAN
			BEGIN TRY
			SET @id_estante=JSON_VALUE(@json, '$.id_estante')
			DROP TABLE IF EXISTS #productosExistencia,#productoSeccion,#ProductosOcupados
			CREATE TABLE #ProductosOcupados(
			id_producto INT
			)
			CREATE TABLE #productoSeccion (
			id_producto INT,
			num_seccion INT,
			nombre_producto varchar (500),
			url_imagen varchar(500),
			capacidad_seccion INT,
			cantidad_otorgada INT,
			)
			CREATE TABLE #productosExistencia(
			id_producto INT,
			disponible INT,
			numeroVentas INT
			)
			--INSERTAMOS LA CANTIDAD DISPONIBLE DE TODOS LOS PRODUCTOS
			INSERT INTO #productosExistencia(id_producto,disponible,numeroVentas)
			SELECT 
				l.id_producto,SUM(l.disponible) as disponible,ISNULL(SUM(d.cantidad),0) as numeroVentas
			FROM 
				Lotes l with (UPDLOCK)
			INNER JOIN 
				Producto p on p.id_producto=l.id_producto
			LEFT JOIN Detalle_Venta d on d.id_producto=p.id_producto
			WHERE 
				disponible >0 AND p.activo_opc=1
			GROUP BY 
				l.id_producto

			--INSERTAMOS LOS PRODUCTOS QUE ESTEN EN UNA SECCION YA O ESTEN APARTADOS
			INSERT INTO #ProductosOcupados
				SELECT
					id_producto 
				FROM 
					OrdenamientoPendiente
				WHERE 
					id_estante <> @id_estante
			UNION 
				SELECT 
					l.id_producto 
				FROM 
					Lote_Seccion ls
				INNER JOIN 
					Lotes l on l.id_lote=ls.id_lote
				INNER JOIN 
					Seccion s on s.id_seccion=ls.id_seccion
				WHERE 
					s.id_estante <>@id_estante
			GROUP BY
					id_producto
			
			--OBTENEMOS LOS 6 PRODUCTOS MAS VENDIDOS Y QUE NO ESTEN UN UNA SECCION YA 
			INSERT INTO #productoSeccion(id_producto,num_seccion)
			SELECT TOP 6 
				pe.id_producto,ROW_NUMBER() OVER(ORDER BY pe.numeroVentas desc) 
			FROM
				#productosExistencia pe
			LEFT JOIN 
				#ProductosOcupados po 
					on po.id_producto=pe.id_producto
			WHERE
				po.id_producto IS NULL
			ORDER BY 
				pe.numeroVentas desc

			--OPTEMOS LOS DATOS DE CADA PRODUCTO
			UPDATE #productoSeccion
			SET 
				nombre_producto= p.nombre,
				url_imagen= image_url,
				capacidad_seccion=(2/p.volumen)
			FROM 
				Producto p
			INNER JOIN 
				#productoSeccion pe on pe.id_producto=p.id_producto
			WHERE 
				p.activo_opc=1 
			
			UPDATE #productoSeccion 
			SET
				cantidad_otorgada=(CASE WHEN capacidad_seccion>pe.disponible THEN pe.disponible ELSE capacidad_seccion END)
			FROM 
				#productosExistencia pe 
			INNER JOIN 
				#productoSeccion ps on ps.id_producto=pe.id_producto
			
			--REGISTRAMOS EL ESTANTE PENDIENTE
			  INSERT INTO OrdenamientoPendiente
			 SELECT 
				 id_producto,
				 num_seccion,
				 nombre_producto,
				 url_imagen,
				 capacidad_seccion,
				 @id_estante,
				 GETDATE()
			 FROM
			 #productoSeccion
			 --REGISTRAMOMS EL MOVIMIENTO
			 INSERT INTO mov_Estante VALUES(@id_estante,GETDATE(),null)

			 --LO DEVOLVEMOS
			  SELECT 
				 o.id_producto,
				 o.num_seccion,
				 o.nombre_producto,
				 o.image_url,
				 o.capacidad_seccion,
				 o.ultimo_ordenamiento,
				 ps.cantidad_otorgada
			 FROM 
				 OrdenamientoPendiente o
			INNER JOIN #productoSeccion ps on ps.id_producto=o.id_producto
			 WHERE
				 o.id_estante=@id_estante
			COMMIT TRAN
			END TRY
			BEGIN CATCH
			ROLLBACK TRAN
			END CATCH
		END
		--CONFIRMAR REORDEN
		IF @proceso=8
		BEGIN
		BEGIN TRAN
			BEGIN TRY
			SET @id_estante=JSON_VALUE(@json, '$.id_estante')
			--OBTENEMOS LOS PRODUCTOS QUE ESTAN BODEGA
			UPDATE Bodega
			SET cantidad=b.cantidad+l.cantidad
			FROM Lote_Seccion L
			LEFT JOIN Bodega b on b.id_lote=l.id_lote
			INNER JOIN Seccion s on s.id_seccion=l.id_seccion
			WHERE s.id_estante=@id_estante AND b.id_lote IS  NOT NULL 


			--OBTENEMOS LOS PRODUCTOS QUE NO ESTAN BODEGA
			INSERT INTO Bodega
			SELECT l.id_lote,l.cantidad FROM Lote_Seccion L
			LEFT JOIN Bodega b on b.id_lote=l.id_lote
			INNER JOIN Seccion s on s.id_seccion=l.id_seccion
			WHERE s.id_estante=@id_estante AND b.id_lote IS  NULL 

			DELETE Lote_Seccion
				   FROM Lote_Seccion ls INNER JOIN Seccion s
						ON s.id_seccion=ls.id_seccion
				   WHERE s.id_estante=@id_estante

			SET @seccion_actual=1
			WHILE(@seccion_actual<=6)
				BEGIN
				--SET @bodegaSacamos=0
				SELECT 
					@id_producto=id_producto,
					@cantidad_solicitada=capacidad_seccion
				FROM OrdenamientoPendiente WHERE num_seccion=@seccion_actual
				SELECT 
						@numero_lotes= COUNT(0) 
					FROM 
						Lotes 
					WHERE 
						id_producto = @id_producto and disponible >0
	
					WHILE (@numero_lotes >0 AND @cantidad_Solicitada <> 0)
						BEGIN
		
							--OBTENEMOS EL DATO DE CADA LOTE
							SELECT 
								 @id_lote=l.id_lote,
								 @cantidad_lote=disponible
							FROM 
								Lotes l with (UPDLOCK)
							WHERE 
								id_producto = @id_producto and disponible >0
							ORDER BY 
								l.fecha_registro DESC 
							offset(@numero_lotes-1)*1 ROWS 
							FETCH NEXT 1 ROWS only
			
							IF @cantidad_lote > @cantidad_Solicitada
								BEGIN
									--IF @operacion=1
										--BEGIN
											INSERT INTO Lote_Seccion 
											SELECT id_seccion,@id_lote,@cantidad_Solicitada FROM Seccion WHERE id_estante= @id_estante and num_seccion=@seccion_actual
											UPDATE Bodega SET cantidad=cantidad-@cantidad_Solicitada where id_lote=@id_lote
										--END
									--SET @bodegaSacamos=@bodegaSacamos+@cantidad_Solicitada
									SET @cantidad_Solicitada=0
								END
							ELSE
								BEGIN
									--IF @operacion=1
										--BEGIN
											DELETE Bodega WHERE id_lote=@id_lote
											INSERT INTO Lote_Seccion 
											SELECT id_seccion,@id_lote,@cantidad_lote FROM Seccion WHERE id_estante= @id_estante and num_seccion=@seccion_actual
										--END
									--SET @bodegaSacamos=@bodegaSacamos+@cantidad_lote
									SET @cantidad_Solicitada=@cantidad_Solicitada-@cantidad_lote

								END
				
							SET @numero_lotes=@numero_lotes-1
						END
				SET @seccion_actual=@seccion_actual+1
		END
		UPDATE Estantes SET ultimo_ordenamiento = GETDATE() WHERE id_estante=@id_estante
		UPDATE mov_Estante SET fecha_fin=GETDATE() WHERE id_estante=@id_estante

		DELETE OrdenamientoPendiente WHERE id_estante=@id_estante
		COMMIT TRAN
			END TRY
			BEGIN CATCH
			ROLLBACK TRAN
			END CATCH
		END
	IF @proceso=15
	begin
		SELECT
			e.id_estante as id_estantes
		FROM
		Estantes e
			ORDER BY e.id_estante ASC
	end
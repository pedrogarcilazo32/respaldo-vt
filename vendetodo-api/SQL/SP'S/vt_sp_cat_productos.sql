/****** Object:  StoredProcedure [dbo].[vt_sp_cat_productos]    Script Date: 07/12/2022 08:55:57 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[vt_sp_cat_productos]

@proceso int,@json VARCHAR (MAX) 
AS
	DECLARE 
		@total_elementos INT,
		@pagina INT,
		@marca_id INT, 
		@nombre VARCHAR(500),
		@volumen DECIMAL(10,3),
		@precio DECIMAL(10,3),
		@usuario_id INT,
		@id INT,
		@search VARCHAR(500),
		@f_registro_inicio VARCHAR(500),
		@f_registro_fin VARCHAR(500),
		@imageUrl VARCHAR(100),
		@descripcion VARCHAR(500),
		@categoria_id INT,
		@marcaJson VARCHAR(MAX),
		@categoriaJson VARCHAR(MAX)

	--INSERTAR PRODUCTO
	IF @proceso=1
	BEGIN
		
		SET @precio=JSON_VALUE(@json, '$.d_precio')
		SET @volumen=JSON_VALUE(@json, '$.d_volumen')
		SET @nombre=JSON_VALUE(@json, '$.v_nombre')
		SET @imageUrl = JSON_VALUE(@json, '$.v_image_url')
		SET @descripcion=JSON_VALUE(@json, '$.v_descripcion')
		SET @marcaJson=JSON_QUERY(@json, '$.marca')
		SET @categoriaJson=JSON_QUERY(@json, '$.categoria')
		SET @categoria_id = JSON_VALUE(@categoriaJson, '$.id')
		SET @marca_id=JSON_VALUE(@marcaJson, '$.id')

		
		
		BEGIN
			INSERT INTO Producto VALUES(
			@categoria_id,
			@marca_id,
			@nombre,
			@descripcion,
			@precio,
			@volumen,
			GETDATE(),
			1,
			@imageUrl
			)
		END
	END
	--Obtener Productos PAGINADO
	ELSE IF @proceso=2
	BEGIN
	SET @pagina=JSON_VALUE(@json, '$.pagina')

	
	SELECT 
			@total_elementos=COUNT(*)
		FROM 
			Producto p
		INNER JOIN Marcas m ON m.id_marca=p.id_marca
		INNER JOIN Categorias c ON c.id_categoria=p.id_categoria
		WHERE
			p.activo_opc=1
		SELECT 
			p.id_producto,
			p.nombre,
			m.nombre AS v_nombreMarca,
			m.id_marca AS id_marca,
			p.precio as d_precio,
			p.volumen as d_volumen,
			p.fecha_registro,
			p.image_url,
			c.nombre as v_nombreCategoria,
			p.descripcion,
			c.id_categoria as id_categoria,
			

				CASE WHEN CONVERT(VARCHAR(50),(CONVERT(int,@total_elementos ) / 10)) 
			LIKE '%.0%' THEN (( CONVERT(int,@total_elementos ) ) / 10 ) 
			ELSE (( CONVERT(int,@total_elementos ) ) / 10 ) + 1 END AS total_paginas
		FROM 
			Producto p
		INNER JOIN Marcas m ON m.id_marca=p.id_marca
		INNER JOIN Categorias c ON c.id_categoria=p.id_categoria
		WHERE
			p.activo_opc=1
			ORDER BY p.fecha_registro DESC
			offset(@pagina-1)*10 ROWS 
			FETCH NEXT 10 ROWS only
	END
		--ACTUALIZAR Productos 
	ELSE IF @proceso=3
	BEGIN
	
		SET @precio=JSON_VALUE(@json, '$.d_precio')
		SET @volumen=JSON_VALUE(@json, '$.d_volumen')
		SET @nombre=JSON_VALUE(@json, '$.v_nombre')
		SET @imageUrl = JSON_VALUE(@json, '$.v_image_url')
		SET @descripcion=JSON_VALUE(@json, '$.v_descripcion')
		SET @marcaJson=JSON_QUERY(@json, '$.marca')
		SET @categoriaJson=JSON_QUERY(@json, '$.categoria')
		SET @categoria_id = JSON_VALUE(@categoriaJson, '$.id')
		SET @marca_id=JSON_VALUE(@marcaJson, '$.id')
		SET @id=JSON_VALUE(@json, '$.id')
	
	
		UPDATE 
			Producto
		SET 
			id_marca=@marca_id,
			nombre=@nombre,
			volumen=@volumen,
			precio=@precio,
			image_url=@imageUrl,
			descripcion=@descripcion,
			id_categoria=@categoria_id

		WHERE 
			id_producto=@id
			
	END
	--BORRA PRODUCTOS
		ELSE IF @proceso=4
	BEGIN
		
		SET @id=JSON_VALUE(@json, '$.id')

		UPDATE 
			Producto
		SET 
			activo_opc=0
		WHERE
			id_producto=@id
			
	END
	--Busqueda de PRODUCTOS con filtros
		ELSE IF @proceso=5
	BEGIN
		
		SET @search=JSON_VALUE(@json, '$.search')
	SET @usuario_id=JSON_VALUE(@json, '$.i_usuario_id')
	SET @marca_id=JSON_VALUE(@json, '$.id_marca')
	SET @f_registro_inicio=JSON_VALUE(@json, '$.f_registro_inicio')
	SET @f_registro_fin=JSON_VALUE(@json, '$.f_registro_fin')
	SET @categoria_id=JSON_VALUE(@json, '$.id_categoria')



	IF @usuario_id =0 BEGIN SET @usuario_id = NULL END
	IF @marca_id =0 BEGIN SET @marca_id = NULL END
	IF @search = '' BEGIN SET @search = NULL END
	IF @f_registro_inicio ='' BEGIN SET @f_registro_inicio = NULL END
	IF @f_registro_fin ='' BEGIN SET @f_registro_fin = NULL END
	IF @categoria_id =0 BEGIN SET @categoria_id = NULL END

			SELECT 
			p.id_categoria,
			p.nombre,
			m.nombre AS v_nombreMarca,
			m.id_marca AS id_marca,
			p.precio,
			p.volumen,
			p.fecha_registro,
			p.image_url,
			c.nombre as v_nombreCategoria,
			p.descripcion,
			c.id_categoria as id_categoria
		FROM 
			Producto p
		INNER JOIN Marcas m ON m.id_marca=p.id_marca
		INNER JOIN Categorias c ON c.id_categoria=p.id_categoria
		WHERE
			p.activo_opc=1
		AND
			p.nombre like '%'+COALESCE(@search,p.nombre)+'%'
		AND 
			m.id_marca=COALESCE(@marca_id,m.id_marca)
		AND 
			CONVERT(DATE,p.fecha_registro) >= CONVERT(DATE,COALESCE(@f_registro_inicio,p.fecha_registro))
		AND 
			CONVERT(DATE,p.fecha_registro) <= CONVERT(DATE,COALESCE(@f_registro_fin,p.fecha_registro))
		AND 
			c.id_categoria=COALESCE(@categoria_id,c.id_categoria)
		ORDER BY p.fecha_registro desc
			
	END

	
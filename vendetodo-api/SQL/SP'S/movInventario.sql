/****** Object:  StoredProcedure [dbo].[movInventario]    Script Date: 12/12/2022 06:35:16 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[movInventario]

@folioVenta int 
AS
	DECLARE @numeroProductos INT, @id_producto INT, @cantidad_solicitada INT , @id_provedor INT,@numero_lotes INT,
@id_lote INT,@cantidad_lote INT,@idFolio INT,@contador INT,@cantidadOtorgada INT,@idDetalleVenta INT
BEGIN TRAN
			BEGIN TRY
SELECT @idFolio = @folioVenta,@contador=1,@cantidadOtorgada=0

SELECT @numeroProductos= COUNT(0) FROM Detalle_Venta  WHERE folio_venta=@idFolio


WHILE(@contador<= @numeroProductos)
				BEGIN
					SET @cantidadOtorgada=0
				SELECT 
					@id_producto=id_producto,
					@id_provedor=id_proveedor,
					@cantidad_solicitada=cantidad,
					@idDetalleVenta=id_detalle_venta
				FROM Detalle_Venta 
				WHERE folio_venta =@idFolio
				ORDER BY id_producto
				offset(@contador-1)*1 ROWS 
							FETCH NEXT 1 ROWS only
				SELECT 
						@numero_lotes= COUNT(0) 
					FROM 
						Lotes 
					WHERE 
						id_producto = @id_producto
					AND
						id_proveedor=@id_provedor
					AND
						disponible >0
					
					WHILE (@numero_lotes >0 AND @cantidad_Solicitada <> 0)
						BEGIN
		
							--OBTENEMOS EL DATO DE CADA LOTE
							SELECT 
								 @id_lote=l.id_lote,
								 @cantidad_lote=disponible
							FROM 
								Lotes l with (UPDLOCK)
							WHERE 
								id_producto = @id_producto
							AND
								id_proveedor=@id_provedor
							AND
								disponible >0
							ORDER BY 
								l.fecha_registro DESC 
								offset(@numero_lotes-1)*1 ROWS 
							FETCH NEXT 1 ROWS only
			
							IF @cantidad_lote > @cantidad_Solicitada
								BEGIN

								--SELECT @id_lote as idlote,@cantidad_lote as cantLote,@id_producto as idproducto, @cantidad_solicitada as cantidadSol, 'si',@numero_lotes
									
									UPDATE 
										Lotes 
									SET 
										comprometido=comprometido+ @cantidad_solicitada,
										disponible=disponible-@cantidad_solicitada
									WHERE 
										id_lote=@id_lote
									INSERT INTO 
										DetalleRuta 
									VALUES
									(@idFolio,@idDetalleVenta,@id_lote,@cantidad_solicitada,0)

									SET @cantidadOtorgada=@cantidadOtorgada+@cantidad_Solicitada
									SET @cantidad_Solicitada=0
								END
							ELSE
								BEGIN
									--SELECT @id_lote as idlote,@cantidad_lote as cantLote,@id_producto as idproducto, @cantidad_solicitada as cantidadSol, 'no'
									UPDATE 
										Lotes 
									SET 
										comprometido=comprometido+ @cantidad_lote,
										disponible=disponible-@cantidad_lote
									WHERE 
										id_lote=@id_lote
									INSERT INTO 
										DetalleRuta 
									VALUES
									(@idFolio,@idDetalleVenta,@id_lote,@cantidad_lote,0)

									SET @cantidad_Solicitada=@cantidad_Solicitada-@cantidad_lote
									SET @cantidadOtorgada=@cantidadOtorgada+@cantidad_lote
								END
				
							SET @numero_lotes=@numero_lotes-1
							--SELECT @id_producto as producto,@id_provedor as provedor,@cantidadOtorgada AS cantidadSacada ,'final lote',@numero_lotes
							UPDATE 
								Producto_Proveedor 
							SET
								disponible_total = disponible_total-@cantidadOtorgada 
							WHERE
								id_proveedor=@id_provedor 
							AND 
								id_producto=@id_producto
						END
				SET @contador=@contador+1

		END
COMMIT TRAN
			END TRY
			BEGIN CATCH
			ROLLBACK TRAN
			END CATCH


	
	


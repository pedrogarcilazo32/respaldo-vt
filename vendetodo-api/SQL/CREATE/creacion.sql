-- CREATE DATABASE db_VendeTodo
-- USE db_VendeTodo

--CREACION DE TABLAS
CREATE TABLE vt_cat_tipo_usuarios(
id INT IDENTITY(1,1) PRIMARY KEY NOT NULL,
v_nombre_tipo VARCHAR(100) NOT NULL,
b_activo_opc BIT NOT NULL,
f_registro DATETIME NOT NULL,
i_usuario_id  INT NOT NULL
)
GO

CREATE TABLE vt_cat_usuarios(
id INT IDENTITY(1,1) PRIMARY KEY NOT NULL ,
v_nombre VARCHAR(100) NOT NULL,
v_password VARCHAR(500) NOT NULL,
i_id_tipo_usuario INT NOT NULL,
b_activo_opc BIT NOT NULL,
f_registro DATETIME NOT NULL,
i_usuario_id  INT NOT NULL
)
GO

CREATE TABLE vt_cat_opciones(
id INT IDENTITY (1,1) PRIMARY KEY NOT NULL,
v_nombre VARCHAR(100) NOT NULL,
b_activo_opc BIT NOT NULL,
f_registro DATETIME NOT NULL,
i_usuario_id  INT NOT NULL
)
GO

CREATE TABLE vt_opciones_tipoUsuario(
id INT IDENTITY(1,1)  PRIMARY KEY NOT NULL ,
i_id_tipoUsuario INT NOT NULL,
i_id_opcion INT NOT NULL,
b_activo_opc BIT NOT NULL,
f_registro DATETIME NOT NULL,
i_usuario_id  INT NOT NULL
)
GO

CREATE TABLE vt_cat_almacenes(
id INT IDENTITY(1,1) PRIMARY KEY NOT NULL,
d_capacidad DECIMAL(18,2) NOT NULL,
v_ubicacion VARCHAR(MAX) NOT NULL,
b_activo_opc BIT NOT NULL,
f_registro DATETIME NOT NULL,
i_usuario_id  INT NOT NULL
)
GO

CREATE TABLE vt_cat_estantes(
id INT IDENTITY(1,1) PRIMARY KEY NOT NULL,
i_id_almacen INT  NOT NULL,
d_capacidad DECIMAL(18,2) NOT NULL,
b_activo_opc BIT NOT NULL,
f_registro DATETIME NOT NULL,
i_usuario_id  INT NOT NULL
)
GO

CREATE TABLE vt_cat_secciones(
id INT IDENTITY(1,1) PRIMARY KEY NOT NULL,
i_id_almacen INT NOT NULL,
i_id_estante INT NOT NULL,
d_capacidad DECIMAL(18,2) NOT NULL,
b_activo_opc BIT NOT NULL,
f_registro DATETIME NOT NULL,
i_usuario_id  INT NOT NULL
)
GO

CREATE TABLE vt_cat_provedores(
id INT IDENTITY (1,1) PRIMARY KEY NOT NULL,
v_nombre VARCHAR(500) NOT NULL,
v_apellido_pat VARCHAR(500) NOT NULL,
v_apellido_mat VARCHAR(500) DEFAULT '',
v_direccion VARCHAR(500) NOT NULL,
c_telefono CHAR(10) NOT NULL,
b_activo_opc BIT NOT NULL,
f_registro DATETIME NOT NULL,
i_usuario_id  INT NOT NULL
)
GO

CREATE TABLE vt_cat_marcas(
id INT IDENTITY (1,1) PRIMARY KEY NOT NULL,
v_nombre VARCHAR(500) NOT NULL,
b_activo_opc BIT NOT NULL,
f_registro DATETIME NOT NULL,
i_usuario_id  INT NOT NULL
)
GO

CREATE TABLE vt_marcas_provedor(
id INT IDENTITY (1,1) PRIMARY KEY NOT NULL,
i_id_marca INT NOT NULL,
i_id_provedor INT NOT NULL,
b_activo_opc BIT NOT NULL,
f_registro DATETIME NOT NULL,
i_usuario_id  INT NOT NULL
)
GO

create TABLE vt_cat_productos(
id INT IDENTITY(1,1) PRIMARY KEY NOT NULL,
i_id_marca INT NOT NULL,
v_nombre VARCHAR(500) NOT NULL,
d_volumen DECIMAL(10,2) NOT NULL,
d_precio DECIMAL(10,2) NOT NULL,
b_activo_opc BIT NOT NULL,
f_registro DATETIME NOT NULL,
i_usuario_id  INT NOT NULL,
v_descripcion VARCHAR(300) NULL,
v_image_url VARCHAR(100) NULL
)
GO

CREATE TABLE vt_productos_provedor(
id INT IDENTITY(1,1) PRIMARY KEY NOT NULL,
i_id_producto INT NOT NULL,
i_id_provedor INT NOT NULL,
i_id_estante INT NOT NULL,
i_id_seccion INT NOT NULL,
i_cantidad INT NOT NULL,
b_activo_opc BIT NOT NULL,
f_registro DATETIME NOT NULL,
i_usuario_id  INT NOT NULL
)
GO

CREATE TABLE vt_bodega(
id INT IDENTITY (1,1) PRIMARY KEY NOT NULL,
i_id_producto INT NOT NULL,
i_id_provedor INT NOT NULL,
i_cantidad INT NOT NULL,
b_activo_opc BIT NOT NULL,
f_registro DATETIME NOT NULL,
i_usuario_id  INT NOT NULL
)
GO

CREATE TABLE vt_cat_eventos(
id INT IDENTITY(1,1) PRIMARY KEY NOT NULL,
v_nombre VARCHAR(500) NOT NULL,
b_activo_opc BIT NOT NULL,
f_registro DATETIME NOT NULL,
i_usuario_id  INT NOT NULL
)
GO

CREATE TABLE vt_mov_entradas_salidas_producto(
id INT IDENTITY(1,1) PRIMARY KEY NOT NULL,
i_id_producto INT NOT NULL,
i_id_provedor INT NOT NULL,
i_id_evento INT NOT NULL,
i_cantidad INT NOT NULL,
i_disponible INT NOT NULL,
b_activo_opc BIT NOT NULL,
f_registro DATETIME NOT NULL,
i_usuario_id  INT NOT NULL
)


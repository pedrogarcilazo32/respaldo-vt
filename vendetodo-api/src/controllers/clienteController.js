const { getConnection } = require('../database/connection');
const sql = require('mssql');
const path = require('path');
const clienteQuery = require('../database/clienteQuerys');
const fs = require('fs');
const bcrypt = require('bcrypt');



module.exports = {

  async guardarCliente(req, res) {
    const body = req.body;
    

    const aux = await bcrypt.hash(body.usuario.contra, 10);
    
    body.usuario.contra = aux;
    
    try {
      const pool = await getConnection();
     

      const json = JSON.stringify(body);
      console.log('el json que envio',json)
      await pool
        .request()
        .input("json", sql.VarChar, json)
        .query(clienteQuery.guardarCliente);

      res.send({ isValid: true, data: req.body });
    } catch (error) {
      res.send({ isValid: false, message: error.message });
    }
  },

 
};

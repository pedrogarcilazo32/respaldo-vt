const { getConnection } = require('../database/connection');
const querys = require('../database/suppliersQuerys');
const sql = require('mssql');

module.exports = {
    async getSuppliers(req, res) {
        const json = JSON.stringify(req.body);
        console.log('el json que envio',json)
        try {
          const pool = await getConnection();
    
          const result = await pool
            .request()
            .input("json", sql.VarChar, json)
            .query(querys.getAllSuppliers);
    
          res.send({ isValid: true, data: result.recordset });
        } catch (error) {
          res.send({ isValid: false, message: error.message });
        }
      },
};

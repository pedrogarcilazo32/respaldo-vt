const { getConnection } = require('../database/connection');
const sql = require('mssql');
const path = require('path');
const querys = require('../database/categoriesQuerys');
const tokens = require('../utils/tokens');



module.exports = {
  async getCategories(req, res) {
    const json = JSON.stringify(req.body);
    //console.log('el json que envio',json)
    try {
      const pool = await getConnection();

      const result = await pool
        .request()
        .input("json", sql.VarChar, json)
        .query(querys.getAllCategories);

      res.send({ isValid: true, data: result.recordset });
    } catch (error) {
      res.send({ isValid: false, message: error.message });
    }
  },

  async saveCategories(req, res) {
    const json = JSON.stringify(req.body);
    try {
      const pool = await getConnection();

      const result = await pool
        .request()
        .input("json", sql.VarChar, json)
        .query(querys.saveCategory);

      res.send({ isValid: true, data: result.recordset });
    } catch (error) {
      res.send({ isValid: false, message: error.message });
    }
  }
};

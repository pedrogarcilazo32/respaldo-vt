const { getConnection } = require('../database/connection');
const sql = require('mssql');
const path = require('path');
const querys = require('../database/batchesQuerys');
const tokens = require('../utils/tokens');



module.exports = {
  async addBatch(req, res) {
    const json = JSON.stringify(req.body);
    try {
      const pool = await getConnection();

      const result = await pool
        .request()
        .input("json", sql.VarChar, json)
        .query(querys.addBatch);

      res.send({ isValid: true, data: result.recordset });
    } catch (error) {
      res.send({ isValid: false, message: error.message });
    }
  },
  async getProductsBatches(req, res) {
    const json = JSON.stringify(req.body);
     console.log('el json que envio',json)
    try {
      const pool = await getConnection();

      const result = await pool
        .request()
        .input("json", sql.VarChar, json)
        .query(querys.getProductsBatches);

      res.send({ isValid: true, data: result.recordset });
    } catch (error) {
      res.send({ isValid: false, message: error.message });
    }
  },


  
};

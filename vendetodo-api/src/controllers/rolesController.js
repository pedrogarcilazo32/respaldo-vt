const { getConnection } = require('../database/connection');
const sql = require('mssql');
const path = require('path');
const querys = require('../database/querys');
const tokens = require('../utils/tokens');
const sharp = require('sharp');
const fs = require('fs');


module.exports = {
    
    async getRoles(req, res) {
      const json = JSON.stringify(req.body);
  
      try {
        const pool = await getConnection();
  
        const result = await pool
          .request()
          .input("json", sql.VarChar, json)
          .query(querys.getAllRoles);
  
        res.send({ isValid: true, data: result.recordset });
      } catch (error) {
        res.send({ isValid: false, message: error.message });
      }
    },
}
const { getConnection } = require('../database/connection');
const sql = require('mssql');
const path = require('path');
const querys = require('../database/querys');
const tokens = require('../utils/tokens');
const sharp = require('sharp');
const bcrypt = require('bcrypt')
const fs = require('fs');


module.exports = {

  
  async guardarEmpleado(req, res) {
    const body = req.body;
    

    const aux = await bcrypt.hash(body.usuario.contra, 10);
    
    body.usuario.contra = aux;
    
    try {
      const pool = await getConnection();
     

      const json = JSON.stringify(body);
      console.log('el json que envio',json)
      await pool
        .request()
        .input("json", sql.VarChar, json)
        .query(querys.guardarEmpleado);

      res.send({ isValid: true, data: req.body });
    } catch (error) {
      res.send({ isValid: false, message: error.message });
    }
  },
    async getEmpleados(req, res) {
      const json = JSON.stringify(req.body);
  
      try {
        const pool = await getConnection();
  
        const result = await pool
          .request()
          .input("json", sql.VarChar, json)
          .query(querys.getAllEmpleados);
  
        res.send({ isValid: true, data: result.recordset });
      } catch (error) {
        res.send({ isValid: false, message: error.message });
      }
    },
    
    async searchEmail(req, res) {
      var json = JSON.stringify(req.body);
      console.log('el json', json);
  
      try {
        const pool = await getConnection();
        const result = await pool
          .request()
          .input("json", sql.VarChar, json)
          .query(querys.searchEmail);
  
        res.send({ isValid: true, data: result.recordset });
      } catch (error) {
        console.log(error);
        res.send({ isValid: false, message: error.message });
      }
    },
}
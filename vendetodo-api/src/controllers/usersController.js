const { getConnection } = require('../database/connection');
const querys = require('../database/querys');
const Userquerys = require('../database/usersQuerys');
const sql = require('mssql');

const bcrypt = require('bcrypt');


module.exports = {
  async userLogin(req, res) {
    try {
      const pool = await getConnection();
      const result = await pool.request().query(querys.getAllProducts);

      res.send({ isValid: true, data: result.recordset });
    } catch (error) {
      res.send({ isValid: false, message: error.message });
    }
  },

  async getUsers(req, res) {
    try {
      const pool = await getConnection();
      const result = await pool.request().query(Userquerys.getAllUsers);

      res.send({ isValid: true, data: result.recordset });
    } catch (error) {
      res.send({ isValid: false, message: error.message });
    }
  },
  async guardarCliente(req, res) {
    const body = req.body;


    const aux = await bcrypt.hash(body.usuario.contra, 10);

    body.usuario.contra = aux;

    try {
      const pool = await getConnection();


      const json = JSON.stringify(body);
      console.log('el json que envio',json)
      await pool
        .request()
        .input("json", sql.VarChar, json)
        .query(Userquerys.guardarCliente);

      res.send({ isValid: true, data: req.body });
    } catch (error) {
      res.send({ isValid: false, message: error.message });
    }
  },
  async searchEmail(req, res) {
    var json = JSON.stringify(req.body);
    console.log('el json', json);

    try {
      const pool = await getConnection();
      const result = await pool
        .request()
        .input("json", sql.VarChar, json)
        .query(Userquerys.searchEmail);

      res.send({ isValid: true, data: result.recordset });
    } catch (error) {
      console.log(error);
      res.send({ isValid: false, message: error.message });
    }
  },

  async getUserById(req, res) {
    console.log(req.params);

    const body = JSON.stringify(req.params);

    try {
      const pool = await getConnection();
      const result = await pool.request()
        .input("idUsuario", sql.Int, req.params.id)
        .query(Userquerys.getUser);

      res.send({ isValid: true, data: result.recordset });
    } catch (error) {
      res.send({ isValid: false, message: error.message });
    }
  }
};

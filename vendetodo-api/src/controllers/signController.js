const { getConnection } = require('../database/connection');
const sql = require('mssql');
const path = require('path');
const querys = require('../database/signQuery');
const jwt = require('jsonwebtoken');


const bcrypt = require('bcrypt');

module.exports = {
  async test(req, res) {
    console.log(req.data);
    if (req.data.nombre === 'admin') {

    }
    res.json('Informacion secreta')
  },

  async signIn(req, res) {
    const body = req.body;
    const json = JSON.stringify(req.body);
    try {
      const pool = await getConnection();
      const result = await pool
        .request()
        .input("json", sql.VarChar, json)
        .query(querys.createSignUp);
      const { id_usuario, email, nombre, contra } = result.recordset[0];


      if (result.recordsets.length) {
        const isCorrectPassword = await bcrypt.compare(body.contra, contra)
        console.log(isCorrectPassword, contra);
        if(isCorrectPassword){
          let data = JSON.stringify(result.recordset);
          const token = jwt.sign(data, 'still');
          res.send({ isValid: true, data: token });
        } else {
          res.send({ isValid: false, message: 'Correo o Contraseña incorrectos' });
        }
      }
    } catch (error) {
      res.send({ isValid: false, message: error.message });
    }
  },

};

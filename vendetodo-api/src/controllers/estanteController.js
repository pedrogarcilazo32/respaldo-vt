const { getConnection } = require('../database/connection');
const sql = require('mssql');
const path = require('path');
const querys = require('../database/estanteQuerys');
const tokens = require('../utils/tokens');

module.exports = {
  async getEstantes(req, res) {
    const json = JSON.stringify(req.body);

    try {
      const pool = await getConnection();

      const result = await pool
        .request()
        .input("json", sql.VarChar, json)
        .query(querys.getEstantes);

      res.send({ isValid: true, data: result.recordset });
    } catch (error) {
      res.send({ isValid: false, message: error.message });
    }
  },

  async getOrdenameinto(req, res) {
    const json = JSON.stringify(req.body);
    console.log('el json que envio',json);
    try {
      const pool = await getConnection();

      const result = await pool
        .request()
        .input("json", sql.VarChar, json)
        .query(querys.getAllOrdenamientos);

      res.send({ isValid: true, data: result.recordset });
    } catch (error) {
      res.send({ isValid: false, message: error.message });
    }
  },

  async obtenerEstanteActual(req, res) {
    const json = JSON.stringify(req.body);
    console.log('el json',json)
    try {
      const pool = await getConnection();

      const result = await pool
        .request()
        .input("json", sql.VarChar, json)
        .query(querys.obtenerEstanteActual);

      res.send({ isValid: true, data: result.recordset });
    } catch (error) {
      res.send({ isValid: false, message: error.message });
    }
  },
  async obtenerEstantePendiente(req, res) {
    const json = JSON.stringify(req.body);
    //console.log('el json que envio',json)
    try {
      const pool = await getConnection();

      const result = await pool
        .request()
        .input("json", sql.VarChar, json)
        .query(querys.obtenerEstantePendiente);

      res.send({ isValid: true, data: result.recordset });
    } catch (error) {
      res.send({ isValid: false, message: error.message });
    }
  },
  async solicitarReorden(req, res) {
    const json = JSON.stringify(req.body);
    console.log('el jsono de solicitud',JSON.stringify(req.body))
    try {
      const pool = await getConnection();

      const result = await pool
        .request()
        .input("json", sql.VarChar, json)
        .query(querys.solicitarReorden);

      res.send({ isValid: true, data: result.recordset });
    } catch (error) {
      res.send({ isValid: false, message: error.message });
    }
  },
  async confirmarNuevoOrden(req, res) {
    const json = JSON.stringify(req.body);
    console.log('el jsono de solicitud',JSON.stringify(req.body))
    try {
      const pool = await getConnection();

      const result = await pool
        .request()
        .input("json", sql.VarChar, json)
        .query(querys.confirmarNuevoOrden);

      res.send({ isValid: true, data: req.body });
    } catch (error) {
      res.send({ isValid: false, message: error.message });
    }
  },




};

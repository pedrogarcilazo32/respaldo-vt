const { getConnection } = require('../database/connection');
const sql = require('mssql');
const path = require('path');
const querys = require('../database/marcasQuerys');
const tokens = require('../utils/tokens');

module.exports = {
  async procesarPago(req, res) {
    try {
      const referencia = tokens.generate();
      const data = {
        referencia
      };


      res.send({ isValid: true, data });
    } catch (error) {
      res.send({ isValid: false, message: error.message });
    }
  },
};

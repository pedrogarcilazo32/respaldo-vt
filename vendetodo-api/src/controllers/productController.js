const { getConnection } = require('../database/connection');
const sql = require('mssql');
const path = require('path');
const querys = require('../database/querys');
const tokens = require('../utils/tokens');
const sharp = require('sharp');
const fs = require('fs');

const saveProductImage = async (dataImage) => {
  let nameImage = '';
  const uri = dataImage.split(';base64,').pop();
  const img = new Buffer.from(uri, 'base64');
  nameImage = `${tokens.generate()}.png`;
  const imagePath = path.resolve(__dirname, '..', 'products-images', nameImage);

  sharp(img)
    .toFile(imagePath, (err, info) => {
      if (err) {
        console.log('err', err, info);

        fs.unlinkSync(imagePath);
      }
    });

  const data = {
    imagePath,
    imageUrl: `/api/products/assets?image=${nameImage}`
  };

  return data;
};

const saveImageBanner = async (dataImage) => {
  let nameImage = '';
  const uri = dataImage.split(';base64,').pop();
  const img = new Buffer.from(uri, 'base64');
  nameImage = `${tokens.generate()}.png`;
  const imagePath = path.resolve(__dirname, '..', 'banner-images', nameImage);

  sharp(img)
    .resize({ width: 200, heiht: 200, fit: 'fill' })
    .toFile(imagePath, (err, info) => {
      if (err) {
        console.log('err', err, info);

        fs.unlinkSync(imagePath);
      }
    });

  const data = {
    imagePath,
    imageUrl: `/api/products/assets?image=${nameImage}`
  };

  return data;
};

module.exports = {
  async saveProductoImage(req, res) {
    const body = req.body;

    const dataImage = await saveProductImage(body.image);

    res.send({ isValid: true, data: dataImage});
  },

  async getAssets(req, res) {
    res.sendFile(path.resolve(__dirname, '..', 'products-images', req.query.image));
  },

  async getAssetsBanner(req, res) {
    res.sendFile(path.resolve(__dirname, '..', 'banner-images', req.query.image));
  },

  async getProducts(req, res) {
    const json = JSON.stringify(req.body);

    try {
      const pool = await getConnection();

      const result = await pool
        .request()
        .input("json", sql.VarChar, json)
        .query(querys.getAllProducts);

      res.send({ isValid: true, data: result.recordset });
    } catch (error) {
      res.send({ isValid: false, message: error.message });
    }
  },
  async getProductosApp(req, res) {
    const json = JSON.stringify(req.body);

    try {
      const pool = await getConnection();

      const result = await pool
        .request()
        .input("json", sql.VarChar, json)
        .query(querys.getExistencia);

      res.send({ isValid: true, data: result.recordset });
    } catch (error) {
      res.send({ isValid: false, message: error.message });
    }
  },

  async saveProduct(req, res) {
    // const { id_marca, v_nombreMarca, v_nombre,  d_precio, d_volumen, f_registro, i_usuario_id } = req.body;
    const body = req.body;
    try {
      const pool = await getConnection();
      // @marca, @name, @volumen,@precio,@activo,@registro,@usuario

      if(body.dataFile && body.dataFile.archivo){
        const dataImage =  await saveProductImage(body.dataFile.archivo);
        body.v_image_url = dataImage.imageUrl;
        body.dataFile='';
      }

      delete body.dataFile;

      const json = JSON.stringify(body);
      await pool
        .request()
        .input("json", sql.VarChar, json)
        .query(querys.saveProduct);

      res.send({ isValid: true, data: req.body });
    } catch (error) {
      res.send({ isValid: false, message: error.message });
      fs.unlinkSync(body.imagePath);
    }
  },

  async updateProduct(req, res) {
    // const { id, id_marca, v_nombre,  d_precio, d_volumen } = req.body;
    const body = req.body;
    try {
      const pool = await getConnection();

      if(body.dataFile && body.dataFile.archivo){
        const dataImage =  await saveProductImage(body.dataFile.archivo);
        body.v_image_url = dataImage.imageUrl;
      }

      const json = JSON.stringify(body);

      await pool
        .request()
        .input("json", sql.VarChar, json)
        .query(querys.updateProduct);

      res.send({ isValid: true, data: body });
    } catch (error) {
      res.send({ isValid: false, message: error.message });
      fs.unlinkSync(body.imagePath);
    }
  },

  async deleteProduct(req, res) {
    var json = JSON.stringify(req.body);

    try {
      const pool = await getConnection();
      await pool
        .request()
        .input("json", sql.VarChar, json)
        .query(querys.deleteProduct);

      res.send({ isValid: true, data: req.body });
    } catch (error) {
      res.send({ isValid: false, message: error.message });
    }
  },

  async searchProduct(req, res) {
    console.log(req.body);

    const body = req.body;

    if(body.category === 'all'){
      body.category = null;
    }
    // const { search,id_marca,i_usuario_id,f_registro_fin,f_registro_inicio } = req.body;
    var json = JSON.stringify(body);;

    try {
      const pool = await getConnection();
      const result = await pool
        .request()
        .input("json", sql.VarChar, json)
        // .input("id_marca",sql.Int,req.body.id_marca)
        // .input("i_usuario_id",sql.Int,req.body.i_usuario_id)
        // .input("f_registro_fin",sql.VarChar, req.body.f_registro_fin)
        // .input("f_registro_inicio",sql.VarChar, req.body.f_registro_inicio)

        .query(querys.searchProduct2);

      res.send({ isValid: true, data: result.recordset });
    } catch (error) {
      console.log(error);
      res.send({ isValid: false, message: error.message });
    }
  },

  async getDetalleProducto(req, res) {
    const queryParams = { idProducto: Number(req.params.idProducto)};

    try {
      const pool = await getConnection();

      var json = JSON.stringify(queryParams);;

      const result = await pool
        .request()
        .input("json", sql.VarChar, json)
        // .input("id_marca",sql.Int,req.body.id_marca)
        // .input("i_usuario_id",sql.Int,req.body.i_usuario_id)
        // .input("f_registro_fin",sql.VarChar, req.body.f_registro_fin)
        // .input("f_registro_inicio",sql.VarChar, req.body.f_registro_inicio)

        .query(querys.getDetalleProducto);

      res.send({ isValid: true, data: result.recordset });
    } catch (error) {
      console.log(error);
      res.send({ isValid: false, message: error.message });
    }
  },

  async getProveedor(req, res) {
    const queryParams = { idProducto: Number(req.params.idProducto)};

    try {
      const pool = await getConnection();

      var json = JSON.stringify(queryParams);;

      const result = await pool
        .request()
        .input("json", sql.VarChar, json)
        .query(querys.getProveedor);

      res.send({ isValid: true, data: result.recordset });
    } catch (error) {
      console.log(error);
      res.send({ isValid: false, message: error.message });
    }
  },

  async getProductosPorCategoria(req, res) {
    const queryParams = { idCat: Number(req.params.idCat)};

    try {
      const pool = await getConnection();

      var json = JSON.stringify(queryParams);;

      const result = await pool
        .request()
        .input("json", sql.VarChar, json)
        .query(querys.getProductosPorCategoria);

      res.send({ isValid: true, data: result.recordset });
    } catch (error) {
      console.log(error);
      res.send({ isValid: false, message: error.message });
    }
  },

  async saveBanner(req, res) {
    const body = req.body;

    const dataImage = await saveImageBanner(body.image);

    res.send({ isValid: true, data: dataImage});
  },

  async getProductosPorCategoriaNews(req, res) {
    const queryParams = { idCat: Number(req.params.idCat)};

    try {
      const pool = await getConnection();

      var json = JSON.stringify(queryParams);;

      const result = await pool
        .request()
        .input("json", sql.VarChar, json)
        .query(querys.getProductosPorCategoriaNews);

      res.send({ isValid: true, data: result.recordset });
    } catch (error) {
      console.log(error);
      res.send({ isValid: false, message: error.message });
    }
  },

};

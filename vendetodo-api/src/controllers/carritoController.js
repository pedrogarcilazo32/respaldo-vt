const { getConnection } = require('../database/connection');
const sql = require('mssql');
const querys = require('../database/carritoQuerys');

module.exports = {
  async guardarLineaCarrito(req, res) {
    console.log('llegue', req.body);
    //console.log('el json que envio',json)
    try {
      const pool = await getConnection();

      const params = {
        idCarrito: req.body.idCarrito,
        idProducto: req.body.LC.producto.idProducto,
        idProveedor: req.body.LC.proveedor.idProveedor,
        cantidad: req.body.LC.cantidad
      };

      console.log('pars', params);
      const json = JSON.stringify(params);

      const result = await pool
        .request()
        .input("json", sql.VarChar, json)
        .query(querys.saveLineaCarrito);

      res.send({ isValid: true, data: result.recordset });
    } catch (error) {
      res.send({ isValid: false, message: error.message });
    }
  },

  async getDetalleCarrito(req, res) {
    console.log('llegue', req.params);
    //console.log('el json que envio',json)
    try {
      const pool = await getConnection();

      const json = JSON.stringify(req.params);

      const result = await pool
        .request()
        .input("json", sql.VarChar, json)
        .query(querys.getDetalleCarrito);

      res.send({ isValid: true, data: result.recordset });
    } catch (error) {
      res.send({ isValid: false, message: error.message });
    }
  },

  async validarExistencia(req, res) {
    const body = req.params;
    console.log('recibo validar', body);

    try {
      const pool = await getConnection();

      const json = JSON.stringify(req.params);

      const result = await pool
        .request()
        .input("json", sql.VarChar, json)
        .query(querys.validarCarrito);

      res.send({ isValid: true, data: result.recordset });
    } catch (error) {
      console.log(error);
      res.send({ isValid: false, message: error.message });
    }
  },

  async guardarVenta(req, res) {
    const body = req.body;
    console.log('recibo validar', body);

    try {
      const pool = await getConnection();

      Object.keys(body).forEach((key) => {
        const currentLinea = body[key];

        if (typeof currentLinea === 'object' ) {
          const newBOdy = [];

          Object.keys(body[key]).forEach((fh) => {
            const aux = {};
            // console.log(body[key][fh]);
            aux.idProducto = body[key][fh].producto.idProducto;
            aux.idProveedor = body[key][fh].proveedor.idProveedor;
            aux.cantidad = body[key][fh].cantidad;

            newBOdy.push(aux);

          });
          body[key] = newBOdy;
        }
      });

      console.log(body);

      const json = JSON.stringify(body);

      const result = await pool
        .request()
        .input("json", sql.VarChar, json)
        .query(querys.guardarVenta);

      res.send({ isValid: true, data: result.recordset });
    } catch (error) {
      console.log(error);
      res.send({ isValid: false, message: error.message });
    }
  },

  async eliminarLineaCarrito(req, res) {
    const body = req.body;
    console.log('recibo validar', body);

    try {
      const pool = await getConnection();
      // const json = JSON.stringify(body);

      const result = await pool
        .request()
        .input("idLineaCarrito", sql.Int, body.idLineaCarrito)
        .query(querys.eliminarLineaCarrito);

      res.send({ isValid: true, data: result.recordset });
    } catch (error) {
      console.log(error);
      res.send({ isValid: false, message: error.message });
    }
  },

  async getProveedorExistencia(req, res) {
    const body = req.body;
    console.log('recibo validar', body);

    try {
      const pool = await getConnection();
      const json = JSON.stringify(body);

      const result = await pool
        .request()
        .input("json", sql.VarChar, json)
        .query(querys.proveedorExistencia);

      res.send({ isValid: true, data: result.recordset });
    } catch (error) {
      console.log(error);
      res.send({ isValid: false, message: error.message });
    }
  }
}

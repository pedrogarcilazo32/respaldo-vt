const { getConnection } = require('../database/connection');
const sql = require('mssql');
const path = require('path');
const querys = require('../database/marcasQuerys');
const tokens = require('../utils/tokens');


module.exports = {
  async getMarcas(req, res) {
    const json = JSON.stringify(req.body);
    try {
      const pool = await getConnection();
      // const result = await pool.request().query(querys.getAllBrands);
      const result = await pool
      .request()
      .input("json", sql.VarChar, json)
      .query(querys.getAllBrands);

      res.send({ isValid: true, data: result.recordset });
      console.log(result.recordset)
    } catch (error) {
      res.send({ isValid: false, message: error.message });

    }
  },
};

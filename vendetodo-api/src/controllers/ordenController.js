const { getConnection } = require("../database/connection");
const sql = require("mssql");
const path = require("path");
const querys = require("../database/ordenQuerys");
const tokens = require("../utils/tokens");

module.exports = {
  async getOrdenes(req, res) {
    const json = JSON.stringify(req.body);
    console.log('el json que envio',json);
    try {
      const pool = await getConnection();

      const result = await pool
        .request()
        .input("json", sql.VarChar, json)
        .query(querys.getAllOrders);

      res.send({ isValid: true, data: result.recordset });
    } catch (error) {
      res.send({ isValid: false, message: error.message });
    }
  },

  async solicitarOrden(req, res) {
    const json = JSON.stringify(req.body);
    //console.log('el json que envio',json)
    try {
      const pool = await getConnection();

      const result = await pool
        .request()
        .input("json", sql.VarChar, json)
        .query(querys.solicitarOrden);

      res.send({ isValid: true, data: result.recordset });
    } catch (error) {
      res.send({ isValid: false, message: error.message });
    }
  },
  async obtenerOrdenPendiente(req, res) {
    const json = JSON.stringify(req.body);
    //console.log('el json que envio',json)
    try {
      const pool = await getConnection();

      const result = await pool
        .request()
        .input("json", sql.VarChar, json)
        .query(querys.obtenerOrdenPendiente);

      res.send({ isValid: true, data: result.recordset });
    } catch (error) {
      res.send({ isValid: false, message: error.message });
    }
  },
  async cambiarEstado(req, res) {
    const json = JSON.stringify(req.body);
    console.log('el json que envio',json)
    try {
      const pool = await getConnection();

      const result = await pool
        .request()
        .input("json", sql.VarChar, json)
        .query(querys.cambiarEstado);

      res.send({ isValid: true, data: result.recordset });
    } catch (error) {
      res.send({ isValid: false, message: error.message });
    }
  },
  async finalizarOrden(req, res) {
    const json = JSON.stringify(req.body);
    console.log('el json que envio',json)
    try {
      const pool = await getConnection();

      const result = await pool
        .request()
        .input("json", sql.VarChar, json)
        .query(querys.finalizarOrden);

      res.send({ isValid: true, data: result.recordset });
    } catch (error) {
      res.send({ isValid: false, message: error.message });
    }
  },
};

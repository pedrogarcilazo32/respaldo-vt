
const path = require('path');
const tokens = require('../utils/tokens');
const sharp = require('sharp');
const fs = require('fs');

const saveBanner = async (dataImage) => {
  let nameImage = '';
  const uri = dataImage.split(';base64,').pop();
  const img = new Buffer.from(uri, 'base64');
  nameImage = `${tokens.generate()}.png`;
  const imagePath = path.resolve(__dirname, '..', 'banner-images', nameImage);

  sharp(img)
    .toFile(imagePath, (err, info) => {
      if (err) {
        console.log('err', err, info);

        fs.unlinkSync(imagePath);
      }
    });

  const data = {
    imagePath,
    imageUrl: `/api/banners/assets?image=${nameImage}`
  };

  return data;
};
module.exports = {

  async getAssetsBanner(req, res) {
    res.sendFile(path.resolve(__dirname, '..', 'banner-images', req.query.image));
  },

  async saveBannerImage(req, res) {
    const body = req.body;

    const dataImage = await saveBanner(body.image);

    res.send({ isValid: true, data: dataImage });
  },
};

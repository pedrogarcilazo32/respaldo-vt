module.exports = {
  dbSettings: {
    // user: 'admin',
    user: 'oscarb',
    password: 'C00l1345',
    server: 'sqlvendetodo.database.windows.net',
    database: 'VENDETODO',
    options: {
      encrypt: true, // for azure
      trustServerCertificate: true, // change to true for local dev / self-signed certs
    },
  }
};
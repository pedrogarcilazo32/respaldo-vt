const moment = require('moment-timezone');

module.exports = {
  date(date, formatString = 'D MMMM YYYY') {
    if (!date) return '';
    return moment(date).format(formatString);
  },
  eventDate(date, formatString = 'MMMM D') {
    if (!date) return '';
    return moment(date).format(formatString);
  },
  eventDate2(date, formatString = 'D YYYY') {
    if (!date) return '';
    return moment(date).format(formatString);
  },
  time(rawValue = '00:00:00') {
    const values = rawValue.split(':');
    const [hours, minutes, seconds] = values;

    return {
      hours: parseInt(hours, 10),
      minutes: parseInt(minutes, 10),
      seconds: parseInt(seconds, 10),
    };
  },
  addAccents(searchText = '') {
    let pattern = searchText;

    pattern = pattern.replace(/a/gi, '[aá]');
    pattern = pattern.replace(/e/gi, '[eé]');
    pattern = pattern.replace(/i/gi, '[ií]');
    pattern = pattern.replace(/o/gi, '[oó]');
    pattern = pattern.replace(/u/gi, '[uú]');

    return pattern;
  },
  capitalize(text = '') {
    if (!text) {
      return '';
    }

    if (text.length === 1) {
      return text.toUpperCase();
    }

    const firstLetter = text.substring(0, 1);
    const remainingText = text.substring(1);
    return `${firstLetter.toUpperCase()}${remainingText.toLowerCase()}`;
  },
  isLeapYear(year) {
    const checkYear = (((year % 4 === 0) && (year % 100 !== 0)) || (year % 400 === 0)) ? 1 : 0;
    if (!checkYear) { return false; }
    return true;
  },
  monthOfYear: (index = 0) => {
    const months = [
      'January',
      'February',
      'March',
      'April',
      'May',
      'June',
      'July',
      'August',
      'September',
      'October',
      'November',
      'December'
    ];

    return months[index];
  },
  monthOfYearShort: (index = 0) => {
    const months = [
      'Jan',
      'Feb',
      'Mar',
      'Apr',
      'May',
      'Jun',
      'Jul',
      'Aug',
      'Sep',
      'Oct',
      'Nov',
      'Dec'
    ];

    return months[index];
  },
  momentCompare(left, right) {
    const leftDate = moment.utc(left.timeStamp);
    const rightDate = moment.utc(right.timeStamp);
    const diff = leftDate.diff(rightDate);
    return diff > 0;
  },
  setTimeToActive(start) {
    const currentDate = moment().tz('America/Mazatlan');
    const hour = currentDate.hour();
    let time = 0;

    if (hour > start) {
      const nextTime = 24 - hour;
      time = nextTime + start;
    } else {
      time = start - hour;
    }

    const activeTimeOut = 1000 * 60 * 60 * time;

    return {
      getTime: () => activeTimeOut,
      getHours: () => time
    };
  },
  setLabelTypeOfSale(type_sale_delivery_period) {
    let label = '';

    if (typeof type_sale_delivery_period === 'object') {
      label = `${this.capitalize(type_sale_delivery_period.type)}, ${type_sale_delivery_period.start}-${type_sale_delivery_period.end}`;
    }

    return label;
  },
  getPast5Years() {
    const currentDate = new Date();
    const currentYear = currentDate.getFullYear();
    const years = [];

    for (let index = 1; index <= 5; index++) {
      years.push(currentYear - index);
    }

    return years.reverse();
  },
  slugify(rawText) {
    let str = rawText;
    str = str.replace(/^\s+|\s+$/g, ''); // trim
    str = str.toLowerCase();

    // remove accents, swap ñ for n, etc
    const from = 'åàáãäâèéëêìíïîòóöôùúüûñç·/_,:;';
    const to = 'aaaaaaeeeeiiiioooouuuunc------';

    for (let i = 0, l = from.length; i < l; i++) {
      str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
    }

    str = str
      .replace(/[^a-z0-9 -]/g, '') // remove invalid chars
      .replace(/\s+/g, '-') // collapse whitespace and replace by -
      .replace(/-+/g, '-') // collapse dashes
      .replace(/^-+/, '') // trim - from start of text
      .replace(/-+$/, ''); // trim - from end of text

    return str;
  },
  getWeekNumber(date) {
    const oneJan = new Date(date.getFullYear(), 0, 1);
    const numberOfDays = Math.floor((date - oneJan) / (24 * 60 * 60 * 1000));
    return Math.ceil((date.getDay() + 1 + numberOfDays) / 7);
  }
};

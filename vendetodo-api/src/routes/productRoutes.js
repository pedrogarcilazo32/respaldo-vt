const express = require('express');
const routeHandler = require('./routeHandler');
const productController = require('../controllers/productController');

const router = express.Router();

router.post('/', async (req, res) => {
  routeHandler({
    req,
    res,
    action: productController.getProducts,
    errorLabel: 'error'
  });
});
router.post('/existenciapp', async (req, res) => {
  routeHandler({
    req,
    res,
    action: productController.getProductosApp,
    errorLabel: 'error'
  });
});

router.post('/actualizarProducto', async (req, res) => {
  routeHandler({
    req,
    res,
    action: productController.updateProduct,
    errorLabel: 'error'
  });
});

router.post('/agregarProducto', async (req, res) => {
  routeHandler({
    req,
    res,
    action: productController.saveProduct,
    errorLabel: 'error'
  });
});

router.post('/eliminarProducto', async (req, res) => {
  routeHandler({
    req,
    res,
    action: productController.deleteProduct,
    errorLabel: 'error'
  });
});

router.post('/buscarProducto', async (req, res) => {
  routeHandler({
    req,
    res,
    action: productController.searchProduct,
    errorLabel: 'error'
  });
});

router.get('/assets', async (req, res) => {
  routeHandler({
    req,
    res,
    action: productController.getAssets,
    errorLabel: 'error'
  });
});

router.get('/detalle-producto/:idProducto', async (req, res) => {
  routeHandler({
    req,
    res,
    action: productController.getDetalleProducto,
    errorLabel: 'error'
  });
});

router.get('/getProveedor/:idProducto', async (req, res) => {
  routeHandler({
    req,
    res,
    action: productController.getProveedor,
    errorLabel: 'error'
  });
});

router.get('/getProductosPorCategoria/:idCat', async (req, res) => {
  routeHandler({
    req,
    res,
    action: productController.getProductosPorCategoria,
    errorLabel: 'error'
  });
});

router.post('/save-banner', async (req, res) => {
  routeHandler({
    req,
    res,
    action: productController.saveBanner,
    errorLabel: 'error'
  });
});

router.post('/save-image-product', async (req, res) => {
  routeHandler({
    req,
    res,
    action: productController.saveProductoImage,
    errorLabel: 'error'
  });
});

router.get('/getProductosPorCategoriaNews/:idCat', async (req, res) => {
  routeHandler({
    req,
    res,
    action: productController.getProductosPorCategoriaNews,
    errorLabel: 'error'
  });
});

module.exports = router;

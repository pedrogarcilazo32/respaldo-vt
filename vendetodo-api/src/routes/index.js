const usersRoutes = require('./usersRoutes');
const productRoutes = require('./productRoutes');
const marcasRoutes = require('./marcasRoutes');
const categoryRoutes = require('./categoriesRoutes');
const suppliersRoutes = require('./suppliersRoutes');
const batchesRoutes = require('./batchesRoutes');
const carritoRoutes = require('./carritoRoutes');
const empleadoRoutes = require('./empleadoRoutes');
const rolesRoutes = require('./rolesRoutes');
const estanteRoutes = require('./estanteRoutes');
const signRoute = require('./signRoute');
const clienteRoute = require('./clienteRoute');
const bannerRoute = require('./bannerRoutes');
const paymentRoute = require('./paymentRoutes');
const ordenRoutes = require('./ordenRoutes');

module.exports = (app) => {
  app.use('/api/users', usersRoutes);
  app.use('/api/products', productRoutes);
  app.use('/api/marcas', marcasRoutes);
  app.use('/api/categories', categoryRoutes);
  app.use('/api/suppliers', suppliersRoutes);
  app.use('/api/batches', batchesRoutes);
  app.use('/api/carrito', carritoRoutes);
  app.use('/api/empleados',empleadoRoutes);
  app.use('/api/roles',rolesRoutes);
  app.use('/api/estante',estanteRoutes);
  app.use('/api/sign',signRoute);
  app.use('/api/cliente',clienteRoute);
  app.use('/api/banners',bannerRoute);
  app.use('/api/payment', paymentRoute);
  app.use('/api/ordenes',ordenRoutes);
};


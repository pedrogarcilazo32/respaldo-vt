const express = require('express');
const routeHandler = require('./routeHandler');
const carritoController = require('../controllers/carritoController');

const router = express.Router();

router.get('/:idCarrito', async (req, res) => {
  routeHandler({
    req,
    res,
    action: carritoController.getDetalleCarrito,
    errorLabel: 'error'
  });
});

router.post('/guardar-linea-carrito', async (req, res) => {
  routeHandler({
    req,
    res,
    action: carritoController.guardarLineaCarrito,
    errorLabel: 'error'
  });
});

router.get('/validar-existencia/:idCarrito', async (req, res) => {
  routeHandler({
    req,
    res,
    action: carritoController.validarExistencia,
    errorLabel: 'error'
  });
});

router.post('/guardar-venta', async (req, res) => {
  routeHandler({
    req,
    res,
    action: carritoController.guardarVenta,
    errorLabel: 'error'
  });
});

router.post('/eliminar-linea-carrito', async (req, res) => {
  routeHandler({
    req,
    res,
    action: carritoController.eliminarLineaCarrito,
    errorLabel: 'error'
  });
});


router.post('/proveedor-existencia', async (req, res) => {
  routeHandler({
    req,
    res,
    action: carritoController.getProveedorExistencia,
    errorLabel: 'error'
  });
});


module.exports = router;

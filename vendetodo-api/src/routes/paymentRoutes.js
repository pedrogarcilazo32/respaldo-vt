const express = require('express');
const routeHandler = require('./routeHandler');
const paymentController = require('../controllers/paymentController');

const router = express.Router();


router.post('/', async (req, res) => {
    routeHandler({
        req,
        res,
        action: paymentController.procesarPago,
        errorLabel: 'error'
    });
});

module.exports = router;

module.exports = async ({ req, res, action, errorLabel = { es: 'Hubo un error' } }) => {
  let message;

  if (typeof errorLabel === 'string') {
    message = errorLabel;
  } else {
    message = errorLabel['es'];
  }

  action(req, res).catch((error) => {
    res.send({
      isValid: false,
      message,
      error: 'Fallo en la conexion'
    });
  });

};

const express = require('express');
const routeHandler = require('./routeHandler');
const ordenController = require('../controllers/ordenController');
const router = express.Router();

router.post('/getOrden', async (req, res) => {
    console.log("api");
    routeHandler({
        req,
        res,
        action: ordenController.getOrdenes,
        errorLabel: 'error'
    });
});
router.post('/solicitarOrden', async (req, res) => {
    routeHandler({
        req,
        res,
        action: ordenController.solicitarOrden,
        errorLabel: 'error'
    });
});
router.post('/obtenerOrdenPendiente', async (req, res) => {
    routeHandler({
        req,
        res,
        action: ordenController.obtenerOrdenPendiente,
        errorLabel: 'error'
    });
});
router.post('/cambiarEstado', async (req, res) => {
    routeHandler({
        req,
        res,
        action: ordenController.cambiarEstado,
        errorLabel: 'error'
    });
});
router.post('/finalizarOrden', async (req, res) => {
    routeHandler({
        req,
        res,
        action: ordenController.finalizarOrden,
        errorLabel: 'error'
    });
});

module.exports = router;

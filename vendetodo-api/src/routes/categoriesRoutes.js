const express = require('express');
const routeHandler = require('./routeHandler');
const categoriesController = require('../controllers/categoriesController');

const router = express.Router();


router.get('/', async (req, res) => {
  routeHandler({
    req,
    res,
    action: categoriesController.getCategories,
    errorLabel: 'error'
  });
});

module.exports = router;

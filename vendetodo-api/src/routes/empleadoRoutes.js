const express = require('express');
const routeHandler = require('./routeHandler');
const empleadoController = require('../controllers/empleadoController');

const router = express.Router();


router.post('/', async (req, res) => {
  routeHandler({
    req,
    res,
    action: empleadoController.getEmpleados,
    errorLabel: 'error'
  });
});
router.post('/agregarEmpleado', async (req, res) => {
  routeHandler({
    req,
    res,
    action: empleadoController.guardarEmpleado,
    errorLabel: 'error'
  });
});
router.post('/buscarCorreo', async (req, res) => {
  routeHandler({
    req,
    res,
    action: empleadoController.searchEmail,
    errorLabel: 'error'
  });
});



module.exports = router;

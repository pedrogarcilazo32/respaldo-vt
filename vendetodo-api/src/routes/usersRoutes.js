const express = require('express');
const routeHandler = require('./routeHandler');
const usersController = require('../controllers/usersController')

const router = express.Router();

router.get('/by-id/:id', async (req, res) => {
  routeHandler({
    req,
    res,
    action: usersController.getUserById,
    errorLabel: 'error'
  });
});

router.post('/agregarUser', async (req, res) => {
    routeHandler({
        req,
        res,
        action: usersController.guardarCliente,
        errorLabel: 'error'
    });
});
router.post('/buscarCorreo', async (req, res) => {
    routeHandler({
      req,
      res,
      action: usersController.searchEmail,
      errorLabel: 'error'
    });
  });

module.exports = router;

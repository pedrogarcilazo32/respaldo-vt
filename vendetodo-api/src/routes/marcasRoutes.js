const express = require('express');
const routeHandler = require('./routeHandler');
const marcasController = require('../controllers/marcasController');

const router = express.Router();


router.get('/', async (req, res) => {
    routeHandler({
        req,
        res,
        action: marcasController.getMarcas,
        errorLabel: 'error'
    });
});

module.exports = router;

const express = require('express');
const routeHandler = require('./routeHandler');
const clienteController = require('../controllers/clienteController');
const router = express.Router();


router.post('/agregarCliente', async (req, res) => {
    routeHandler({
        req,
        res,
        action: clienteController.guardarCliente,
        errorLabel: 'error'
    });
});

module.exports = router;
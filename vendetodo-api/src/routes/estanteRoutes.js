const express = require('express');
const routeHandler = require('./routeHandler');
const estanteController = require('../controllers/estanteController');

const router = express.Router();
router.post('/getOrdenamiento', async (req, res) => {
    console.log("api");
    routeHandler({
        req,
        res,
        action: estanteController.getOrdenameinto,
        errorLabel: 'error'
    });
});
router.post('/', async (req, res) => {
    routeHandler({
        req,
        res,
        action: estanteController.getEstantes,
        errorLabel: 'error'
    });
});

router.post('/obtenerEstanteActual', async (req, res) => {
    routeHandler({
        req,
        res,
        action: estanteController.obtenerEstanteActual,
        errorLabel: 'error'
    });
});
router.post('/obtenerEstantePendiente', async (req, res) => {
    routeHandler({
        req,
        res,
        action: estanteController.obtenerEstantePendiente,
        errorLabel: 'error'
    });

});
router.post('/solicitarReorden', async (req, res) => {
    routeHandler({
        req,
        res,
        action: estanteController.solicitarReorden,
        errorLabel: 'error'
    });

});
router.post('/confirmarNuevoOrden', async (req, res) => {
    routeHandler({
        req,
        res,
        action: estanteController.confirmarNuevoOrden,
        errorLabel: 'error'
    });

});

module.exports = router;

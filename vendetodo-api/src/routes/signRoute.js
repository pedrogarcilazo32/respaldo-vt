const express = require('express');
const routeHandler = require('./routeHandler');
const signController = require('../controllers/signController');
const jwt = require('jsonwebtoken');
const router = express.Router();


router.post('/singup', async (req, res) => {
    routeHandler({
        req,
        res,
        action: signController.createSignUp,
        errorLabel: 'error'
    });
});

router.post('/signin', async (req, res) => {
    routeHandler({
        req,
        res,
        action: signController.signIn,
        errorLabel: 'error'
    });
});

router.post('/test',verifyToken, async (req, res) => {
    routeHandler({
        req,
        res,
        action: signController.test,
        errorLabel: 'error'
    });
});
function verifyToken(req,res,next){
    if(!req.headers.authorization) return res.status(401).json('No autorizado');
  
    const token = req.headers.authorization.substr(7);
    if(token!==''){
       const content= jwt.verify(token,'still')
       req.data = content;
       next();
    }else{
        res.status(401).json('Token Vacio');
    }
  }

module.exports = router;
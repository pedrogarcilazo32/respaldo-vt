const express = require('express');
const routeHandler = require('./routeHandler');
const batchesController = require('../controllers/batchesController');

const router = express.Router();


router.post('/', async (req, res) => {
    routeHandler({
        req,
        res,
        action: batchesController.addBatch,
        errorLabel: 'error'
    });
});
router.post('/getProductsBatches', async (req, res) => {
    routeHandler({
        req,
        res,
        action: batchesController.getProductsBatches,
        errorLabel: 'error'
    });
});

module.exports = router;

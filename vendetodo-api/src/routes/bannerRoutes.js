const express = require('express');
const routeHandler = require('./routeHandler');
const bannerController = require('../controllers/bannerController');
const jwt = require('jsonwebtoken');
const router = express.Router();

router.post('/save-banner', async (req, res) => {
  routeHandler({
      req,
      res,
      action: bannerController.saveBannerImage,
      errorLabel: 'error'
    });
});

router.get('/assets', async (req, res) => {
  routeHandler({
    req,
    res,
    action: bannerController.getAssetsBanner,
    errorLabel: 'error'
  });
});

module.exports = router;

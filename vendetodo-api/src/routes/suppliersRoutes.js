const express = require('express');
const routeHandler = require('./routeHandler');
const suppliersController = require('../controllers/suppliersController');

const router = express.Router();


router.get('/', async (req, res) => {
  routeHandler({
    req,
    res,
    action: suppliersController.getSuppliers,
    errorLabel: 'error'
  });
});

module.exports = router;

const express = require('express');
const routeHandler = require('./routeHandler');
const rolesController = require('../controllers/rolesController');

const router = express.Router();


router.post('/', async (req, res) => {
  routeHandler({
    req,
    res,
    action: rolesController.getRoles,
    errorLabel: 'error'
  });
});

module.exports = router;
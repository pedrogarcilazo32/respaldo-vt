module.exports = {
  saveLineaCarrito: "exec vt_sp_carrito 1 , @json",
  getDetalleCarrito: "exec vt_sp_carrito 2 , @json",
  validarCarrito: "exec vt_sp_carrito 3 , @json",
  guardarVenta: "exec vt_sp_carrito 4 , @json",
  eliminarLineaCarrito: "delete from detalle_carrito where id_detalle_carrito = @idLineaCarrito",
  proveedorExistencia: "exec vt_sp_proveedores 2, @json"
};

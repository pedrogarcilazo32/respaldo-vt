module.exports = {
  saveProduct: "exec vt_sp_cat_productos 1,@json",
  getAllProducts: "exec vt_sp_productos 2,@json",
  updateProduct: "exec vt_sp_cat_productos 3,@json",
  deleteProduct: "exec vt_sp_cat_productos 4,@json",
  searchProduct: "select * from vt_cat_productos where v_nombre like '%' +@name+ '%'",
  searchProduct2: "exec vt_sp_cat_productos 5,@json",
  getDetalleProducto: "exec vt_sp_cat_productos 6,@json",
  getProveedor: "exec vt_sp_proveedores 1,@json",
  getProductosPorCategoria: "exec vt_sp_cat_productos 7, @json",
  getProductosPorCategoriaNews: "exec vt_sp_cat_productos 8, @json",
  getAllEmpleados: "exec sp_empleados 2,@json",
  getAllRoles: "exec sp_roles 2,@json",
  searchEmail: "exec sp_empleados 3, @json",
  guardarEmpleado: "exec sp_empleados 1,@json",
  getEstantes: "exec sp_estante 2, @json",
  getExistencia: `select p.id_producto,p.nombre,p.descripcion,p.precio,disponible =sum( s.disponible_total),p.image_url
  from Producto_proveedor s left join Producto p on s.id_producto = p.id_producto
  group by p.id_producto,p.nombre,p.descripcion,p.precio,p.image_url order by p.id_producto desc`
}


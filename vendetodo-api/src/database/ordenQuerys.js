module.exports = {
    solicitarOrden: "exec vt_sp_ordenes 2,@json",
    obtenerOrdenPendiente: "exec vt_sp_ordenes 3,@json",
    cambiarEstado: "exec vt_sp_ordenes 4,@json",
    finalizarOrden: "exec vt_sp_ordenes 5,@json",
    getAllOrders: "exec vt_sp_ordenes 6,@json",
}

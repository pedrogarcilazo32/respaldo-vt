const sql = require('mssql');
const { dbSettings } = require('../utils/config');

const optionsConection = {
  user: dbSettings.user,
  password: dbSettings.password,
  server: dbSettings.server,
  database: dbSettings.database,
  ...dbSettings.options
};

module.exports = {
  async getConnection() {
    try {
      const pool = await sql.connect(optionsConection);
      return pool;
    } catch (error) {
      console.error(error);
    }
  }
};


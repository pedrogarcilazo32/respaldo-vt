module.exports = {
    getAllOrdenamientos: "exec vt_sp_estantes 2,@json",
    obtenerEstanteActual: "exec vt_sp_estantes 5,@json",
    obtenerEstantePendiente: "exec vt_sp_estantes 6,@json",
    solicitarReorden: "exec vt_sp_estantes 7,@json",
    confirmarNuevoOrden: "exec vt_sp_estantes 8,@json",
    getEstantes: "exec sp_empleados 6, @json",
  }


import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-skeleton-dashboard',
  templateUrl: './skeleton-dashboard.component.html',
  styleUrls: ['./skeleton-dashboard.component.scss'],
})
export class SkeletonDashboardComponent implements OnInit {

  constructor() { }

  ngOnInit() {}


  getMinHeight() {
    const mainFooter = document.getElementById('main-footer');
    let mainFooterHeight = 0;
    if (mainFooter) {
      mainFooterHeight= mainFooter.offsetHeight;
    }

    const windowHeight = window.innerHeight;

    return windowHeight - mainFooterHeight;
  }
}

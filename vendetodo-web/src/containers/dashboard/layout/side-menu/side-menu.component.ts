import { Component, OnInit } from '@angular/core';
import decode from 'jwt-decode';
import { Constants } from 'src/utils/constans.utils';

@Component({
  selector: 'app-side-menu',
  templateUrl: './side-menu.component.html',
  styleUrls: ['./side-menu.component.scss'],
})
export class SideMenuComponent implements OnInit {
  Options: any = [];

  constructor(private constants: Constants) {
    this.constants = constants;
  }

  ngOnInit() {
    this.getOptions();
  }

  getOptions() {
    const VENDETODO_SESSION = this.constants.getConstants().VENDETODO_SESSION;

    const token = localStorage.getItem(VENDETODO_SESSION);
    let data: any;
    if (token) {
      data = decode(token);
    }

    if (data) {
      const { nombre } = data[0];

      if (nombre === 'estantero') {
        this.Options = [
          {
            label: 'Ordenar Estante',
            value: 'ordenar-estante',
            icon: 'cube-outline',
          },
        ];
      }
    } else {
      this.Options = [
        {
          label: 'Surtir orden',
          value: 'surtir-orden',
          icon: 'cube-outline',
        },
        {
          label: 'Historial ordenes',
          value: 'historial',
          icon: 'time-outline',
        },
      ];
    }
  }
}

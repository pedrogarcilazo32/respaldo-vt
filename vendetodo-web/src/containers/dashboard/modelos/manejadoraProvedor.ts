import { Injectable } from '@angular/core';
import { Proveedor } from 'src/containers/dashboard/models/Proveedor';
import { ProveedorService } from '../services/provedor.services';
@Injectable({
  providedIn: 'root',
})
export class ManejadoraProvedor {

  constructor(
    private provedorService: ProveedorService
  ) {
    this.provedorService = provedorService;
  }



  public async getSuppliers(): Promise<Array<Proveedor>> {
    const data = await this.provedorService.getSupplier();

    return data;

  }

}

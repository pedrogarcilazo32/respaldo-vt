import { Injectable } from '@angular/core';
import { Producto } from '../models/Producto';
import { Proveedor } from '../models/Proveedor';
import { FormatUtils } from '../../../utils/format.utils';
import { productService } from '../services/productos.services';
import { filtroProducts } from '../models/filtroProducts.model';
import { categoria } from '../models/categoria.model';
import { marca } from '../models/marca.models';
@Injectable({
  providedIn: 'root',
})
export class ManejadoraProducto {
  pagina: number = 1;
  constructor(
    private format: FormatUtils,
    private productService: productService
  ) {
    this.productService = productService;
    this.format = format;
  }

  public async obtenerProductos(pagina: number) {
    const arrayProductos = Array<Producto>();
    const data = await this.productService.getProducts(pagina);

    data.forEach((fh: any) => {
      const aux = new Producto(
        fh.nombre,
        fh.descripcion,
        fh.precio,
        fh.volumen,
        fh.image_url,
        fh.id_producto,
        fh.id_categoria,
        fh.id_marca,
        fh.nombreCategoria,
        fh.nombreMarca
      );

      arrayProductos.push(aux);
    });
    return arrayProductos;
  }

  public async obtenerDetalleProducto(id_producto: string) {
    let data: any = await this.productService.obtenerDetalleProducto(
      id_producto
    );
    data = data[0];
    const producto : Producto = new Producto(
      data.nombre,
      data.descripcion,
      data.precio,
      data.volumen,
      data.image_url,
      data.id_producto,
      data.id_categoria,
      data.id_marca,
      data.nombreCategoria,
      data.nombreMarca
    );

    return producto;
  }

  public async buscarProducto(dataBuscar: any) {
    const arrayProductos = Array<Producto>();
    const data: any = await this.productService.buscarProducto(dataBuscar);


    data.forEach((fh: any) => {
      const aux = new Producto(
        fh.nombre,
        fh.descripcion,
        fh.precio,
        fh.volumen,
        fh.image_url,
        fh.id_producto,
        fh.id_categoria,
        fh.id_marca,
        fh.nombreCategoria,
        fh.nombreMarca
      );

      arrayProductos.push(aux);
    });

    return arrayProductos;
  }

  public async getProveedor(id_producto: string) {
    let dataProv: any = await this.productService.getProveedor(id_producto);
    const arrayProv = Array<Proveedor>();
    dataProv.forEach((currentProv) => {
      //Extraer proveedor
      const prov = new Proveedor(
        currentProv.nombre,
        currentProv.id_proveedor,
        currentProv.telefono,
        currentProv.email
      );

      arrayProv.push(prov);
    });

    return arrayProv;
  }

  public async obtenerProductosPorCategoria(idCat: number) {
    const arrayProdCat = Array<Producto>();
    const data = await this.productService.getProductosPorCategoria(idCat);
    console.log('categoria', data);
    data.forEach((fh: any) => {
      const aux = new Producto(
        fh.nombre,
        fh.descripcion,
        fh.precio,
        fh.volumen,
        fh.image_url,
        fh.id_producto,
        fh.id_categoria,
        fh.id_marca,
        fh.nombreCategoria,
        fh.nombreMarca
      );

      arrayProdCat.push(aux);
    });
    return arrayProdCat;
  }

  public async obtenerProductosPorCategoriaNews(idCat: number) {
    const arrayProdCat = Array<Producto>();
    const data = await this.productService.obtenerProductosPorCategoriaNews(idCat);

    data.forEach((fh: any) => {
      const aux = new Producto(
        fh.nombre,
        fh.descripcion,
        fh.precio,
        fh.volumen,
        fh.image_url,
        fh.id_producto,
        fh.id_categoria,
        fh.id_marca,
        (fh.nombreCategoria = ''),
        fh.nombreMarca,
        fh.estatus,
        fh.descuento
      );

      arrayProdCat.push(aux);
    });
    return arrayProdCat;
  }

  public Obtenerpagina(opc: number) {
    if (opc == 1) {
      this.pagina = this.pagina + 1;
    } else if (opc == 2) {
      this.pagina = 1;
    }

    return this.pagina;
  }
}

import { Injectable } from '@angular/core';
import { Rol } from '../models/rol';
import { rolService } from '../services/rol.services';

@Injectable({
  providedIn: 'root',
})
export class RolModel {
  pagina: number = 1;
  constructor(
    private rolservice: rolService
  ) {}

  
  public async obtenerRol(): Promise<Array<Rol>> {
    const data =  await this.rolservice.obtenerRol();
    return data;
  }
}

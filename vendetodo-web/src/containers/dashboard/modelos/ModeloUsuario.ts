import { Injectable } from '@angular/core';
import { Usuario } from '../models/usuario';
import { FormatUtils } from '../../../utils/format.utils';
import { usuariosService } from '../services/usuarios.services';
@Injectable({
  providedIn: 'root',
})
export class ModeloUsuario {
  constructor(
    private format: FormatUtils,
    private usuariosService: usuariosService
  ) {
    this.format = format;
    this.usuariosService = usuariosService;

  }

  public async obtenerUsuario(session: number) {
    const userData = await this.usuariosService.ObtenerUsuario(session);
    const user = new Usuario(userData[0].id_usuario, userData[0].email, userData[0].contraseña, userData[0].id_role);

    return user;
  }
}

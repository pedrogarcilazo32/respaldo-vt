import { Injectable } from '@angular/core';
import { FormatUtils } from '../../../utils/format.utils';
import { Empleado } from '../models/empleado.model';
import { empleadoService } from '../services/empleado.services';
import { Rol } from 'src/containers/dashboard/models/rol';
import { Estante } from 'src/containers/dashboard/models/estante.model';
import { Usuario } from 'src/containers/dashboard/models/usuario';
import { Direccion } from 'src/containers/dashboard/models/direccion';
@Injectable({
  providedIn: 'root',
})
export class EmpleadoModel {
  pagina: number = 1;
  constructor(
    private format: FormatUtils,
    private empleadoService: empleadoService
  ) {}

  public Obtenerpagina(opc: number) {
    if (opc == 1) {
      this.pagina = this.pagina + 1;
    } else if (opc == 2) {
      this.pagina = 1;
    }
    return this.pagina;
  }
  public async obtenerEmpleados(pagina: number): Promise<Array<Empleado>> {
    const data =  await this.empleadoService.getEmpleados(pagina);
    return data;
  }

   public validarCorreo(correo: string) {
    return new Promise((resolve, reject) => {
      this.empleadoService
        .getCorreo(correo)
        .then((response: Array<Usuario>) => {
          resolve(response);
        })
        .catch((e) => reject(e));
    });
  }
  agregarEmpleado(
    email,
    contra,
    nombre,
    ape_paterno,
    ape_materno,
    telefono,
    cp,
    calle,
    ciudad,
    no_exterior,
    id_estante,
    nombre_rol
  ) {
    return new Promise((resolve, reject) => {
      let objRol = new Rol(null, nombre_rol);

      let objUsu = new Usuario(
        0,
        email,
        contra,
        objRol
      );
      let objEstante = new Estante(id_estante, null);
      let objDir = new Direccion(
        null,
        Number(cp),
        calle,
        ciudad,
        no_exterior
      );
      let obj = new Empleado(
        objUsu,
        nombre,
        ape_paterno,
        ape_materno,
        Number(telefono),
        null,
        objDir,
        objEstante
      );
      console.log(obj);

      this.empleadoService
        .agregarEmpleado(obj)
        .then((response: Array<Empleado>) => {
          resolve(response);
        })
        .catch((e) => reject(e));
    });
  }
}

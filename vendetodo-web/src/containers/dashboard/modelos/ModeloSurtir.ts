import { Injectable } from '@angular/core';
import { surtirServices } from '../services/SurtirServices';
import { EventManager } from 'src/utils/event-manager';
import { Orden } from '../models/Orden';

@Injectable({
  providedIn: 'root',
})
export class ModeloSurtir {
  eventManager: EventManager;

  constructor(private surtirServices: surtirServices) {
    this.surtirServices = surtirServices;
  }

  public async obtenerOrdenes(idUsuario: number) {
    const arrayOrdenes = Array<Orden>();
    const data : any = await this.surtirServices.getOrden(idUsuario);
    data.forEach((fh: any) => {
      const aux = new Orden();
      aux.setfolio_venta = fh.folio_venta;
      aux.setnombreEmpleado = fh.nombreEmpleado;
      aux.setfecha_inicio = fh.fecha_inicio;
      aux.setfecha_fin = fh.fecha_fin;
      arrayOrdenes.push(aux);
    });
    return arrayOrdenes;
  }

  crearOrden(data: any, id_usuario: number, estante_id: number) {
    const ordenObj = new Orden('', '', 0, id_usuario);
    ordenObj.setEstante_id = estante_id;
    let id_orden;
    data.forEach((fh: any) => {
      id_orden = fh.idOrden;
      ordenObj.crearDetalleOrden(
        fh.idDetalleOrden,
        fh.nombre_producto,
        fh.nombre_provedor,
        fh.cantidad,
        fh.id_estante,
        fh.id_seccion,
        fh.estatus,
        fh.estaenBodega
      );
      ordenObj.setfecha_inicio = fh.fecha_registro;
      ordenObj.setIdOrden = id_orden;
    });
    ordenObj.crearRuta();

    return ordenObj;
  }

  public async solicitarOrden(
    id_usuario: number,
    estante_id: number
  ): Promise<Orden> {
    const data = await this.surtirServices.solicitarOrden(id_usuario);

    if (data.length) {
      return this.crearOrden(data, id_usuario, estante_id);
    }
    return null;
  }

  public async obtenerOrdenPendiente(
    id_usuario: number,
    estante_id: number
  ): Promise<Orden> {
    const data = await this.surtirServices.obtenerOrdenPendiente(id_usuario);
    if (data.length) {
      return this.crearOrden(data, id_usuario, estante_id);
    }
    return null;
  }

  public async cambiarEstado(ord: Orden, idDetalleOrden: number): Promise<any> {
    var detalle = ord.cambiarEstado(idDetalleOrden);
    const data = await this.surtirServices.cambiarEstado(detalle);

    return data;
  }

  public async finalizarOrden(Ord: Orden): Promise<any> {
    Ord.finalizaOrden();
    const data = await this.surtirServices.finalizarOrden(Ord);

    return data;
  }
}

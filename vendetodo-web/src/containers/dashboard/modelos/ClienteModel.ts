import { Injectable } from '@angular/core';
import { Cliente } from '../models/cliente';
import { Direccion } from '../models/direccion';
import { Rol } from '../models/rol';
import { Usuario } from '../models/usuario';
import { clienteService } from '../services/cliente.services';

@Injectable({
  providedIn: 'root',
})
export class ClienteModel {
  pagina: number = 1;
  constructor(private clienteService: clienteService) {}
  comprobarUsuario(email:string, contra:string) {
    return new Promise((resolve, reject) => {
      let usuario = new Usuario(0, email, contra, null);

      this.clienteService
        .recibirToken(usuario)
        .then((response: Array<Usuario>) => {
          resolve(response);
        })
        .catch((e) => reject(e));
    });
  }
  agregarUsuario(
    email,
    contra,
    nombre,
    ape_paterno,
    ape_materno,
    telefono,
    cp,
    calle,
    ciudad,
    no_exterior
  ) {
    return new Promise((resolve, reject) => {
      let obj = new Cliente(nombre, ape_paterno, ape_materno, telefono);

      obj.obj_Direccion = new Direccion();
      obj.obj_Direccion.Cp = cp;
      obj.obj_Direccion.Calle = calle;
      obj.obj_Direccion.Ciudad = ciudad;
      obj.obj_Direccion.no_Exterior = no_exterior;
      let rol = new Rol(4, 'cliente');
      obj.Usuario = new Usuario(0, email, contra, rol);
      console.log(obj);

      this.clienteService
        .agregarCliente(obj)
        .then((response: Array<Cliente>) => {
          resolve(response);
        })
        .catch((e) => reject(e));
    });
  }

  public validarCorreo(correo: string) {
    return new Promise((resolve, reject) => {
      this.clienteService
        .getCorreo(correo)
        .then((response: Array<Usuario>) => {
          resolve(response);
        })
        .catch((e) => reject(e));
    });
  }
}

import { Injectable } from '@angular/core';
import { Batch } from '../models/batch.model';
import { BatchService } from '../services/batch.services';
import { Producto } from '../models/Producto.model';

@Injectable({
  providedIn: 'root',
})
export class ManejadoraBatch {

  constructor(
    private BatchService: BatchService
  ) {
    this.BatchService = BatchService;
  }
  public addBatch(lote: Batch) {
    return new Promise((resolve, reject) => {
      this.BatchService
        .addBatch(lote)
        .then((response: Array<Batch>) => {
          resolve(response);
        })
        .catch((e) => reject(e));
    });
  }
  public async getProductsBatches(producto:Producto): Promise<Array<Batch>> {
    const arrayBatches = Array<Batch>();
    const data = await this.BatchService.getProductsBatches(producto);
    console.log('data',data)
    data.forEach((fh: any) => {

      const aux = new Batch(
        fh.id_lote,
        fh.id_provedor,
        fh.id_producto,
        fh.cantidad_ent,
        fh.comprometido,
        fh.salida_pro,
        fh.disponible,
        fh.i_usuario_id,
        fh.fecha_registro,
        '',
        fh.v_nombreProvedor
      );


      arrayBatches.push(aux);

    });
    return arrayBatches;

  }

}

import { Injectable } from '@angular/core';
import { Estante } from '../models/estante.model';
import { estanteServices } from '../services/estante.services';

@Injectable({
  providedIn: 'root',
})
export class EstanteModel {
  pagina: number = 1;
  constructor(
    private estanteservices: estanteServices
  ) {}

  public Obtenerpagina(opc: number) {
    if (opc == 1) {
      this.pagina = this.pagina + 1;
    } else if (opc == 2) {
      this.pagina = 1;
    }
    return this.pagina;
  }
  public async obtenerEstantes(): Promise<Array<Estante>> {
    const data : any =  await this.estanteservices.obtenerEstantes();
    return data;
  }
}

import { Injectable } from '@angular/core';
import { Estante } from '../models/estante.model';
import { Producto } from '../models/Producto.model';
import { Seccion } from '../models/seccion.model';
import { estanteServices } from '../services/estante.services';
@Injectable({
  providedIn: 'root',
})
export class modeloEstante {
  constructor(private estanteServices: estanteServices) {
    this.estanteServices = estanteServices;
  }

  crearEstante(data: any, id_estante: number) {
    const estanteObj = new Estante(id_estante);
    // const arraySecciones = Array<Seccion>();
    // estanteObj.Secciones = arraySecciones;
    // var ultimoOrdenamiento = data[0].ultimo_ordenamiento;
    data.forEach((fh: any) => {
      // estanteObj.SeccionEstante = new Seccion(
      //   fh.id_seccion,
      //   fh.capacidad_seccion,
      //   fh.num_seccion,
      //   fh.cantidad_otorgada
      // );
      // estanteObj.SeccionEstante.productoSeccion = new Producto();
      // estanteObj.SeccionEstante.productoSeccion.nombreProducto =
      //   fh.nombre_producto;
      // estanteObj.SeccionEstante.productoSeccion.image_url = fh.image_url;
      // ultimoOrdenamiento = fh.ultimo_ordenamiento;
      // estanteObj.Secciones.push(estanteObj.SeccionEstante);
      estanteObj.crearSeccion(fh.id_seccion,fh.capacidad_seccion,fh.num_seccion, fh.cantidad_otorgada,fh.nombre_producto,fh.image_url);
      estanteObj.ultimoOrdenamiento=fh.ultimo_ordenamiento
    });
    // estanteObj.ultimoOrdenamiento = ultimoOrdenamiento;
    // estanteObj.Secciones = arraySecciones;
    return estanteObj;
  }

  public async obtenerOrdenamientos(idEstante: number) {
    console.log("en el modelo");
    const arrayOrdenes = Array<Estante>();
    const data : any = await this.estanteServices.getOrdenamientos(idEstante);
    console.log(data);
    data.forEach((fh: any) => {
      const aux = new Estante();
      aux.idEstante = fh.id_estante;
      aux.setnombreEmpleado = fh.nombreEmpleado;
      aux.setfecha_inicio = fh.fecha_inicio;
      aux.setfecha_fin = fh.fecha_fin;
      arrayOrdenes.push(aux);
    });
    return arrayOrdenes;
  }

  public async obtenerEstanteActual(id_estante: number): Promise<Estante> {
    const data = await this.estanteServices.obtenerEstanteActual(id_estante);
    console.log('data estante', data);
    if (data) {
      return this.crearEstante(data, id_estante);
    }

    return null;
  }

  public async obtenerEstantePendiente(id_estante: number): Promise<Estante> {
    const data = await this.estanteServices.obtenerEstantePendiente(id_estante);
    if (data) {
      return this.crearEstante(data, id_estante);
    }
    return null;
  }

  public async solicitarReorden(id_estante: number): Promise<Estante> {
    const data = await this.estanteServices.solicitarReorden(id_estante);
    if (data) {
      return this.crearEstante(data, id_estante);
    }
    return null;
  }

  public confirmarNuevoOrden(id_estante: number) {
    return new Promise((resolve, reject) => {
      this.estanteServices
        .confirmarNuevoOrden(id_estante)
        .then((response: any) => {
          resolve(response);
        })
        .catch((e) => reject(e));
    });
  }

  public async obtenerEstantes(): Promise<Array<Estante>> {
    const data =  await this.estanteServices.obtenerEstantes();
    return data;
  }
}

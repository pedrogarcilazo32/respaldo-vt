import { Injectable } from '@angular/core';
import { Payment } from '../models/Pago';

@Injectable({
  providedIn: 'root',
})
export class ModeloPayment {
  pagina: number = 1;
  constructor() {}


  guardarDatosPago(datosPago: any){
    const pago = new Payment(datosPago.creditCard, datosPago.nombreApellido, datosPago.fechaExp, datosPago.cvv);
    return pago;
  }
}

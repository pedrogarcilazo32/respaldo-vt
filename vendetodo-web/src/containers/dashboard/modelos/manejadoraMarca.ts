import { Injectable } from '@angular/core';
import { marca } from '../models/marca.models';
import { FormatUtils } from '../../../utils/format.utils';
import { marcasService } from '../services/marcas.services';

@Injectable({
  providedIn: 'root',
})
export class manejadoraMarca {
  constructor(
    private format: FormatUtils,
    private marcasService: marcasService
  ) {
    this.format=format;
    this.marcasService=marcasService;
  }

  public obtenerMarcas() {
    return new Promise((resolve, reject) => {
      this.marcasService
        .obtenerMarcas()
        .then((response: Array<marca>) => {
          resolve(response);
        })
        .catch((e) => reject(e));
    });
  }
}

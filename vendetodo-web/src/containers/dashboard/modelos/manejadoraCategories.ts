import { Injectable } from '@angular/core';
import { FormatUtils } from '../../../utils/format.utils';
import { categoria } from '../models/categoria.model';
import { categoriesService } from '../services/categories.services';
@Injectable({
  providedIn: 'root',
})
export class manejadoraCategorias {
  constructor(
    private format: FormatUtils,
    private categoriesService: categoriesService
  ) {
    this.format=format;
    this.categoriesService=categoriesService;
  }

  public obtenerCategorias() {
    return new Promise((resolve, reject) => {
      this.categoriesService
        .getCategories()
        .then((response: Array<categoria>) => {
          resolve(response);
        })
        .catch((e) => reject(e));
    });
  }
}

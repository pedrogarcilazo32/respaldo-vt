import { Injectable } from '@angular/core';
import { CarritoServices } from '../services/CarritoServices';
import { Carrito } from '../models/Carrito';
import { Producto } from '../models/Producto';
import { Proveedor } from '../models/Proveedor';
import { EventManager } from 'src/utils/event-manager';
import { Storage } from 'src/utils/storage';
import { Pago } from '../models/Pago';
import { Constants } from 'src/utils/constans.utils';
import decode from 'jwt-decode';
import { Venta } from '../models/Venta';

@Injectable({
  providedIn: 'root',
})
export class ModeloCarrito {
  carritoServices: CarritoServices;
  carrito: Carrito;
  eventManager: EventManager;

  constructor(
    carritoServices: CarritoServices,
    eventManager: EventManager,
    private storage: Storage,
    private constants: Constants,
  ) {
    this.constants = constants;
    this.eventManager = eventManager;
    this.carritoServices = carritoServices;
    this.storage = storage;
    this.carrito = this.storage.getData('carrito');
  }

  public async crearLineaCarrito(prod: Producto, cant: number, prov: Proveedor) {
    const LC = this.carrito.crearLineaCarrito(prod, cant, prov, 5); //Crear LineaCarrito
    this.eventManager.emit('agregar-carrito', true);
    const params = {
      idCarrito: this.carrito.getIdCarrito,
      LC,
    };

    await this.carritoServices.guardarLC(params); //Enviar LineaCarrito al servicio
    this.eventManager.emit('agregar-carrito', false);
  }

  public async crearLineasCarrito() {
    const idCarrito = this.carrito.getIdCarrito;
    if (idCarrito) {
      const datosLC: any = await this.carritoServices.getLineasCarrito(
        idCarrito
      );
      if (datosLC) {
        datosLC.forEach((currentLC) => {
          //Extraer producto
          const prod = new Producto(
            currentLC.nombre,
            currentLC.descripcion,
            currentLC.precio,
            currentLC.volumen,
            currentLC.image_url,
            currentLC.id_producto
          );

          //Extraer proveedor
          const prov = new Proveedor(currentLC.nombreProveedor, currentLC.id_proveedor);

          //Crear LineaCarrito
          this.carrito.crearLineaCarrito(
            prod,
            currentLC.cantidad,
            prov,
            currentLC.id_detalle_carrito,
            currentLC.disponible_total
          );
        });

        return this.carrito.getLineasCarrito;
      }
    }

    return null;
  }

  public getLineasCarrito() {
    if (this.carrito) {
      return this.carrito.getLineasCarrito;
    }
  }

  public obtenerTotal() {
    return this.carrito.obtenerTotal();
  }

  public async confirmaVenta() {
    // const venta: Venta = this.carrito.crearNuevaVenta();
    // await this.carritoServices.confirmaVenta(venta); //Enviar Venta al servicio
  }

  public async getNumLineasCarrito() {
    const arrayLineasCarrito = await this.crearLineasCarrito();
    return arrayLineasCarrito.length;
  }

  private async comprobarExistencia() {
    const existencias: any =
      await this.carritoServices.comprobarExistenciaProductos(
        this.carrito.getIdCarrito
      );
    if (existencias) {
      existencias.forEach((mp) => {
        this.carrito.setearNuevaCantidad(
          mp.id_detalle_carrito,
          mp.disponible_total
        );
      });
    }
  }

  public async pagar(datosPago: Pago) {
    await this.comprobarExistencia();

    if (this.carrito.checarExistenciaMinima()) {
      const params = {
        datosPago,
        carrito: this.carrito
      };


      const procesado: any = await this.carritoServices.procesarPago(params);

      if (procesado) {
        const token = localStorage.getItem(
          this.constants.getConstants().VENDETODO_SESSION
        );

        datosPago.setReferencia = procesado.referencia;

        const data = decode(token);
        const { id_usuario } = data[0];
        const venta: Venta = this.carrito.crearNuevaVenta(id_usuario, datosPago.getReferencia);
        // // venta.pasarLineasCarritoAVenta(this.carrito.getLineasCarrito);

        this.carritoServices.guardarVenta({...venta, referencia: procesado.referencia });
      }
    }
  }

  guardarDatosPago(datosPago: any) {
    const token = localStorage.getItem(
      this.constants.getConstants().VENDETODO_SESSION
    );
    const data = decode(token);
    const { id_usuario } = data[0];

    const pago = new Pago(
      id_usuario,
      datosPago.creditCard,
      datosPago.nombreApellido,
      datosPago.fechaExp,
      datosPago.cvv
    );
    return pago;
  }

  async eliminarProducto(idLineaCarrito) {
    await this.carritoServices.eliminarProducto(idLineaCarrito);
  }

  async getProveedorMayorExistencia(id_producto){
    const dataP: any = await this.carritoServices.getProveedorMayorExistencia(id_producto);
    console.log('data', dataP);
    const proveedor = new Proveedor('nada', dataP[0].id_proveedor);
    console.log(proveedor);
    return proveedor;
  }
}

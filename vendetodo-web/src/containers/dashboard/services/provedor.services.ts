import { Injectable } from '@angular/core';
import { Constants } from '../../../utils/constans.utils';
import { Proveedor } from '../models/Proveedor';
@Injectable({ providedIn: 'root' })
export class ProveedorService {
  private constants: Constants;
  private url: string;
  constructor(constants: Constants) {
    this.constants = constants;

    this.url = this.constants.getUrl();
  }


  public async getSupplier(): Promise<Array<Proveedor>> {

    return new Promise(async (resolve) => {
      const arrayProveedores = Array<Proveedor>();

      const response = await fetch(this.url + '/api/suppliers', {

        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
        },
      });

      const respon = await response.json();

      if (respon.isValid) {

        respon.data.forEach((fh: any) => {

          const aux = new Proveedor(
            fh.id_proveedor,
            fh.nombre
          );


          arrayProveedores.push(aux);

        });

        resolve(arrayProveedores);

      }
    });
  }







}




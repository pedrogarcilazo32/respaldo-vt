import { Injectable } from '@angular/core';
import { Constants } from '../../../utils/constans.utils';
import { Empleado } from '../models/empleado.model';
import { Direccion } from '../models/direccion';
import { Estante } from '../models/estante.model';
import { Rol } from '../models/rol';
import { Usuario } from '../models/usuario';


@Injectable({ providedIn: 'root' })
export class empleadoService {
  private constants: Constants;
  private url: string;
  
  constructor(constants: Constants) {
    this.constants = constants;

    this.url = this.constants.getUrl();
  }

  public getCorreo(correo: string) {
    const params ={
      correo
    }
    return new Promise(async (resolve, reject) => {
      const arrayUsuarios = Array<Usuario>();
      const response = await fetch(this.url + '/api/users/buscarCorreo', {
        body: JSON.stringify(params),
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
      });

      const respon = await response.json();

      if (respon.isValid) {
        
        respon.data.forEach((fh: any) => {
          const auxUsuario = new Usuario(
            fh.id_usuario,
            fh.correo,
            fh.contra,
            null
          );
         
          arrayUsuarios.push(auxUsuario);
        });
        
        resolve(arrayUsuarios);
      }
    });
  }
  public async getEmpleados(pagina: number): Promise<Array<Empleado>> {
    const params = {
      pagina,
    };

    return new Promise(async (resolve) => {
      const arrayEmpleados = Array<Empleado>();

      const response = await fetch(this.url + '/api/empleados', {
        body: JSON.stringify(params),
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
      });

      const respon = await response.json();

      if (respon.isValid) {
       
        respon.data.forEach((fh: any) => {

          const auxRol = new Rol(
            fh.id_role,
            fh.rol
          );
           
          const auxUsuario = new Usuario(
            fh.id_usuario,
            fh.correo,
            fh.contra,
            auxRol
          );
          const auxEstan = new Estante(
            fh.no_estante,
            null
          );

          const auxDir = new Direccion(
            fh.id_direccion,
            fh.cp,
            fh.calle,
            fh.ciudad,
            fh.no_exterior
          );
          
          const aux = new Empleado(
            auxUsuario,
            fh.nombre,
            fh.ape_paterno,
            fh.ape_materno,
            fh.telefono,
            fh.total_paginas,
            auxDir,
            auxEstan
          );
            

          arrayEmpleados.push(aux);
          
        });
        
        resolve(arrayEmpleados);
       
      }
    });
  }
  public agregarEmpleado(pro: Empleado) {
    return new Promise(async (resolve, reject) => {
      const response = await fetch(this.url + '/api/empleados/agregarEmpleado', {
        body: JSON.stringify(pro),
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
      });

      if (response.status === 200) {
        const data = await response.json();
        if (data) {
          resolve(data);
        }
      } else {
        reject('No se pudo registrar el Cliente');
      }
    });
  }
}
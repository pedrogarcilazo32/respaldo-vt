import { Injectable } from '@angular/core';
import { Constants } from '../../../utils/constans.utils';
import { Producto } from '../models/Producto.model';
import { filtroProducts } from '../../dashboard/models/filtroProducts.model';
//import { dataFile } from '../models/dataFile.model';
@Injectable({ providedIn: 'root' })
export class productService {
  private constants: Constants;
  private url: string;
  constructor(constants: Constants) {
    this.constants = constants;

    this.url = this.constants.getUrl();
  }

  public async getProducts(pagina: number): Promise<Array<Producto>> {
    const params = {
      pagina,
    };

    return new Promise(async (resolve) => {
      // const arrayProductos = Array<Producto>();

      const response = await fetch(this.url + '/api/products', {
        body: JSON.stringify(params),
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
      });

      const respon = await response.json();

      if (respon.isValid) {
        resolve(respon.data);
      }
    });
  }

  public eliminarProducto(pro: Producto) {
    return new Promise(async (resolve, reject) => {
      const response = await fetch(
        this.url + '/api/products/eliminarProducto',
        {
          body: JSON.stringify(pro),
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
        }
      );

      if (response.status === 200) {
        const data = await response.json();
        if (data) {
          resolve(data);
        }
      } else {
        reject('No se pudo borrar el Producto');
      }
    });
  }

  public agregarProducto(pro: Producto) {
    return new Promise(async (resolve, reject) => {
      const response = await fetch(this.url + '/api/products/agregarProducto', {
        body: JSON.stringify(pro),
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
      });

      if (response.status === 200) {
        const data = await response.json();
        if (data) {
          resolve(data);
        }
      } else {
        reject('No se pudo agregar el Producto');
      }
    });
  }

  public actualizarProducto(pro: Producto) {
    console.log('lo que envio',pro)
    return new Promise(async (resolve, reject) => {
      const response = await fetch(
        this.url + '/api/products/actualizarProducto',
        {
          body: JSON.stringify(pro),
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
        }
      );

      if (response.status === 200) {
        const data = await response.json();
        if (data) {
          resolve(data);
        }
      } else {
        reject('No se pudo actualizar el Producto');
      }
    });
  }

  public buscarProducto(data) {
    return new Promise(async (resolve) => {
      const response = await fetch(this.url + '/api/products/buscarProducto', {
        body: JSON.stringify(data),
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
      });

      const respon = await response.json();

      if (respon.isValid) {
        resolve(respon.data);
      }
    });
  }

  public async obtenerDetalleProducto(id_producto: string) {
    return new Promise(async (resolve) => {
      const response = await fetch(this.url + `/api/products/detalle-producto/${id_producto}`, {
        method: 'get',
        headers: {
          'Content-Type': 'application/json',
        },
      });

      const respon = await response.json();

      if (respon.isValid) {
        resolve(respon.data);
      }
    });
  }

  public async getProveedor(id_producto: string){
    return new Promise(async (resolve) => {
      const response = await fetch(this.url + `/api/products/getProveedor/${id_producto}`, {
        method: 'get',
        headers: {
          'Content-Type': 'application/json',
        },
      });

      const respon = await response.json();

      if (respon.isValid) {
        resolve(respon.data);
      }
    });
  }

  public async getProductosPorCategoria(idCat: number):Promise<Array<Producto>>{
    return new Promise(async (resolve) => {
      const response = await fetch(this.url + `/api/products/getProductosPorCategoria/${idCat}`, {
        method: 'get',
        headers: {
          'Content-Type': 'application/json',
        },
      });
      const respon = await response.json();

      if (respon.isValid) {
        resolve(respon.data);
      }
    });
  }

  public async obtenerProductosPorCategoriaNews(idCat: number):Promise<Array<Producto>>{
    return new Promise(async (resolve) => {
      const response = await fetch(this.url + `/api/products/getProductosPorCategoriaNews/${idCat}`, {
        method: 'get',
        headers: {
          'Content-Type': 'application/json',
        },
      });
      const respon = await response.json();

      if (respon.isValid) {
        resolve(respon.data);
      }
    });
  }


}




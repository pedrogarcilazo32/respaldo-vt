import { Injectable } from '@angular/core';
import { Constants } from '../../../utils/constans.utils';
@Injectable({
  providedIn: 'root',
})
export class usuariosService {
  private constants: Constants;
  private url: string;
  constructor(constants: Constants) {
    this.constants = constants;

    this.url = this.constants.getUrl();
  }


  public ObtenerUsuario(id: number) {
    return new Promise(async (resolve, reject) => {
      const response = await fetch(this.url + `/api/users/by-id/${id}`, {
        method: 'get',
        headers: {
          'Content-Type': 'application/json',
        },
      });

      if (response.status === 200) {
        const respon = await response.json();

        if(respon.isValid){
          resolve(respon.data);
        }
      } else {
        reject('No se pudieron obtener los usuarios');
      }
    });
  }

  public agregarUsuario(){

  }
}

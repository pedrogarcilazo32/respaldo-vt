import { Injectable } from '@angular/core';
import { Constants } from 'src/utils/constans.utils';
import { Estante } from '../models/estante.model';
//import { dataFile } from '../models/dataFile.model';
@Injectable({ providedIn: 'root' })
export class estanteServices {
  private constants: Constants;
  private url: string;
  constructor(constants: Constants) {
    this.constants = constants;

    this.url = this.constants.getUrl();
  }

  public async getOrdenamientos(id_estante: number): Promise<Array<Estante>> {
    console.log("en services", id_estante);
    const params = {
      id_estante,
    };

    return new Promise(async (resolve) => {
      const response = await fetch(
        this.url + '/api/estante/getOrdenamiento',
        {
          body: JSON.stringify(params),
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
        }
      );
      const respon = await response.json();
      console.log(respon);
      if (respon.isValid) {
        resolve(respon.data);
      }
    });
  }

  public async obtenerEstanteActual(
    id_estante: number
  ): Promise<Array<Estante>> {
    const params = {
      id_estante,
    };

    return new Promise(async (resolve) => {
      // const arrayProductos = Array<Producto>();

      const response = await fetch(
        this.url + '/api/estante/obtenerEstanteActual',
        {
          body: JSON.stringify(params),
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
        }
      );

      const respon = await response.json();
        console.log(respon)
      if (respon.isValid) {
        resolve(respon.data);
      }
    });
  }

  public async obtenerEstantePendiente(
    id_estante: number
  ): Promise<Array<Estante>> {
    const params = {
      id_estante,
    };

    return new Promise(async (resolve) => {
      const response = await fetch(
        this.url + '/api/estante/obtenerEstantePendiente',
        {
          body: JSON.stringify(params),
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
        }
      );
      const respon = await response.json();
      if (respon.isValid) {
        resolve(respon.data);
      }
    });
  }
  public async solicitarReorden(id_estante: number): Promise<Array<Estante>> {
    console.log('llego');
    const params = {
      id_estante,
    };

    return new Promise(async (resolve) => {
      const response = await fetch(this.url + '/api/estante/solicitarReorden', {
        body: JSON.stringify(params),
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
      });
      const respon = await response.json();
      if (respon.isValid) {
        resolve(respon.data);
      }
    });
  }
  public async confirmarNuevoOrden(id_estante: number): Promise<Array<Estante>> {
    const params = {
      id_estante,
    };

    return new Promise(async (resolve) => {
      const response = await fetch(
        this.url + '/api/estante/confirmarNuevoOrden',
        {
          body: JSON.stringify(params),
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
        }
      );
      const respon = await response.json();
      if (respon.isValid) {
        resolve(respon.data);
      }
    });
  }

  public async obtenerEstantes(): Promise<Array<Estante>> {
    const params = {
      pagina: '',
    };

    return new Promise(async (resolve) => {
      const arrayEstante = Array<Estante>();

      const response = await fetch(this.url + '/api/estante', {
        body: JSON.stringify(params),
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
      });

      const respon = await response.json();

      if (respon.isValid) {
        respon.data.forEach((fh: any) => {
          const aux = new Estante(fh.id_estantes, null);

          arrayEstante.push(aux);
        });

        resolve(arrayEstante);
      }
    });
  }
}




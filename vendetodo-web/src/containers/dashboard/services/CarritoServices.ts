import { Injectable } from '@angular/core';
import { Constants } from '../../../utils/constans.utils';

@Injectable({
  providedIn: 'root',
})
export class CarritoServices {
  constructor(private constants: Constants) {
    this.constants = constants;
  }
  public url = this.constants.getUrl();

  public guardarLC(LC: any) {
    return new Promise(async (resolve) => {
      const response = await fetch(
        this.url + '/api/carrito/guardar-linea-carrito',
        {
          method: 'post',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(LC),
        }
      );

      const respon = await response.json();

      if (respon.isValid) {
        resolve(respon.data);
      } else {
        resolve(null);
      }
    });
  }

  public getLineasCarrito(idCarrito: number) {
    return new Promise(async (resolve) => {
      const response = await fetch(this.url + `/api/carrito/${idCarrito}`, {
        method: 'get',
        headers: {
          'Content-Type': 'application/json',
        }
      });

      const respon = await response.json();

      if (respon.isValid) {
        resolve(respon.data);
      } else {
        resolve(null);
      }
    });
  }

  public procesarPago(datosPago) {
    return new Promise(async (resolve) => {
      const response = await fetch(this.url + `/api/payment`, {
        method: 'post',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(datosPago)
      });

      const respon = await response.json();

      if (respon.isValid) {
        resolve(respon.data);
      } else {
        resolve(null);
      }
    });
  }

  public comprobarExistenciaProductos(idCarrito: number) {
    return new Promise(async (resolve) => {
      const response = await fetch(this.url + `/api/carrito/validar-existencia/${idCarrito}`, {
        method: 'get',
        headers: {
          'Content-Type': 'application/json',
        }
      });

      const respon = await response.json();

      if (respon.isValid) {
        resolve(respon.data);
      } else {
        resolve(null);
      }
    });
  }

  public guardarVenta(venta: any) {
    return new Promise(async (resolve) => {
      const response = await fetch(this.url + `/api/carrito/guardar-venta`, {
        method: 'post',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(venta)
      });

      const respon = await response.json();

      if (respon.isValid) {
        resolve(respon.data);
      } else {
        resolve(null);
      }
    });
  }

  public eliminarProducto(idLineaCarrito: number) {
    const params = {
      idLineaCarrito,
    };

    return new Promise(async (resolve) => {
      const response = await fetch(this.url + `/api/carrito/eliminar-linea-carrito`, {
        method: 'post',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(params)
      });

      const respon = await response.json();

      if (respon.isValid) {
        resolve(respon.data);
      } else {
        resolve(null);
      }
    });
  }

  public getProveedorMayorExistencia(idProducto: number) {
    const params = {
      idProducto,
    };

    return new Promise(async (resolve) => {
      const response = await fetch(this.url + `/api/carrito/proveedor-existencia`, {
        method: 'post',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(params)
      });

      const respon = await response.json();
      if (respon.isValid) {
        resolve(respon.data);
      } else {
        resolve(null);
      }
    });
  }
}

import { Injectable } from '@angular/core';
import { Constants } from 'src/utils/constans.utils';
import { Orden } from '../models/Orden';
import { DetalleOrden } from '../models/DetalleOrden';

@Injectable({
  providedIn: 'root',
})

export class surtirServices {
  private constants: Constants;
  private url: string;
  constructor(constants: Constants) {
    this.constants = constants;
    this.url = this.constants.getUrl();
  }

  public async getOrden(id_usuario: number): Promise<Array<Orden>> {
    const params = {
      id_usuario,
    };

    return new Promise(async (resolve) => {
      const response = await fetch(
        this.url + '/api/ordenes/getOrden',
        {
          body: JSON.stringify(params),
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
        }
      );
      const respon = await response.json();
      if (respon.isValid) {
        resolve(respon.data);
      }
    });
  }
  // public getOrden(id_usuario: number) : Promise<Array<Orden>> {
  //   const params = {
  //     id_usuario,
  //   };
  //   return new Promise(async (resolve, reject) => {
  //     const response = await fetch(this.url + '/api/ordenes', {
  //       method: 'get',
  //       headers: {
  //         'Content-Type': 'application/json',
  //       },
  //     });
  //     const respon = await response.json();
  //     if (respon.isValid) {
  //       resolve(respon.data);
  //     } else {
  //       resolve(null);
  //     }
  //   });
  // }

  public async solicitarOrden( id_usuario: number): Promise<Array<Orden>> {
    const params = {
      id_usuario,
    };

    return new Promise(async (resolve) => {
      const response = await fetch(
        this.url + '/api/ordenes/solicitarOrden',
        {
          body: JSON.stringify(params),
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
        }
      );
      const respon = await response.json();
      if (respon.isValid) {
        resolve(respon.data);
      }
    });
  }
  public async obtenerOrdenPendiente(id_usuario: number): Promise<Array<Orden>> {
    const params = {
      id_usuario,
    };

    return new Promise(async (resolve) => {
      const response = await fetch(
        this.url + '/api/ordenes/obtenerOrdenPendiente',
        {
          body: JSON.stringify(params),
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
        }
      );
      const respon = await response.json();
      if (respon.isValid) {
        resolve(respon.data);
      }
    });
  }
  public async cambiarEstado(det:DetalleOrden): Promise<Array<Orden>> {

    // const id_detalle=det.getIdDetalleOrden
    // const estado=det.getEstatus
    // const params = {
    //   id_detalle,
    //   estado
    // };
    return new Promise(async (resolve) => {
      const response = await fetch(
        this.url + '/api/ordenes/cambiarEstado',
        {
          body: JSON.stringify(det),
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
        }
      );
      const respon = await response.json();
      if (respon.isValid) {
        resolve(respon.data);
      }
    });
  }
  public async finalizarOrden( ord: Orden): Promise<Array<Orden>> {
    // const params = {
    //   id_orden,
    // };

    return new Promise(async (resolve) => {
      const response = await fetch(
        this.url + '/api/ordenes/finalizarOrden',
        {
          body: JSON.stringify(ord),
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
        }
      );
      const respon = await response.json();
      if (respon.isValid) {
        resolve(respon);
      }
    });
  }
}

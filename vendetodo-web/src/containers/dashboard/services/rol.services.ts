import { Injectable } from '@angular/core';
import { Constants } from '../../../utils/constans.utils';
import { Rol } from '../models/rol';

@Injectable({
  providedIn: 'root',
})
export class rolService {
  private constants: Constants;
  private url: string;
  constructor(constants: Constants) {
    this.constants = constants;

    this.url = this.constants.getUrl();
  }

  public async obtenerRol(): Promise<Array<Rol>> {
    const params = {
      pagina: '',
    };

    return new Promise(async (resolve) => {
      const arrayRol = Array<Rol>();

      const response = await fetch(this.url + '/api/roles', {
        body: JSON.stringify(params),
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
      });

      const respon = await response.json();

      if (respon.isValid) {
       
        respon.data.forEach((fh: any) => {

          const aux = new Rol(
            fh.id_rol,
            fh.nombre_rol
          );
            

          arrayRol.push(aux);
          
        });
        
        resolve(arrayRol);
       
      }
    });
  }
}

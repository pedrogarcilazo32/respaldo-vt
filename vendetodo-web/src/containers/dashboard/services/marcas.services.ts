import { Injectable } from '@angular/core';
import { Constants } from '../../../utils/constans.utils';
import { marca } from '../models/marca.models';

@Injectable({
  providedIn: 'root',
})
export class marcasService {
  private constants: Constants;
  private url: string;
  constructor(constants: Constants) {
    this.constants = constants;

    this.url = this.constants.getUrl();
  }

  public obtenerMarcas() {
    return new Promise((resolve, reject) => {
      fetch(this.url + '/api/marcas', {
        method: 'get',
        headers: {
          'Content-Type': 'application/json',
        },
      })
        .then((response) => {
          if (response.status === 200) {
            response.json().then((data: marca) => {
              console.log('lo que devolvio la api', data);
              resolve(data);
            });
          } else {
            reject('No se pudieron obtener las marcas');
          }
        })
        .catch((e) => reject(e));
    });
  }
}

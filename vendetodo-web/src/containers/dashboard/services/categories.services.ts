import { Injectable } from '@angular/core';
import { Constants } from '../../../utils/constans.utils';
import { categoria } from '../models/categoria.model';

@Injectable({
  providedIn: 'root',
})
export class categoriesService {
  constructor(private constants: Constants) {
    this.constants=constants;

  }
  public url = this.constants.getUrl();

  public getCategories() {
    return new Promise((resolve, reject) => {
      fetch(this.url + '/api/categories', {
        method: 'get',
        headers: {
          'Content-Type': 'application/json',
        },
      })
        .then((response) => {
          if (response.status === 200) {
            response.json().then((data: categoria) => {
              console.log('lo que devolvio la api', data);
              resolve(data);
            });
          } else {
            reject('No se pudieron obtener las categorias');
          }
        })
        .catch((e) => reject(e));
    });
  }
}

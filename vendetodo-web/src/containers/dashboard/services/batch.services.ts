import { Injectable } from '@angular/core';
import { Constants } from '../../../utils/constans.utils';
import { Batch } from '../models/batch.model';
import { Producto } from '../models/Producto.model';
@Injectable({ providedIn: 'root' })
export class BatchService {
  private constants: Constants;
  private url: string;
  constructor(constants: Constants) {
    this.constants = constants;

    this.url = this.constants.getUrl();
  }

  public addBatch(lote: Batch) {
    return new Promise(async (resolve, reject) => {
      const response = await fetch(this.url + '/api/batches', {
        body: JSON.stringify(lote),
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
      });

      if (response.status === 200) {
        const data = await response.json();
        if (data) {
          resolve(data);
        }
      } else {
        reject('No se pudo agregar el Lote');
      }
    });
  }
  public async getProductsBatches(pro: Producto): Promise<Array<Batch>> {


    return new Promise(async (resolve) => {
      

      const response = await fetch(this.url + '/api/batches/getProductsBatches', {
        body: JSON.stringify(pro),
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
      });

      const respon = await response.json();

      if (respon.isValid) {

       

        resolve(respon.data);

      }
    });
  }







}




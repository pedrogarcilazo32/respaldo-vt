import { Injectable } from '@angular/core';
import { Constants } from '../../../utils/constans.utils';
import { Cliente } from '../models/cliente';
import { Usuario } from '../models/usuario';
import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable({ providedIn: 'root' })
export class clienteService {
  private constants: Constants;
  private url: string;
  private VENDETODO_SESSION: string;
  constructor(constants: Constants, private jwtHelper: JwtHelperService) {
    this.constants = constants;
    this.jwtHelper = jwtHelper;

    this.url = this.constants.getConstants().VENDETODO_API;
    this.VENDETODO_SESSION = this.constants.getConstants().VENDETODO_SESSION;
  }

  public isAuth(): boolean {
    const token = localStorage.getItem(this.VENDETODO_SESSION);
    if (
      this.jwtHelper.isTokenExpired(token) ||
      !localStorage.getItem(this.VENDETODO_SESSION)
    ) {
      return false;
    }
    return true;
  }

  public recibirToken(user: Usuario) {
    return new Promise(async (resolve, reject) => {
      const response = await fetch(this.url + '/api/sign/signin', {
        body: JSON.stringify(user),
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
      });

      if (response.status === 200) {
        const data = await response.json();
        if (data) {
          resolve(data);
        }
      } else {
        reject('Correo o contraseña incorrectos');
      }
    });
  }

  public agregarCliente(pro: Cliente) {
    return new Promise(async (resolve, reject) => {
      const response = await fetch(this.url + '/api/users/agregarUser', {
        body: JSON.stringify(pro),
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
      });

      if (response.status === 200) {
        const data = await response.json();
        if (data) {
          resolve(data);
        }
      } else {
        reject('No se pudo registrar el Cliente');
      }
    });
  }

  public getCorreo(correo: string) {
    const params = {
      correo,
    };
    return new Promise(async (resolve, reject) => {
      const arrayUsuarios = Array<Usuario>();
      const response = await fetch(this.url + '/api/users/buscarCorreo', {
        body: JSON.stringify(params),
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
      });

      const respon = await response.json();

      if (respon.isValid) {
        respon.data.forEach((fh: any) => {
          const auxUsuario = new Usuario(
            fh.id_usuario,
            fh.correo,
            fh.contra,
            null
          );

          arrayUsuarios.push(auxUsuario);
        });

        resolve(arrayUsuarios);
      }
    });
  }
}

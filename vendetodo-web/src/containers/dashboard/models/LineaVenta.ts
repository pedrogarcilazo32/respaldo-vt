import { Producto } from "./Producto";
import { Proveedor } from "./Proveedor";

export class LineaVenta{

    private producto: Producto;
    private cantidad: number;
    private proveedor: Proveedor;
    private idLineaVenta?: number;

    constructor(
      producto: Producto,
      cantidad: number,
      proveedor: Proveedor,
      idLineaVenta?: number,
    ) {
      this.producto = producto;
      this.proveedor = proveedor;
      this.cantidad = cantidad;
      this.idLineaVenta = idLineaVenta;
    }

    //Calcular subtotal
    public obtenerSubtotal(): number {
      return this.cantidad * this.producto.getPrecio;
    }

    // Getters and Setters
    public get getIdLineaVenta(): number {
      return this.idLineaVenta;
    }

    public set setIdLineaVenta(idLineaVenta: number) {
      this.idLineaVenta = idLineaVenta;
    }

    public get getProducto(): Producto {
      return this.producto;
    }

    public set setProducto(producto: Producto) {
      this.producto = producto;
    }

    public get getProveedor(): Proveedor {
      return this.proveedor;
    }

    public set setProveedor(proveedor: Proveedor) {
      this.proveedor = proveedor;
    }

    public get getCantidad(): number {
      return this.cantidad;
    }

    public set setCantidad(cantidad: number) {
      this.cantidad = cantidad;
    }


}

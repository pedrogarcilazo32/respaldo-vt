import { Rol } from './rol';

export class Usuario {
  private id_usuario?: number;
  private email?: string;
  private contra?: string;
  private rol?: Rol;

  constructor(id_usuario?: number, email?: string, contra?: string, rol?: Rol) {
    this.id_usuario = id_usuario;
    this.email = email;
    this.contra = contra;
    this.rol = rol;
  }
  //*Getters and Setters
  public get id_Usuario(): number {
    return this.id_usuario;
  }
  public set id_Usuario(id_usuario: number) {
    this.id_usuario = id_usuario;
  }
  public get Email(): string {
    return this.email;
  }
  public set Email(email: string) {
    this.email = email;
  }
  public get Contra(): string {
    return this.contra;
  }
  public set Contra(contra: string) {
    this.contra = contra;
  }
  public get Rol(): Rol {
    return this.rol;
  }
  public set Rol(rol: Rol) {
    this.rol = rol;
  }
}

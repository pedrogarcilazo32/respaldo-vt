export class Producto {
  private nombre: string;
  private descripcion: string;
  private precio: number;
  private volumen: number;
  private imageURL: string;
  private idProducto: number;
  private idCategoria: number;
  private idMarca: number;
  private nombreCategoria: string;
  private nombreMarca: string;
  private estatus: string;
  private descuento: number;

  constructor(
    nombre: string,
    descripcion: string,
    precio: number,
    volumen: number,
    imageUrl: string,
    idProducto: number,
    idCategoria?: number,
    idMarca?: number,
    nombreCategoria?: string,
    nombreMarca?: string,
    estatus?: string,
    descuento?: number,
  ) {
    this.nombre = nombre;
    this.descripcion = descripcion;
    this.precio = precio;
    this.volumen = volumen;
    this.idCategoria = idCategoria;
    this.idMarca = idMarca;
    this.imageURL = imageUrl;
    this.idProducto = idProducto;
    this.nombreCategoria = nombreCategoria;
    this.nombreMarca = nombreMarca;
    this.estatus = estatus;
    this.descuento = descuento;
  }

  public get getIdProducto(): number {
    return this.idProducto;
  }
  public set setIdProducto(idProducto: number) {
    this.idProducto = idProducto;
  }

  public get getNombre(): string {
    return this.nombre;
  }
  public set setNombre(nombre: string) {
    this.nombre = nombre;
  }

  public get getNombreMarca(): string {
    return this.nombreMarca;
  }

  public get getNombreCategoria(): string {
    return this.nombreCategoria;
  }

  public get getDescripcion(): string {
    return this.descripcion;
  }
  public set setDescripcion(descripcion: string) {
    this.descripcion = descripcion;
  }

  public get getPrecio(): number {
    return this.precio;
  }
  public set setPrecio(precio: number) {
    this.precio = precio;
  }

  public get getVolumen(): number {
    return this.volumen;
  }
  public set setVolumen(volumen: number) {
    this.volumen = volumen;
  }

  public get getIdCategoria(): number {
    return this.idCategoria;
  }
  public set setIdCategoria(idCategoria: number) {
    this.idCategoria = idCategoria;
  }

  public get getImageURL(): string {
    return this.imageURL;
  }
  public set setImageURL(imageURL: string) {
    this.imageURL = imageURL;
  }

  public get getEstatus(): string {
    return this.estatus;
  }
  public set setEstatus(estatus: string) {
    this.estatus = estatus;
  }
  public get getDescuento(): number {
    return this.descuento;
  }
  public set setDescuento(descuento: number) {
    this.descuento = descuento;
  }

}


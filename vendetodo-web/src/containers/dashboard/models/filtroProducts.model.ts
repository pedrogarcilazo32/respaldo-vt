export class filtroProducts {
  f_registro_inicio?: string;
  f_registro_fin?: string;
  i_usuario_id?: number;
  id_marca?: number;
  search?: string;
  v_nombreEmpleado?: string;
  v_nombreMarca?: string;
  id_categoria?: number;
  v_nombreCategoria?: string;
  constructor(
    f_registro_inicio?:string,
    f_registro_fin?:string,
    i_usuario_id?:number,
    id_marca?:number,
    search?:string,
    v_nombreMarca?:string,
    v_nombreEmpleado?:string,
    id_categoria?:number,
    v_nombreCategoria?:string

  ) {
    this.f_registro_inicio = f_registro_inicio;
    this.f_registro_fin = f_registro_fin;
    this.i_usuario_id = i_usuario_id;
    this.id_marca = id_marca;
    this.search = search;
    this.v_nombreMarca = v_nombreMarca;
    this.v_nombreEmpleado = v_nombreEmpleado;
    this.id_categoria = id_categoria;
    this.v_nombreCategoria = v_nombreCategoria;
  }
  public get fechaInicio(): string {
    return this.f_registro_inicio;
  }

  public set fechaInicio(f_registro_inicio: string) {
    this.f_registro_inicio = f_registro_inicio;
  }
  public get fechaFin(): string {
    return this.f_registro_inicio;
  }

  public set fechaFin(f_registro_fin: string) {
    this.f_registro_fin = f_registro_fin;
  }
  public get usuarioId(): number {
    return this.i_usuario_id;
  }

  public set usuarioId(id_marca: number) {
    this.id_marca = id_marca;
  }
  public get marcaId(): number {
    return this.id_marca;
  }

  public set marcaId(id_marca: number) {
    this.id_marca = id_marca;
  }
  public get busqueda(): string {
    return this.search;
  }

  public set busqueda(search: string) {
    this.search = search;
  }
  public get nombreMarca(): string {
    return this.v_nombreMarca;
  }

  public set nombreMarca(v_nombreMarca: string) {
    this.v_nombreMarca = v_nombreMarca;
  }
  public get categoriaId(): number {
    return this.id_categoria;
  }

  public set categoriaId(id_categoria: number) {
    this.id_categoria = id_categoria;
  }
  public get nombreCategoria(): string {
    return this.v_nombreCategoria;
  }

  public set nombreCategoria(v_nombreCategoria: string) {
    this.v_nombreCategoria = v_nombreCategoria;
  }

}

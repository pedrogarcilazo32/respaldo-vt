import { dataFile } from './dataFile.model';
import { categoria } from './categoria.model';
import { marca } from './marca.models';
export class Producto {
  private id?: number;
  private c_clave?: string;
  private v_nombre: string;
  private d_volumen: number;
  private d_precio: number;
  private id_marca: number;
  private b_activo_opc?: boolean;
  private f_registro?: string;
  private i_usuario_id?: number;
  private v_nombreUsuario?: string;
  private v_nombreMarca?: string;
  private total_paginas?: number;
  private dataFile?: dataFile;
  private v_image_url?: string;
  private v_descripcion?: string;
  private marca:marca;
  private categoria:categoria;



  constructor(
    v_nombre?: string,
    d_volumen?: number,
    d_precio?: number,
    i_usuario_id?: number,
    f_registro?: string,
    v_nombreUsuario?: string,
    id?: number,
    v_descripcion?: string,
    v_image_url?: string,
    dataFile?: dataFile,
    total_paginas?: number,
    marca?:marca,
    categoria?:categoria
  ) {
    this.v_nombre = v_nombre;
    this.d_volumen = d_volumen;
    this.d_precio = d_precio;
    this.i_usuario_id = i_usuario_id;
    this.f_registro = f_registro;
    this.v_nombreUsuario = v_nombreUsuario;
    this.id = id;
    this.v_descripcion = v_descripcion;
    this.v_image_url = v_image_url;
    this.dataFile = dataFile;
    this.total_paginas=total_paginas;
    this.marca=marca;
    this.categoria=categoria;
  }
  // constructor(
  //   // nombre: string,
  //   // volumen: number,
  //   // precio: number,
  //   // usuarioId: number,
  //   // idMarca: number,
  //   // nombreMarca?: string,
  //   // Id?: number,
  //   // nombreUsuario?: string,
  //   // fecha?: string,
  //   // idCategoria?: number,
  //   // nombreCategoria?: string,
  //   // imageUrl?: string,
  //   // descripcionProducto?: string,
  // ) {
  //   this.id = Id;
  //   this.v_nombre = nombre;
  //   this.d_volumen = volumen;
  //   this.d_precio = precio;
  //   this.i_usuario_id = usuarioId;
  //   this.v_nombreUsuario = nombreUsuario;
  //   this.id_marca = idMarca;
  //   this.v_nombreMarca = nombreMarca;
  //   this.f_registro = fecha;
  //   this.id_categoria = idCategoria;
  //   this.v_nombreCategoria = nombreCategoria;
  //   this.v_image_url = imageUrl;
  //   this.v_descripcion = descripcionProducto;
  // }

  public get idProducto(): number {
    return this.id;
  }

  public set idProducto(id: number) {
    this.id = id;
  }

  public get nombreProducto(): string {
    return this.v_nombre;
  }

  public set nombreProducto(nombre: string) {
    this.v_nombre = nombre;
  }

  public get volumenProducto(): number {
    return this.d_volumen;
  }

  public set volumenProducto(volumen: number) {
    this.d_volumen = volumen;
  }

  public get precioProducto(): number {
    return this.d_precio;
  }

  public set precioProducto(precio: number) {
    this.d_precio = precio;
  }

  public get nombreUsuario(): string {
    return this.v_nombreUsuario;
  }

  public set nombreUsuario(nombreUsuario: string) {
    this.v_nombreUsuario = nombreUsuario;
  }

  public get Marca(): marca {
    return this.marca;
  }

  public set Marca(marca: marca) {
    this.marca = marca;
  }
  public get Categoria(): categoria {
    return this.categoria;
  }

  public set Categoria(categoria: categoria) {
    this.categoria = categoria;
  }



  get fechaRegistro(): string {
    return this.f_registro;
  }

  public set fechaRegistro(fechaRegistro: string) {
    this.f_registro = fechaRegistro;
  }

  public get totalPaginas(): number {
    return this.total_paginas;
  }
  public set totalPaginas(total_paginas:number){
    this.total_paginas=total_paginas;
  }

  get image_url(): string {
    return this.v_image_url;
  }

  public set image_url(v_image_url: string) {
    this.v_image_url = v_image_url;
  }
  get descripcion(): string {
    return this.v_descripcion;
  }

  public set descripcion(v_descripcion: string) {
    this.v_descripcion = v_descripcion;
  }






}

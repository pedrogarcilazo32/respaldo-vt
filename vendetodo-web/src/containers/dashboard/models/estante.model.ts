import { Seccion } from "./seccion.model";
export class Estante {
  private id_estante?: number;
  private ultimo_ordenamiento?: string;
  private fecha_inicio?: string;
  private fecha_fin?: string;
  private nombreEmpleado?: string;
  // private seccionEstante?:Seccion;
  private secciones: Array<Seccion>;
  constructor(
    id_estante?: number,
    ultimo_ordenamiento?: string,
    fecha_inicio?: string,
    fecha_fin?: string,
    nombreEmpleado?: string,
    // seccionEstante?:Seccion,
    secciones?:Array<Seccion>

  ) {
    this.id_estante = id_estante;
    this.ultimo_ordenamiento = ultimo_ordenamiento;
    this.fecha_inicio = fecha_inicio;
    this.fecha_fin = fecha_fin;
    this.nombreEmpleado = nombreEmpleado;
    // this.seccionEstante=seccionEstante;
    this.secciones=[];
  }

  public crearSeccion(id_seccion:number,capacidad_seccion:number,num_seccion:number,cantidad_otorgada:number,nombreProducto:string,image_url:string) {
    const sec = new Seccion(id_seccion,capacidad_seccion,num_seccion,cantidad_otorgada);
    sec.crearproducto(nombreProducto,image_url);
    this.secciones.push(sec);
  }

  public get gtidEstante(): number {
    return this.id_estante;
  }
  public set idEstante(idEstante: number) {
    this.id_estante = idEstante;
  }
  public get ultimoOrdenamiento(): string {
    return this.ultimo_ordenamiento;
  }

  public set ultimoOrdenamiento(ultimoOrdenamiento: string) {
    this.ultimo_ordenamiento = ultimoOrdenamiento;
  }
  public get Secciones(): Array<Seccion> {
    return this.secciones;
  }

  public set Secciones(Secciones: Array<Seccion>) {
    this.secciones = Secciones;
  }
  public get getfecha_inicio(): string {
    return this.fecha_inicio;
  }

  public set setfecha_inicio(value: string) {
    this.fecha_inicio = value;
  }

  public get gtfecha_fin(): string {
    return this.fecha_fin;
  }

  public set setfecha_fin(value: string) {
    this.fecha_fin = value;
  }
  public get gtnombreEmpleado(): string {
    return this.nombreEmpleado;
  }

  public set setnombreEmpleado(value: string) {
    this.nombreEmpleado = value;
  }
  // public get SeccionEstante(): Seccion {
  //   return this.seccionEstante;
  // }

  // public set SeccionEstante(seccionEstante: Seccion) {
  //   this.seccionEstante = seccionEstante;
  // }








}

export class Estantes{

    private id_estante: number;
    private ult_modificacion: string;

    constructor(
        id_estante: number,
        ult_modificacion: string
    ){
        this.id_estante = id_estante;
        this.ult_modificacion = ult_modificacion;
    }

    //*GETTERS AND SETTERS
    public get id_Estante(): number{
        return this.id_estante;
    }
    
    public set id_Estante(id_estante: number){
        this.id_estante = id_estante;
    }

    public get ult_Modificacion():string{
        return this.ult_Modificacion;
    }

    public set ult_Modificacion(ult_modificacion: string){
        this.ult_modificacion = ult_modificacion;
    }
    
}
export class Direccion{

    private id_direccion?: number;
    private cp?: number;
    private calle?: string;
    private ciudad?: string;
    private no_exterior?: string;

    constructor(
        id_direccion?: number,
        cp?: number,
        calle?: string,
        ciudad?: string,
        no_exterior?: string,
    ){
        this.id_direccion = id_direccion;
        this.cp           = cp;
        this.calle        = calle;
        this.ciudad       = ciudad;
        this.no_exterior  = no_exterior;
    }
    //* GETTERS Y SETTERS
    public get id_Direccion():number{
        return this.id_direccion;
    }
    public set id_Direccion(id_direccion:number){
        this.id_direccion = id_direccion;
    }
    public get Cp(): number{
        return this.cp;
    }
    public set Cp(cp: number){
        this.cp = cp;
    }
    public get Calle(): string{
        return this.calle;
    }
    public set Calle(calle: string){
        this.calle = calle;
    }
    public get Ciudad(): string{
        return this.ciudad;
    }
    public set Ciudad(ciudad: string){
        this.ciudad = ciudad;
    }
    public get no_Exterior(): string{
        return this.no_exterior;
    }
    public set no_Exterior(no_exterior: string){
        this.no_exterior = no_exterior;
    }
}
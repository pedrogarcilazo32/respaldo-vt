import { Direccion } from './direccion';
import { Estante } from './estante.model';
import { Usuario } from './usuario';
export class Empleado{
    
    //?Esto es del objeto Empleado
    private nombre?: string;
    private ape_paterno?: string;
    private ape_materno?: string;
    private telefono?: number;

    //!Esto va en los otros objetos
    private usuario: Usuario;
    private total_paginas?: number;
    private direccion?: Direccion;
    private estante?: Estante;

    constructor(
        usuario: Usuario,
        nombre: string,
        ape_paterno: string,
        ape_materno: string,
        telefono: number,
        total_paginas : number,
        direccion: Direccion,
        estante: Estante,
    ){
        this.usuario = usuario;
        this.nombre       = nombre;
        this.ape_paterno  = ape_paterno;
        this.ape_materno  = ape_materno;
        this.telefono     = telefono;
        this.total_paginas= total_paginas;
        this.direccion = direccion;
        this.estante = estante;
    }
    //?Setters y Getters
    public get Usuario(): Usuario{
        return this.usuario;
    }
    public set Usuario(usuario: Usuario){
        this.usuario = usuario;
    }
    
    public get Nombre(): string{
        return this.nombre;
    }
    public set Nombre(nombre: string){
        this.nombre = nombre;
    }
    public get ape_Materno(): string{
        return this.ape_materno;
    }
    public set ape_Materno(ape_materno: string){
        this.ape_materno = ape_materno;
    }
    public get ape_Paterno(): string{
        return this.ape_paterno;
    }
    public set ape_Paterno(ape_paterno: string){
        this.ape_paterno = ape_paterno;
    }
    public get Telefono(): number{
        return this.telefono;
    }
    public set Telefono(telefono: number){
        this.telefono = telefono;
    }
  
    public get total_Paginas(): number{
        return this.total_paginas;
    }
    public set total_Paginas(total_paginas: number){
        this.total_paginas = total_paginas;
    }
    public get obj_Direccion(): Direccion{
        return this.direccion;
    }
    public set obj_Direccion(direccion: Direccion){
        this.direccion = direccion;
    }
    public get obj_Estante(): Estante{
        return this.estante;
    }
    public set obj_Estante(estante: Estante){
        this.estante = estante;
    }
}
import { LineaCarrito } from "./LineaCarrito";
import { Producto } from "./Producto";
import { Proveedor } from "./Proveedor";
import { Venta } from "./Venta";


export class Carrito {
  private idCarrito: number;
  private lineasCarrito: Array<LineaCarrito>;

  constructor(idCarrito: number) {
    this.idCarrito = idCarrito;
    this.lineasCarrito = [];
  }

  public crearLineaCarrito(
    prod: Producto,
    cant: number,
    prov: Proveedor,
    idCar?: number,
    disponible_total?: number
  ) {
    const LC = new LineaCarrito(prod, cant, prov, idCar, disponible_total);
    this.lineasCarrito.push(LC);
    return LC;
  }

  public crearNuevaVenta(id_usuario: number, referencia: string): Venta {
    return new Venta(
      id_usuario,
      this.obtenerTotal(),
      'Pendiente',
      new Date(),
      this.getLineasCarrito,
      referencia
    );
  }

  //Calcular total
  public obtenerTotal() {
    var sumaTotal: number = 0;
    this.lineasCarrito.forEach((LC) => {
      sumaTotal += LC.obtenerSubtotal();
    });

    return sumaTotal;
  }

  public setearNuevaCantidad(
    idLineaCarrito: number,
    disponible_cantidad: number
  ) {
    console.log('recibo', idLineaCarrito, disponible_cantidad);
    this.lineasCarrito.forEach((fh) => {
      if (fh.getIdLineaCarrito === idLineaCarrito) {
        if (disponible_cantidad < fh.getCantidad) {
          const cantidadAVender = fh.getCantidad - disponible_cantidad;
          if (cantidadAVender > 0) {
            fh.setCantidad = cantidadAVender;
          } else {
            fh.setCantidad = 0;
          }
        }
      }
    });
  }

  public checarExistenciaMinima() {
    let existe = false;
    this.lineasCarrito.forEach((fh) => {
      if (fh.getCantidad > 0) {
        existe = true;
        return;
      }
    });

    return existe;
  }

  // Getters and Setters
  public get getIdCarrito(): number {
    return this.idCarrito;
  }

  public set setIdCarrito(idCarrito: number) {
    this.idCarrito = idCarrito;
  }

  public get getLineasCarrito(): Array<LineaCarrito> {
    return this.lineasCarrito;
  }

  public set setLineasCarrito(lineasCarrito: Array<LineaCarrito>) {
    this.lineasCarrito = lineasCarrito;
  }
}

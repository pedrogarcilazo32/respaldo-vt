import { LineaCarrito } from "./LineaCarrito";
import { LineaVenta } from "./LineaVenta";

export class Venta {
  private totalVenta: number;
  private estatus: string;
  private fecha: Date;
  private lineasVenta = Array<LineaVenta>();
  private idVenta?: number;
  private id_usuario: number;
  private referencia: string;

  constructor(
    id_usuario: number,
    totalVenta: number,
    estatus: string,
    fecha: Date,
    lineasCarrito?: Array<LineaCarrito>,
    referencia?:string,
    idVenta?: number,
  ) {
    this.id_usuario = id_usuario;
    this.totalVenta = totalVenta;
    this.estatus = estatus;
    this.fecha = fecha;
    this.idVenta = idVenta;
    this.referencia = referencia;
    this.pasarLineasCarritoAVenta(lineasCarrito);
  }


  public pasarLineasCarritoAVenta(lineasCarrito: Array<LineaCarrito>) {
    lineasCarrito.forEach(LC => {
      const LV = new LineaVenta(LC.getProducto, LC.getCantidad, LC.getProveedor);
      this.lineasVenta.push(LV);
    });
  }

  public pagar() {
  }

  // Getters and Setters
  public get getIdVenta(): number {
    return this.idVenta;
  }

  public get getIdUsuario(): number {
    return this.id_usuario;
  }


  public set setIdVenta(idVenta: number) {
    this.idVenta = idVenta;
  }

  public get getTotalVenta(): number {
    return this.totalVenta;
  }

  public set setTotalVenta(totalVenta: number) {
    this.totalVenta = totalVenta;
  }

  public get getEstatus(): string {
    return this.estatus;
  }

  public set setEstatus(estatus: string) {
    this.estatus = estatus;
  }

  public get getFecha(): Date {
    return this.fecha;
  }

  public set setFecha(fecha: Date) {
    this.fecha = fecha;
  }

  public get getLineasVenta(): Array<LineaVenta> {
    return this.lineasVenta;
  }

  public set setLineasVenta(lineasVenta: Array<LineaVenta>) {
    this.lineasVenta = lineasVenta;
  }
}

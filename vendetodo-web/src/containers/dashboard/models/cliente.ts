import { Direccion } from './direccion';
import { Usuario } from './usuario';
export class Cliente{
    
    //?Esto es del objeto Empleado
    private nombre: string;
    private ape_paterno: string;
    private ape_materno: string;
    private telefono: number;

    //!Esto va en los otros objetos
    private usuario?: Usuario;
    private direccion?: Direccion;

    constructor(
        nombre: string,
        ape_paterno: string,
        ape_materno: string,
        telefono: number,
        usuario?: Usuario,
        direccion?: Direccion,
    ){
        this.usuario = usuario;
        this.nombre       = nombre;
        this.ape_paterno  = ape_paterno;
        this.ape_materno  = ape_materno;
        this.telefono     = telefono;
        this.direccion = direccion;
    }
    //?Setters y Getters
    public get Usuario(): Usuario{
        return this.usuario;
    }
    public set Usuario(usuario: Usuario){
        this.usuario = usuario;
    }
    
    public get Nombre(): string{
        return this.nombre;
    }
    public set Nombre(nombre: string){
        this.nombre = nombre;
    }
    public get ape_Materno(): string{
        return this.ape_materno;
    }
    public set ape_Materno(ape_materno: string){
        this.ape_materno = ape_materno;
    }
    public get ape_Paterno(): string{
        return this.ape_paterno;
    }
    public set ape_Paterno(ape_paterno: string){
        this.ape_paterno = ape_paterno;
    }
    public get Telefono(): number{
        return this.telefono;
    }
    public set Telefono(telefono: number){
        this.telefono = telefono;
    }
  
    public get obj_Direccion(): Direccion{
        return this.direccion;
    }
    public set obj_Direccion(direccion: Direccion){
        this.direccion = direccion;
    }
   
}
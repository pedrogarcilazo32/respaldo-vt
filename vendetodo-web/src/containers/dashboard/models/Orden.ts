import { DetalleOrden } from './DetalleOrden';

export class Orden {
  private listaDetalleOrden: Array<DetalleOrden>;
  private listaDetalleOrdenAux: Array<DetalleOrden>;
  private listaDetalleBodega: Array<DetalleOrden>;
  private estatus: string;
  private idOrden?: number;
  private idEmpleado?: number;
  private folio_venta: string;
  private fecha_inicio?: string;
  private fecha_fin?: string;
  private estante_id?: number;
  private nombreEmpleado?: string;

  constructor(
    estatus?: string,
    folio_venta?: string,
    idOrden?: number,
    idEmpleado?: number,
    fecha_inicio?: string,
    fecha_fin?: string,
    listaDetalleOrden?: Array<DetalleOrden>,
    listaDetalleOrdenAux?: Array<DetalleOrden>,
    estante_id?: number,
    nombreEmpleado?: string,
    listaDetalleBodega?: Array<DetalleOrden>
  ) {
    this.estatus = estatus;
    this.idOrden = idOrden;
    this.idEmpleado = idEmpleado;
    this.fecha_inicio = fecha_inicio;
    this.fecha_fin = fecha_fin;
    this.folio_venta = folio_venta;
    this.listaDetalleOrden = [];
    this.listaDetalleOrdenAux = [];
    (this.estante_id = estante_id), (this.listaDetalleBodega = []);
  }
  public crearDetalleOrden(
    idDetalleOrden: number,
    nombre_producto: string,
    nombre_provedor,
    cantidad: number,
    id_estante: number,
    id_seccion: number,
    estatus: boolean,
    estaenBodega: boolean
  ) {
    const DT = new DetalleOrden(
      nombre_producto,
      cantidad,
      nombre_provedor,
      estatus,
      idDetalleOrden,
      id_estante,
      id_seccion,
      0,
      estaenBodega
    );

    if (estaenBodega) {
      this.listaDetalleBodega.push(DT);
    } else {
      this.listaDetalleOrdenAux.push(DT);
    }
  }

  public finalizaOrden() {
    this.estatus = 'Finalizada';
  }

  public cambiarEstado(idDetalleOrden: number) {
    var DT = this.listaDetalleOrden.filter(
      (det) => det.getIdDetalleOrden == idDetalleOrden
    );
    DT[0].cambiarEstado();
    console.log(DT[0]);
    return DT[0];
  }

  //CREAR RUTA
  obtenerCostoSeccion(seccion: number) {
    if (seccion == 1 || seccion == 4) {
      return 1;
    } else if (seccion == 2 || seccion == 5) {
      return 2;
    }

    return 3;
  }

  obtenerMasCercano(estanteActual: number, seccionActual: number) {
    for (var i in this.listaDetalleOrdenAux) {
      var costoEstante = Math.abs(
        estanteActual - this.listaDetalleOrdenAux[i].idestante
      );
      var costoSeccionActual = this.obtenerCostoSeccion(seccionActual);
      var costoSeccionIr = this.obtenerCostoSeccion(
        this.listaDetalleOrdenAux[i].idSeccion
      );
      var costoSeccionTotal = Math.abs(costoSeccionActual - costoSeccionIr);
      var costo = costoEstante + costoSeccionTotal;
      if (
        costoSeccionIr == 2 &&
        costoSeccionActual == 2 &&
        estanteActual != this.listaDetalleOrdenAux[i].idestante
      ) {
        costo = costo + 2;
      }
      this.listaDetalleOrdenAux[i].setCostoRuta = costo;
    }
    this.listaDetalleOrdenAux.sort((x, y) => x.GetcostoRuta - y.GetcostoRuta);

    this.listaDetalleOrden.push(this.listaDetalleOrdenAux[0]);
    this.listaDetalleOrdenAux.splice(0, 1);
  }

  crearRuta() {
    if (this.listaDetalleOrdenAux.length) {
      this.obtenerMasCercano(this.getEstante_id, 1);
      while (this.listaDetalleOrdenAux.length) {
        var pos = this.listaDetalleOrden.length - 1;
        this.obtenerMasCercano(
          this.listaDetalleOrden[pos].idestante,
          this.listaDetalleOrden[pos].idSeccion
        );
      }
    }
    if (this.listaDetalleBodega.length) {
      this.agregarProductosEnBodega();
    }
  }

  agregarProductosEnBodega() {
    for (var i in this.listaDetalleBodega) {
      this.listaDetalleOrden.push(this.listaDetalleBodega[i]);
    }
    this.listaDetalleBodega = [];
  }
  // Getters and Setters
  public get getIdOrden(): number {
    return this.idOrden;
  }

  public set setIdOrden(idOrden: number) {
    this.idOrden = idOrden;
  }

  public get getIdEmpleado(): number {
    return this.idEmpleado;
  }

  public set setIdEmpleado(idEmpleado: number) {
    this.idEmpleado = idEmpleado;
  }

  public get getEstante_id(): number {
    return this.estante_id;
  }

  public set setEstante_id(estante_id: number) {
    this.estante_id = estante_id;
  }

  public get getListaDetalleOrden(): Array<DetalleOrden> {
    return this.listaDetalleOrden;
  }

  public set setListaDetalleOrden(listaDetalleOrden: Array<DetalleOrden>) {
    this.listaDetalleOrden = listaDetalleOrden;
  }

  public get getlistaDetalleOrdenAux(): Array<DetalleOrden> {
    return this.listaDetalleOrdenAux;
  }

  public set setlistaDetalleOrdenAux(
    listaDetalleOrdenAux: Array<DetalleOrden>
  ) {
    this.listaDetalleOrdenAux = listaDetalleOrdenAux;
  }

  public get getlistaDetalleBodega(): Array<DetalleOrden> {
    return this.listaDetalleBodega;
  }

  public set setlistaDetalleBodega(listaDetalleBodega: Array<DetalleOrden>) {
    this.listaDetalleBodega = listaDetalleBodega;
  }

  public get getEstatus(): string {
    return this.estatus;
  }

  public set setEstatus(estatus: string) {
    this.estatus = estatus;
  }

  public get getfecha_inicio(): string {
    return this.fecha_inicio;
  }

  public set setfecha_inicio(value: string) {
    this.fecha_inicio = value;
  }

  public get gtfecha_fin(): string {
    return this.fecha_fin;
  }

  public set setfecha_fin(value: string) {
    this.fecha_fin = value;
  }

  public get gtfolio_venta(): string {
    return this.folio_venta;
  }

  public set setfolio_venta(value: string) {
    this.folio_venta = value;
  }

  public get gtnombreEmpleado(): string {
    return this.nombreEmpleado;
  }

  public set setnombreEmpleado(value: string) {
    this.nombreEmpleado = value;
  }
}

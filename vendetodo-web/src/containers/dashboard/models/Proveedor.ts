
export class Proveedor {
  private nombre: string;
  private idProveedor?: number;
  private telefono:string;
  private email:string;

  constructor(
    nombre: string,
    idProveedor?: number,
    telefono?:string,
    email?:string,
  ) {
    this.nombre = nombre;
    this.telefono = telefono;
    this.email = email;
    this.idProveedor = idProveedor;
  }

  public get getIdProvedor(): number {
    return this.idProveedor;
  }
  public set setIdProvedor(idProveedor: number) {
    this.idProveedor = idProveedor;
  }

  public get getNombre(): string {
    return this.nombre;
  }
  public set setNombre(nombre: string) {
    this.nombre = nombre;
  }

  public get getTelefono(): string {
    return this.telefono;
  }
  public set setTelefono(telefono: string) {
    this.telefono = telefono;
  }

  public get getEmail(): string {
    return this.email;
  }
  public set setEmail(email: string) {
    this.email = email;
  }


}

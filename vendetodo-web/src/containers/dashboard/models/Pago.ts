export class Pago {
  private creditCard: number;
  private nombreApellido: string;
  private fechaExp: string;
  private cvv: number;
  private id_usuario: number;
  private referencia: string;

  constructor(id_usuario: number, creditCard: number, nombreApellido: string, fechaExp: string, cvv: number, referencia?: string) {
    this.id_usuario = id_usuario;
    this.creditCard = creditCard;
    this.nombreApellido = nombreApellido;
    this.fechaExp = fechaExp;
    this.cvv = cvv;
    this.referencia = referencia;
  }

  public get getIdUsuario(): number {
    return this.id_usuario;
  }

  public get getReferencia(): string {
    return this.referencia;
  }

  public set setReferencia(referencia: string) {
    this.referencia = referencia;
  }

  public get getCreditCard(): number {
    return this.creditCard;
  }

  public get getNombreApellido(): string {
    return this.nombreApellido;
  }

  public get getFechaExp(): string {
    return this.fechaExp;
  }

  public get getCvv(): number {
    return this.cvv;
  }

}

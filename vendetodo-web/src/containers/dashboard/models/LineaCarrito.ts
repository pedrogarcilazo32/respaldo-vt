import { Producto } from "./Producto";
import { Proveedor } from "./Proveedor";

export class LineaCarrito {
  private producto: Producto;
  private cantidad: number;
  private proveedor: Proveedor;
  private idLineaCarrito?: number;
  private disponible_total: number;

  constructor(
    producto: Producto,
    cantidad: number,
    proveedor: Proveedor,
    idLineaCarrito?: number,
    disponible_total?: number,
  ) {
    this.producto = producto;
    this.proveedor = proveedor;
    this.cantidad = cantidad;
    this.idLineaCarrito = idLineaCarrito;
    this.disponible_total = disponible_total;
  }

  //Calcular subtotal
  public obtenerSubtotal(): number {
    return this.cantidad * this.producto.getPrecio;
  }

  // Getters and Setters
  public get getIdLineaCarrito(): number {
    return this.idLineaCarrito;
  }

  public get getDisponibleTotal(): number {
    return this.disponible_total;
  }


  public set setIdLineaCarrito(idLineaCarrito: number) {
    this.idLineaCarrito = idLineaCarrito;
  }

  public get getProducto(): Producto {
    return this.producto;
  }

  public set setProducto(producto: Producto) {
    this.producto = producto;
  }

  public get getProveedor(): Proveedor {
    return this.proveedor;
  }

  public set setProveedor(proveedor: Proveedor) {
    this.proveedor = proveedor;
  }

  public get getCantidad(): number {
    return this.cantidad;
  }

  public set setCantidad(cantidad: number) {
    this.cantidad = cantidad;
  }
}

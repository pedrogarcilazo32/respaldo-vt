
export class Batch {
   private id_lote?:number;
   private  id_provedor?:number;
   private id_producto?:number;
   private entrada_can?:number;
   private  comprometido_can?:number;
   private salida_can?:number;
   private  disponible_cant?:number;
   private i_usuario_id?:number;
   private fecha_registro?:string;
   private v_nombreUsuario?: string;
  private v_nombreProvedor?:string;
  
  
    constructor(
    id_lote?:number,
    id_provedor?:number,
    id_producto?:number,
    entrada_can?:number,
    comprometido_can?:number,
    salida_can?:number,
    disponible_cant?:number,
    i_usuario_id?:number,
    fecha_registro?:string,
    v_nombreUsuario?: string,
    v_nombreProvedor?: string
     
    ) {
      this.id_lote=id_lote;
      this.id_provedor=id_provedor;
      this.id_producto=id_producto;
      this.entrada_can=entrada_can;
      this.comprometido_can=comprometido_can;
      this.salida_can=salida_can;
      this.disponible_cant=disponible_cant;
      this.i_usuario_id=i_usuario_id;
      this.fecha_registro=fecha_registro;
      this.v_nombreUsuario=v_nombreUsuario;
      this.v_nombreProvedor=v_nombreProvedor;
      
    }
  
  
    public get idBatch(): number {
      return this.id_lote;
    }
  
    public set idBatch(id_lote: number) {
      this.id_lote = id_lote;
    }
    public get idProvedor(): number {
        return this.id_provedor;
      }
    
      public set idProvedor(id_provedor: number) {
        this.id_provedor = id_provedor;
      }
      public get idProducto(): number {
        return this.id_producto;
      }
    
      public set idProducto(id_producto: number) {
        this.id_producto = id_producto;
      } 
      public get entrada(): number {
        return this.entrada_can;
      }
    
      public set entrada(entrada_can: number) {
        this.entrada_can = entrada_can;
      } 
      public get comprometido(): number {
        return this.comprometido_can;
      }
    
      public set comprometido(comprometido_can: number) {
        this.comprometido_can = comprometido_can;
      }
      public get salida(): number {
        return this.salida_can;
      }
    
      public set salida(salida_can: number) {
        this.salida_can = salida_can;
      }
      public get disponible(): number {
        return this.disponible_cant;
      }
    
      public set disponible(disponible_cant: number) {
        this.disponible_cant = disponible_cant;
      }
      public get usuarioId(): number {
        return this.i_usuario_id;
      }
    
      public set usuarioId(i_usuario_id: number) {
        this.i_usuario_id = i_usuario_id;
      }
      public get fechaRegistro(): string {
        return this.fecha_registro;
      }
    
      public set fechaRegistro(fecha_registro: string) {
        this.fecha_registro = fecha_registro;
      }
      public get nombreUsario(): string {
        return this.v_nombreUsuario;
      }
    
      public set nombreUsario(v_nombreUsuario: string) {
        this.v_nombreUsuario = v_nombreUsuario;
      }
      public get nombreProvedor(): string {
        return this.v_nombreProvedor;
      }
    
      public set nombreProvedor(v_nombreProvedor: string) {
        this.v_nombreProvedor = v_nombreProvedor;
      }
  
   
  
    
    
  }
  
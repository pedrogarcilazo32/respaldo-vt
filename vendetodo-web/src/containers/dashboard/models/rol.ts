export class Rol{
    private id_rol?: number;
    private nombre_rol?: string; 

    constructor(
        id_rol?: number,
        nombre_rol?: string
    ){
        this.id_rol = id_rol;
        this.nombre_rol = nombre_rol;
    }

    //*Getters and Setters
    public get id_Rol(): number{
        return this.id_rol;
    }
    public set id_Rol(id_rol: number){
        this.id_Rol=id_rol;
    }
    public get nombre_Rol(): string{
        return this.nombre_rol;
    }
    public set nombre_Rol(nombre_rol: string){
        this.nombre_Rol=nombre_rol;
    }
}
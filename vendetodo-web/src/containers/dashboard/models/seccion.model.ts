import { Producto } from "./Producto.model";
export class Seccion {
    private id_seccion?: number;
    private capacidad_seccion?: number;
    private num_seccion?:number;
    private cantidad_otorgada?:number;
    private producto?:Producto
    constructor(
        id_seccion?: number,
        capacidad_seccion?: number,
        num_seccion?:number,
        cantidad_otorgada?:number,
        producto?:Producto
    ) {
      this.id_seccion = id_seccion;
      this.capacidad_seccion = capacidad_seccion;
      this.num_seccion=num_seccion;
      this.cantidad_otorgada=cantidad_otorgada
      this.producto=producto

    }

    public crearproducto(nombreProducto:string,image_url) {
      this.producto=new Producto();
      this.producto.nombreProducto=nombreProducto;
      this.producto.image_url=image_url
    }
    public get idSeccion(): number {
      return this.id_seccion;
    }

    public set idSeccion(idSeccion: number) {
      this.id_seccion = idSeccion;
    }
    public get CapacidadSeccion(): number {
        return this.capacidad_seccion;
      }

      public set CapacidadSeccion(capacidad_seccion: number) {
        this.capacidad_seccion = capacidad_seccion;
      }
      public get numSeccion(): number {
        return this.num_seccion;
      }

      public set numSeccion(num_seccion: number) {
        this.num_seccion = num_seccion;
      }
      public get cantidadOtorgada(): number {
        return this.cantidad_otorgada;
      }

      public set cantidadOtorgada(cantidad_otorgada: number) {
        this.cantidad_otorgada = cantidad_otorgada;
      }
      public get productoSeccion(): Producto {
        return this.producto;
      }

      public set productoSeccion(producto: Producto) {
        this.producto = producto;
      }









  }

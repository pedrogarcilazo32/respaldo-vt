import { Producto } from "./Producto";
import { Proveedor } from "./Proveedor";
//import { Seccion } from "./Seccion";

export class DetalleOrden {
  private nombre_producto: string;
  private cantidad: number;
  private nombre_provedor: string;
  private estatus: boolean;
  private id_estante:number;
  private id_seccion:number;
  private idDetalleOrden?: number;
  private costoRuta?:number;
  private estaenBodega?:boolean;

  constructor(
    nombre_producto?: string,
    cantidad?: number,
    nombre_provedor?: string,
    estatus?: boolean,
    idDetalleOrden?: number,
    id_estante?:number,
    id_seccion?:number,
    costoRuta?:number,
    estaenBodega?:boolean
  ) {
    this.nombre_producto = nombre_producto;
    this.cantidad = cantidad;
    this.nombre_provedor = nombre_provedor;
    this.estatus = estatus;
    this.idDetalleOrden = idDetalleOrden;
    this.id_estante=id_estante;
    this.id_seccion=id_seccion;
    this.costoRuta=costoRuta;
    this.estaenBodega=estaenBodega;
  }

  //Cambiar estatus
  // public productoRecogigo(){
  //   this.setEstatus = "Recogido";
  // }

  cambiarEstado(){
    var estado=this.getEstatus;
    this.setEstatus=!estado;
    console.log(this.estatus)
  }
  // Getters and Setters
  public get getIdDetalleOrden(): number {
    return this.idDetalleOrden;
  }

  public set setIdDetalleOrden(idDetalleOrden: number) {
    this.idDetalleOrden = idDetalleOrden;
  }

  public get getNombre_producto(): string {
    return this.nombre_producto;
  }

  public set setNombre_producto(nombre_producto: string) {
    this.nombre_producto = nombre_producto;
  }

  public get getNombre_provedor(): string {
    return this.nombre_provedor;
  }

  public set setNombre_provedor(nombre_provedor: string) {
    this.nombre_provedor = nombre_provedor;
  }

  public get getCantidad(): number {
    return this.cantidad;
  }

  public set setCantidad(cantidad: number) {
    this.cantidad = cantidad;
  }
 public get getEstatus(): boolean {
    return this.estatus;
  }

  public set setEstatus(estatus: boolean) {
    this.estatus = estatus;
  }
  public get idestante(): number {
    return this.id_estante;
  }

  public set idestante(id_estante: number) {
    this.id_estante = id_estante;
  }
  public get idSeccion(): number {
    return this.id_seccion;
  }

  public set idSeccion(id_seccion: number) {
    this.id_seccion = id_seccion;
  }
  public get GetcostoRuta(): number {
    return this.costoRuta;
  }

  public set setCostoRuta(costoRuta: number) {
    this.costoRuta = costoRuta;
  }
  public get GetestaenBodega(): boolean {
    return this.estaenBodega;
  }

  public set setestaenBodega(estaenBodega: boolean) {
    this.estaenBodega = estaenBodega;
  }
  // public get getSeccion(): Seccion {
  //   return this.seccion;
  // }

  // public set setSeccion(seccion: Seccion) {
  //   this.seccion = seccion;
  // }


}

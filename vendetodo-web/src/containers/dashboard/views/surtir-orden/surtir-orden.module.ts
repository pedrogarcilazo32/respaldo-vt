import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SurtirOrdenPageRoutingModule } from './surtir-orden-routing.module';

import { SurtirOrdenPage } from './surtir-orden.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SurtirOrdenPageRoutingModule
  ],
  declarations: [SurtirOrdenPage]
})
export class SurtirOrdenPageModule {}

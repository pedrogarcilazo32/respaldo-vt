import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SurtirOrdenPage } from './surtir-orden.page';

const routes: Routes = [
  {
    path: '',
    component: SurtirOrdenPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SurtirOrdenPageRoutingModule {}

import { Component, OnInit } from '@angular/core';
import { ModeloSurtir } from '../../Modelos/ModeloSurtir';
import { Constants } from 'src/utils/constans.utils';
import { AlertController } from '@ionic/angular';
import { FormatUtils } from 'src/utils/format.utils';
import { modeloEstante } from '../../Modelos/modeloEstante';
import { Orden } from '../../models/Orden';
import { DetalleOrden } from '../../models/DetalleOrden';
import { MessagingService } from 'src/utils/messaging.service';

@Component({
  selector: 'app-surtir-orden',
  templateUrl: './surtir-orden.page.html',
  styleUrls: ['./surtir-orden.page.scss'],
})
export class SurtirOrdenPage implements OnInit {
  array = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 1, 1, 1, 1, 1, 1, 1, 1, 10, 100, 10];

  //aqui empieza lo chido
  banderaOrdenPendiente: boolean = false;
  estante: number = 2;
  usuario_id: number = 18;
  ordenAct: Orden;
  detalleOrden: Array<DetalleOrden>;
  banderaFinalizar: boolean = false;
  banderaBoton:boolean=true;
  constructor(
    private ModeloSurtir: ModeloSurtir,
    private format: FormatUtils,
    private alertCon: AlertController,
    private MessagingService: MessagingService
  ) {
    this.ModeloSurtir = ModeloSurtir;
  }

  ngOnInit() {
    this.obtenerOrdenPendiente();
  }
  formatearFecha(date: string) {
    let fecha = '';
    if (date) {
      fecha =
        this.format.formatDateMXN(date) + ' ' + this.format.getHora24(date);
    }
    return fecha;
  }
  validarEdoOrden() {
    setTimeout(() => {
      var lista = this.detalleOrden.filter((det) => !det.getEstatus);
      if (lista.length == 0) {
        this.banderaFinalizar = true;
      } else {
        this.banderaFinalizar = false;
      }
      console.log(lista, this.banderaFinalizar);
    }, 500);
  }
  async cambiarEstado(item: DetalleOrden) {
    this.validarEdoOrden();

    const data = await this.ModeloSurtir.cambiarEstado(
      this.ordenAct,
      item.getIdDetalleOrden
    );
  }

  async solicitarOrden() {
    const data = await this.ModeloSurtir.solicitarOrden(
      this.usuario_id,
      this.estante
    );
    if (data) {
      this.ordenAct = data;
      this.detalleOrden = this.ordenAct.getListaDetalleOrden;
      this.banderaOrdenPendiente = true;
    } else {
      this.MessagingService.warning('No hay ordenes pendientes');
      this.banderaOrdenPendiente = false;
    }
  }
  async obtenerOrdenPendiente() {
    const data = await this.ModeloSurtir.obtenerOrdenPendiente(
      this.usuario_id,
      this.estante
    );
    if (data) {
      this.ordenAct = data;
      this.detalleOrden = this.ordenAct.getListaDetalleOrden;
      this.banderaOrdenPendiente = true;
      this.validarEdoOrden();
    } else {
      this.banderaOrdenPendiente = false;
      this.banderaBoton=false;
    }
  }

  async finalizarOrden() {
    const data = await this.ModeloSurtir.finalizarOrden(this.ordenAct);
    console.log(data);

    this.MessagingService.success('Orden finalizada con éxito');
    this.ordenAct = new Orden();
    this.detalleOrden = [];
    this.banderaOrdenPendiente = false;
    this.banderaBoton=true;
  }
  async comfirmarAlert(opc: number) {
    const alert = await this.alertCon.create({
      cssClass: 'my-custom-class',
      header: 'Confirmación',
      message: '¿Estás seguro?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {},
        },
        {
          text: 'Aceptar',
          handler: () => {
            if (opc == 1) {
              this.finalizarOrden();
            }
            else if(opc==2){
              this.solicitarOrden()
            }
          },
        },
      ],
    });

    await alert.present();
  }
}

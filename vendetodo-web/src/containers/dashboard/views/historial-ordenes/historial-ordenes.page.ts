import { Component, OnInit } from '@angular/core';
import { ModeloSurtir } from '../../Modelos/ModeloSurtir';
import { Orden } from '../../models/Orden';
import { FormatUtils } from 'src/utils/format.utils';

@Component({
  selector: 'app-historial-ordenes',
  templateUrl: './historial-ordenes.page.html',
  styleUrls: ['./historial-ordenes.page.scss'],
})
export class HistorialOrdenesPage implements OnInit {
  public arrayOrdenes = Array<Orden>();
  idUsuario: number = 18;

  constructor(
    private modeloSurtir: ModeloSurtir,
    private format: FormatUtils
    ) {
    this.modeloSurtir = modeloSurtir;
  }

  ngOnInit() {
    this.obtenerOrdenes();
  }

  async obtenerOrdenes() {
    const data = await this.modeloSurtir.obtenerOrdenes(this.idUsuario);
    if (data.length) {
      this.arrayOrdenes = this.arrayOrdenes.concat(data);
    }
  }
  formatearFecha(date: string) {
    let fecha = '';
    if (date) {
      fecha =
        this.format.formatDateMXN(date) + ' ' + this.format.getHora24(date);
    }
    return fecha;
  }
}

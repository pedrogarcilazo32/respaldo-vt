import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CatalogoEmpleadosPageRoutingModule } from './catalogo-empleados-routing.module';
import { AgregarEmpleadoPageModule } from 'src/components/agregar-empleado/agregar-empleado.module';

import { CatalogoEmpleadosPage } from './catalogo-empleados.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    CatalogoEmpleadosPageRoutingModule,
    AgregarEmpleadoPageModule
  ],
  declarations: [CatalogoEmpleadosPage]
})
export class CatalogoEmpleadosPageModule {}

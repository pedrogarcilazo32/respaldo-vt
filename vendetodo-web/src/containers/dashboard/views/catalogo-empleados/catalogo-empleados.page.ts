import { Component, OnInit } from '@angular/core';
import { EmpleadoModel } from '../../modelos/EmpleadoModel';
import { Empleado } from '../../models/empleado.model';
import { ModalController } from '@ionic/angular';
import { AgregarEmpleadoPage } from 'src/components/agregar-empleado/agregar-empleado.page';

@Component({
  selector: 'app-catalogo-empleados',
  templateUrl: './catalogo-empleados.page.html',
  styleUrls: ['./catalogo-empleados.page.scss'],
})
export class CatalogoEmpleadosPage implements OnInit {
  arrayEmpleados = Array<Empleado>();
  infiniteDisabled: boolean = false;
  constructor(
    private empleadoModel: EmpleadoModel,
    private modalController: ModalController,
  ) { }

  ngOnInit() {
    this.obtenerEmpleados(this.empleadoModel.Obtenerpagina(0));
  }

  async modalAgregaEmpleados() {
    const modal = await this.modalController.create({
      component: AgregarEmpleadoPage,
      cssClass: 'modal-agrega-empleados',
      componentProps: {
        edita: false,
        empleadoEdita: null,
      },
    });

    modal.onDidDismiss().then(() => {
      this.empleadoModel.Obtenerpagina(2);
      this.obtenerEmpleados(this.empleadoModel.Obtenerpagina(0));
    });

    return await modal.present();
  }

  async obtenerEmpleados(pagina: number) {

    const data = await this.empleadoModel.obtenerEmpleados(pagina);

    if (data.length) {

      console.log('pagina',data)
      if (pagina > 1) {
        console.log(1)
        this.arrayEmpleados = this.arrayEmpleados.concat(data)
        }
          else{
            this.infiniteDisabled = false;
            this.arrayEmpleados = data
            console.log(2)
          }
        }
     }

     loadData(event) {
      setTimeout(() => {
        if (
          this.empleadoModel.Obtenerpagina(0) ==
          this.arrayEmpleados[0].total_Paginas
        ) {
          event.target.complete();
          this.infiniteDisabled = true;
        } else {
          this.empleadoModel.Obtenerpagina(1);
          this.obtenerEmpleados(this.empleadoModel.Obtenerpagina(0));
          event.target.complete();
        }
      }, 500);
    }
}

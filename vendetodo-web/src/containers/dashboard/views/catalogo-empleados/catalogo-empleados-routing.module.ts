import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CatalogoEmpleadosPage } from './catalogo-empleados.page';

const routes: Routes = [
  {
    path: '',
    component: CatalogoEmpleadosPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CatalogoEmpleadosPageRoutingModule {}

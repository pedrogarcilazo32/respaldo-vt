import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HistorialEstantePage } from './historial-estante.page';

const routes: Routes = [
  {
    path: '',
    component: HistorialEstantePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HistorialEstantePageRoutingModule {}

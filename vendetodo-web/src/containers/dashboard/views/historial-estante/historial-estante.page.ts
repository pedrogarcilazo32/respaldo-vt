import { Component, OnInit } from '@angular/core';
import { FormatUtils } from 'src/utils/format.utils';
import { modeloEstante } from '../../modelos/modeloEstante';
import { Estante } from '../../models/estante.model';

@Component({
  selector: 'app-historial-estante',
  templateUrl: './historial-estante.page.html',
  styleUrls: ['./historial-estante.page.scss'],
})
export class HistorialEstantePage implements OnInit {
  public arrayEstante = Array<Estante>();
  idEstante: number = 2;

  constructor(
    private modeloEstante: modeloEstante,
    private format: FormatUtils
  ) { }

  ngOnInit() {
    this.obtenerOrdenamientos();
  }
  async obtenerOrdenamientos() {
    console.log("el ts");
    const data = await this.modeloEstante.obtenerOrdenamientos(this.idEstante);
    console.log(data);
    if (data.length) {
      this.arrayEstante = this.arrayEstante.concat(data);
    }
  }

  formatearFecha(date: string) {
    let fecha = '';
    if (date) {
      fecha =
        this.format.formatDateMXN(date) + ' ' + this.format.getHora24(date);
    }
    return fecha;
  }
}

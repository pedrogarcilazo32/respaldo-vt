import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HistorialEstantePageRoutingModule } from './historial-estante-routing.module';

import { HistorialEstantePage } from './historial-estante.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HistorialEstantePageRoutingModule
  ],
  declarations: [HistorialEstantePage]
})
export class HistorialEstantePageModule {}

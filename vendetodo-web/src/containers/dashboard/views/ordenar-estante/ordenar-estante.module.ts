import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OrdenarEstantePageRoutingModule } from './ordenar-estante-routing.module';

import { OrdenarEstantePage } from './ordenar-estante.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OrdenarEstantePageRoutingModule
  ],
  declarations: [OrdenarEstantePage]
})
export class OrdenarEstantePageModule {}

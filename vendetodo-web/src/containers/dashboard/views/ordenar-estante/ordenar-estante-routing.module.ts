import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OrdenarEstantePage } from './ordenar-estante.page';

const routes: Routes = [
  {
    path: '',
    component: OrdenarEstantePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OrdenarEstantePageRoutingModule {}

import { Component, OnInit } from '@angular/core';
import { modeloEstante } from '../../modelos/modeloEstante';
import { Estante } from '../../models/estante.model';
import { Seccion } from '../../models/seccion.model';
import { Constants } from 'src/utils/constans.utils';
import { AlertController } from '@ionic/angular';
import { FormatUtils } from 'src/utils/format.utils';
import { MessagingService } from 'src/utils/messaging.service';


@Component({
  selector: 'app-ordenar-estante',
  templateUrl: './ordenar-estante.page.html',
  styleUrls: ['./ordenar-estante.page.scss'],
})
export class OrdenarEstantePage implements OnInit {

  //array = [1, 2, 3, 4, 5, 6]
  estante_id=1;
  usuario_id =1;
  estanteActual:Estante;
  estantePendiente:Estante;
  secciones:Array<Seccion>;
  seccionesPendiente:Array<Seccion>;
  banderaPendiente:boolean = true;
  banderaVerPendiente:boolean=false;

  item1: any = '../../../assets/img/prodDefault.png';
  constructor(
    private modeloEstante:modeloEstante,
    private constants: Constants,
    private alertCon: AlertController,
    private format: FormatUtils,
    private MessagingService:MessagingService


  ) {

    this.modeloEstante=modeloEstante

  }

  ngOnInit() {
    this.obtenerEstanteActual();
  }
  formatearFecha(date: string) {
    let fecha = '';
    if (date){
      fecha= this.format.formatDateMXN(date) + ' ' + this.format.getHora24(date);
    }
    return fecha

  }
  getMinHeight(item){

    //item=item * 10
    var porcentaje = (item.cantidad_otorgada / item.capacidad_seccion) * 100
    var cadena=parseInt(porcentaje+'');
    return cadena+'%'
  }
  async obtenerEstanteActual() {
    const data = await this.modeloEstante.obtenerEstanteActual(this.estante_id);
    if (data) {
      if (data.Secciones) {
        this.estanteActual = data;
        this.secciones=this.estanteActual.Secciones
        console.log('actual',this.secciones)
      }
      this.obtenerEstantePendiente()
    }
  }
  async obtenerEstantePendiente() {
    const data = await this.modeloEstante.obtenerEstantePendiente(this.estante_id);
    if (data) {
      if (data.Secciones) {
        this.estantePendiente = data;
        this.seccionesPendiente=this.estantePendiente.Secciones
        console.log('pendiente',this.seccionesPendiente)
        this.seccionesPendiente.length ? this.banderaPendiente=true:this.banderaPendiente=false
      }
      else{
        this.banderaPendiente=false;
      }
    }
    else{
      this.banderaPendiente=false;
    }


  }

  CambiarVista(banderaVista:boolean){

    this.banderaVerPendiente=banderaVista;
  }
  async solicitarReorden(){
    const data = await this.modeloEstante.solicitarReorden(this.estante_id);
    if (data) {
      if (data.Secciones) {
        this.estantePendiente = data;
        this.seccionesPendiente=this.estantePendiente.Secciones
        console.log('pendiente',this.seccionesPendiente)
        this.banderaPendiente=true;
        this.MessagingService.success('Operacion realizada con éxito')
        this.CambiarVista(true)
      }
      else{
        this.banderaPendiente=false;
      }
    }
  }
  async confirmarNuevoOrden(){
    const data = await this.modeloEstante.confirmarNuevoOrden(this.estante_id);
    if (data) {

     console.log('confirmar data',data)
     this.CambiarVista(false)
     this.obtenerEstanteActual();
     this.MessagingService.success('Operacion realizada con éxito')
    }
  }
  async comfirmarAlert(opc:number) {
    const alert = await this.alertCon.create({
      cssClass: 'my-custom-class',
      header: 'Confirmación',
      message: '¿Estás seguro?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => { },
        },
        {
          text: 'Aceptar',
          handler: () => {
            if(opc==1){
              this.solicitarReorden();
            }
            else{
              this.confirmarNuevoOrden();
            }
          },
        },
      ],
    });

    await alert.present();
  }

}

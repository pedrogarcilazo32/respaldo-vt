import { Component, OnInit } from '@angular/core';
import { AgregarProductoPage } from '../../../../components/agregar-producto/agregar-producto.page';
import { ModalController } from '@ionic/angular';
import { Producto } from '../../../dashboard/models/Producto.model';
import { AlertController } from '@ionic/angular';
import { ManejadoraProducto } from '../../modelos/manejadoraProducto';
import { LoadingService } from 'src/utils/loading.service';
import { MessagingService } from 'src/utils/messaging.service';
import { FormatUtils } from 'src/utils/format.utils';
import { ValidatorsUtils } from 'src/utils/validators.utils';
// import { filtroProducts } from '../../models/filtroProducts.model';
// import { FiltroProductosPage } from 'src/components/filtro-productos/filtro-productos.page';
import { AumentarExistenciaComponent } from 'src/components/aumentar-existencia/aumentar-existencia.component';
@Component({
  selector: 'app-catalogo-productos',
  templateUrl: './catalogo-productos.page.html',
  styleUrls: ['./catalogo-productos.page.scss'],
})
export class CatalogoProductosPage implements OnInit {
  private arrayProductos = Array<Producto>();
  private search: string = '';
  // private filtro: filtroProducts;
  private infiniteDisabled: boolean = false;
  constructor(
    private modalController: ModalController,
    private alertCon: AlertController,
    private manejaProductos: ManejadoraProducto,
    private loadingService: LoadingService,
    private messaginService: MessagingService,
    private format: FormatUtils,
    private ValidatorsUtils: ValidatorsUtils
  ) {
    this.modalController = modalController;
    this.format = format;
    this.loadingService = loadingService;
    this.messaginService = messaginService;
    this.alertCon = alertCon;
    this.manejaProductos = manejaProductos;
    this.ValidatorsUtils = ValidatorsUtils;

  }

  ngOnInit() {
    // this.inicializarFiltro();
    this.obtenerProductos(this.manejaProductos.Obtenerpagina(0));
  }

  async obtenerProductos(pagina: number) {

    const data: any = await this.manejaProductos.obtenerProductos(pagina);

    console.log('dataa catalogo', data);
    if (data.length) {
      if (pagina > 1) {
        this.arrayProductos = this.arrayProductos.concat(data);
      }
      else {
        this.infiniteDisabled = false;
        this.arrayProductos = data;
      }
      // pagina == 1 ? this.arrayProductos = data : this.arrayProductos.concat(data);

    }

    //PENDIENTE CHECAR PARA REFACTORIZAR
    // this.loadingService.present();
    // this.manejaProductos
    //   .obtenerProductos(pagina)
    //   .then((response: any) => {
    //     this.loadingService.dismiss();
    //     if (response.isValid) {
    //       pagina == 1
    //         ? (this.arrayProductos = response.data)
    //         : this.arrayProductos.concat(response.data);
    //       if (pagina == 1) {
    //         this.infiniteDisabled = false;
    //       }
    //       console.log(this.arrayProductos);
    //     } else {
    //       this.loadingService.dismiss();
    //       this.messaginService.warning("No se encontraron productos");
    //     }
    //   })
    //   .catch(() => {
    //     this.loadingService.dismiss();
    //     // arrayproductos = [];
    //   });

    // this.manejaProductos
    //   .obtenerProductos(pagina)
    //   .then((data: Array<Producto>) => {
    //     if (data.length) {
    //       this.Productos = data;
    //     }
    //   })
    //   .catch(() => {
    //   });
  }

  // buscarProducto(banderaFiltros: boolean) {
  //   if (this.search != null) {
  //     if (this.search.trim().length == 0 && !banderaFiltros) {
  //       this.manejaProductos.Obtenerpagina(2);
  //       // this.inicializarFiltro();
  //       this.obtenerProductos(this.manejaProductos.Obtenerpagina(0));
  //     } else {
  //       this.arrayProductos = [];
  //       this.infiniteDisabled = true;
  //       this.filtro.search = this.search;
  //       this.loadingService.present();
  //       this.manejaProductos
  //         .buscarProducto(this.filtro)
  //         .then((response: any) => {
  //           if (response.isValid) {
  //             this.loadingService.dismiss();
  //             if (response.data.length > 0) {
  //               this.arrayProductos = response.data;
  //             } else {
  //               this.messaginService.warning('No se encontraron productos');
  //             }
  //           } else {
  //             this.loadingService.dismiss();
  //             this.messaginService.warning('No se pudieron obtener productos');
  //           }
  //         })
  //         .catch(() => {
  //           this.loadingService.dismiss();
  //           // arrayproductos = [];
  //         });
  //     }
  //   }
  // }

  async levantarModalAgregarProductos() {
    const modal = await this.modalController.create({
      component: AgregarProductoPage,
      cssClass: 'modal-agrega-productos',
      componentProps: {
        edita: false,
        productoEdita: null,
      },
    });

    modal.onDidDismiss().then(() => {
      this.manejaProductos.Obtenerpagina(2);
      // this.inicializarFiltro();
      this.obtenerProductos(this.manejaProductos.Obtenerpagina(0));
    });

    return await modal.present();
  }

  async levantarModalEditaProductos(pro: Producto) {
    // const productoEdita = new Producto(
    //   pro.nombreProducto,
    //   pro.volumenProducto,
    //   pro.precioProducto,
    //   1,
    //   pro.idMarca,
    //   pro.id_categoria,
    //   null,
    //   null,
    //   null,
    //   null,
    //   pro.idProducto,
    //   pro.v_descripcion,
    //   pro.v_image_url
    // );

    const modal = await this.modalController.create({
      component: AgregarProductoPage, // <-- este recibe los props
      cssClass: 'modal-agrega-productos',
      componentProps: {
        edita: true,
        productoEdita: pro,
      },
    });

    modal.onDidDismiss().then(() => {
      this.manejaProductos.Obtenerpagina(2);

      // this.inicializarFiltro();
      this.obtenerProductos(this.manejaProductos.Obtenerpagina(0));
    });
    return await modal.present();
  }

  // inicializarFiltro() {
  //   this.filtro = new filtroProducts(
  //     '',
  //     '',
  //     0,
  //     0,
  //     '',
  //     '',
  //     '',
  //     0,
  //     ''
  //   );
  // }

  // async eliminarProductoAlert(pro: Producto) {
  //   const alert = await this.alertCon.create({
  //     cssClass: 'my-custom-class',
  //     header: 'Confirmación',
  //     message: '¿Esta seguro de eliminar este Producto?',
  //     buttons: [
  //       {
  //         text: 'Cancelar',
  //         role: 'cancel',
  //         cssClass: 'secondary',
  //         handler: () => { },
  //       },
  //       {
  //         text: 'Aceptar',
  //         handler: () => {
  //           this.eliminarProducto(pro);
  //         },
  //       },
  //     ],
  //   });

  //   await alert.present();
  // }

  // eliminarProducto(pro: Producto) {
  //   this.loadingService.present();
  //   this.manejaProductos
  //     .eliminarProductos(pro)
  //     .then((response: any) => {
  //       if (response.isValid) {
  //         this.loadingService.dismiss();
  //         this.messaginService.success('Producto eliminado con exito!');
  //         this.manejaProductos.Obtenerpagina(2);

  //         this.obtenerProductos(this.manejaProductos.Obtenerpagina(0));
  //       } else {
  //         this.loadingService.dismiss();
  //         this.messaginService.warning('No se pudo eliminar el Producto');
  //       }
  //     })
  //     .catch((e) => {
  //       this.loadingService.dismiss();
  //       // arrayproductos = [];
  //       console.log('erros =>>', e);
  //     });
  // }

  formatearFecha(date: string) {
    return this.format.formatDateD(date) + ' ' + this.format.getHora24(date);
  }

  soloLetras(e, tipo: string) {
    return this.ValidatorsUtils.soloLetras(e, tipo);
  }

  loadData(event) {
    setTimeout(() => {
      if (
        this.manejaProductos.Obtenerpagina(0) ==
        this.arrayProductos[0].totalPaginas
      ) {
        event.target.complete();
        this.infiniteDisabled = true;
      } else {
        this.manejaProductos.Obtenerpagina(1);
        this.obtenerProductos(this.manejaProductos.Obtenerpagina(0));
        event.target.complete();
      }
    }, 500);
  }

  // async levantarModalFiltroProducts() {
  //   const modal = await this.modalController.create({
  //     component: FiltroProductosPage,
  //     cssClass: 'modal-filtro-products',
  //     componentProps: {},
  //   });

  //   modal.onDidDismiss().then((response: any) => {
  //     if (response.data) {
  //       this.filtro = response.data;
  //       this.filtro.search = this.search;
  //       this.buscarProducto(true);
  //       //console.log(this.filtro)
  //     }
  //     else {
  //       this.inicializarFiltro();
  //     }
  //   });
  //   // const response = await modal.onDidDismiss();
  //   // if(response.data){
  //   //   this.filtro=response.data;
  //   //   this.filtro.search=this.search;
  //   //   this.buscarProducto(true)
  //   // }

  //   return await modal.present();
  // }

  //PENDIENTE AGREGAR CONSTRUCTOR Y GET AND SET CLASE FILTRO
  // removerFiltro(nombreFiltro: string) {
  //   if (nombreFiltro == 'empleado') {
  //     this.filtro.i_usuario_id = 0;
  //     this.filtro.v_nombreEmpleado = '';
  //   } else if (nombreFiltro == 'marca') {
  //     this.filtro.id_marca = 0;
  //     this.filtro.v_nombreMarca = '';
  //   } else if (nombreFiltro == 'fecha') {
  //     this.filtro.f_registro_fin = '';
  //     this.filtro.f_registro_inicio = '';
  //   } else if (nombreFiltro == 'categoria') {
  //     this.filtro.id_categoria = 0;
  //     this.filtro.v_nombreCategoria = '';
  //   }
  //   if (
  //     this.filtro.i_usuario_id == 0 &&
  //     this.filtro.id_marca == 0 &&
  //     this.filtro.f_registro_inicio == '' &&
  //     this.filtro.f_registro_fin == '' &&
  //     this.filtro.id_categoria == 0
  //   ) {
  //     this.buscarProducto(false);
  //   } else {
  //     // this.buscarProducto(true);
  //   }
  // }

  async levantarModalAumentarExistencia(pro: Producto) {
    // const productoEdita = new Producto(
    //   pro.nombreProducto,
    //   pro.volumenProducto,
    //   pro.precioProducto,
    //   1,
    //   pro.idMarca,
    //   pro.id_categoria,
    //   null,
    //   null,
    //   null,
    //   null,
    //   pro.idProducto,
    //   pro.v_descripcion,
    //   pro.v_image_url
    // );
    const modal = await this.modalController.create({
      component: AumentarExistenciaComponent,
      cssClass: 'modal-existencia',
      componentProps: {
        productoEdita: pro
      },
    });

    modal.onDidDismiss().then(() => {
      // this.manejaProductos.Obtenerpagina(2);
      // this.inicializarFiltro();
      // this.obtenerProductos(this.manejaProductos.Obtenerpagina(0));
    });

    return await modal.present();
  }
}

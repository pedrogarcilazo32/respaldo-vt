import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CatalogoProductosPageRoutingModule } from './catalogo-productos-routing.module';
import { CatalogoProductosPage } from './catalogo-productos.page';
import { MatTooltipModule } from '@angular/material/tooltip';
import { AgregarProductoPageModule } from '../../../../components/agregar-producto/agregar-producto.module';
// import { FiltroProductosPageModule } from 'src/components/filtro-productos/filtro-productos.module'
import { AumentarExistenciaComponentModule } from 'src/components/aumentar-existencia/aumentar-existencia.module';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CatalogoProductosPageRoutingModule,
    MatTooltipModule,
    AgregarProductoPageModule,
    // FiltroProductosPageModule,
    AumentarExistenciaComponentModule
  ],
  declarations: [CatalogoProductosPage]
})
export class CatalogoProductosPageModule {}

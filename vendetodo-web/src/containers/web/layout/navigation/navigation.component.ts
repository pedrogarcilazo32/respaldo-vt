import { Component, OnInit } from '@angular/core';
import { ComunicacionService } from 'src/utils/comunicacion.service';
import { ValidatorsUtils } from 'src/utils/validators.utils';
import { EventManager } from 'src/utils/event-manager';
import { Router } from '@angular/router';
import { ModeloCarrito } from 'src/containers/dashboard/modelos/ModeloCarrito';
import { Storage } from 'src/utils/storage';
import { Constants } from 'src/utils/constans.utils';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss'],
})
export class NavigationComponent implements OnInit {

  Categories : Array<any> = [
    { value: 'tecnologia', label: 'Tecnología', idCategoria: 11},
    { value: 'herramientas', label: 'Herramientas', idCategoria: 5},
    { value: 'libros', label: 'Libros',idCategoria: 3},
    { value: 'electrodomesticos', label: 'Electrodomésticos', idCategoria: 2}];

  searchProduct: string = 'mous';
  message: string;
  contador: number;
  private existUser: any;
  private isPayment: boolean = false;
  private url: any;

  constructor(
    private ValidatorsUtils: ValidatorsUtils,
    private comunicacion: ComunicacionService,
    private eventManager: EventManager,
    private router: Router,
    private modeloCarrito: ModeloCarrito,
    private storage: Storage,
    private constants: Constants
  ) {
    this.modeloCarrito = modeloCarrito;
    this.router = router;

  }

  async ngOnInit() {
    this.existUser = localStorage.getItem(this.constants.getConstants().VENDETODO_SESSION);


    if(this.existUser){
      this.contador = await this.modeloCarrito.getNumLineasCarrito();
    }
  }

  soloLetras(e: any, tipo: string) {
    return this.ValidatorsUtils.soloLetras(e, tipo);
  }

  onClickOption(item: string){
    if(item === 'todo'){
      this.eventManager.emit('show-sidebar');
    }
  }

  goToHome(){
    this.router.navigate(['/']);
  }

  public goToCategorie(idCat: number, nomCat:string){
    this.eventManager.emit('ver-categoria');
    setTimeout(()=>{
      // this.router.navigateByUrl(`/categorie?idCat=${idCat}&nomCat=${nomCat}`);
      this.router.navigateByUrl(`/categorie?idCat=${idCat}&nomCat=${nomCat}`);
    },100);

    // this.router.navigateByUrl(`/categorie?idCat=${idCat}&nomCat=${nomCat}`);
    // window.history.pushState(null, 'Page Title', `/categorie?idCat=${idCat}&nomCat=${nomCat}`);
  }

  logOut(){
    localStorage.removeItem(this.constants.getConstants().VENDETODO_SESSION);
    this.existUser = null;
    this.router.navigateByUrl('/');
  }
}

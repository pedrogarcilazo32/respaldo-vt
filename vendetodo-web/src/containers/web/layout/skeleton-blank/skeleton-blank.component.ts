import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-skeleton-blank',
  templateUrl: './skeleton-blank.component.html',
  styleUrls: ['./skeleton-blank.component.scss'],
})
export class SkeletonBlankComponent implements OnInit {

  constructor() { }

  ngOnInit() {}
  getMinHeight() {
    const mainFooter = document.getElementById('main-footer');
    let mainFooterHeight = 0;
    if (mainFooter) {
      mainFooterHeight= mainFooter.offsetHeight;
    }

    const windowHeight = window.innerHeight;

    return windowHeight - mainFooterHeight;
  }
}

import { Component, OnInit } from '@angular/core';
import { ComunicacionService } from 'src/utils/comunicacion.service';
import { EventManager } from 'src/utils/event-manager';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
})
export class SidebarComponent implements OnInit {

  Options : Array<any> = [
    { value: 'tecnologia', label: 'Tecnologia'},
    { value: 'herramientas', label: 'Herramientas'},
    { value: 'libros', label: 'Libros'},
    { value: 'electrodomesticas', label: 'Electrodomesticas'}];;

  public valorEmitido:boolean;

  callbackOpenSidebar: any;

  constructor(private comunicacion: ComunicacionService, private eventManager: EventManager) { }

  ngOnInit() {
    this.callbackOpenSidebar = this.eventManager.on('show-sidebar', () => {
      this.valorEmitido = true;

      setTimeout(() => {
        document.getElementById('sidebar').style.transform = 'translateX(0)';
      }, 100);

    });
  }

  ngOnChange() {
  }

  ngOnDestroy() {
    this.eventManager.unsubscribe('sidebar' , this.callbackOpenSidebar);
  }

  closeSidebar() {
    this.valorEmitido = false;
  }


  ngOnChanges() {
  }

  async checkValor(){
    console.log('emito valores', this.valorEmitido);

  }

  clickContainer(){
    // this.valorEmitido = false;
    setTimeout(() => {
      document.getElementById('sidebar').style.transform = 'translateX(-100%)';
    }, 100);
  }

}

import { Component, OnInit } from '@angular/core';
import { Storage } from 'src/utils/storage';
import { ModeloUsuario } from 'src/containers/dashboard/modelos/ModeloUsuario';
import { Carrito } from 'src/containers/dashboard/models/Carrito';
import { Constants } from 'src/utils/constans.utils';
import decode from 'jwt-decode';


@Component({
  selector: 'app-skeleton',
  templateUrl: './skeleton.component.html',
  styleUrls: ['./skeleton.component.scss'],
})
export class SkeletonComponent implements OnInit {
  sizeScreen: number = this.getMinHeight();


  constructor(private storage: Storage, private modeloUsuario: ModeloUsuario, private constants: Constants) {
    this.storage = storage;
    this.constants = constants;
    this.modeloUsuario = modeloUsuario;

    const token = localStorage.getItem(this.constants.getConstants().VENDETODO_SESSION);

    if(token){
      const data = decode(token);
      const  {id_usuario } = data[0];
      const carrito = new Carrito(id_usuario);
      this.storage.save('carrito', carrito);
    }
  }

  async ngOnInit() {

    // this.modeloUsuario.obtenerUsuario(id_usuario);
  }

  offset(el) {
    var rect = el.getBoundingClientRect(),
      scrollLeft =
        window.pageXOffset || window.document.documentElement.scrollLeft,
      scrollTop =
        window.pageYOffset || window.document.documentElement.scrollTop;
    return { top: rect.top + scrollTop, left: rect.left + scrollLeft };
  }

  scrollToView() {
    const container = document.getElementById('container-web');
    const elementOffset = this.offset(container);
    console.log(elementOffset);

    if (elementOffset) {
      window.scrollTo({ top: elementOffset.top - 180, behavior: 'auto' });
    }
  }

  goToUp() {
    this.scrollToView();
  }

  getMinHeight() {
    const mainFooter = document.getElementById('main-footer');
    let mainFooterHeight = 0;
    if (mainFooter) {
      mainFooterHeight = mainFooter.offsetHeight;
    }

    const windowHeight = window.innerHeight;

    return windowHeight - mainFooterHeight;
  }
}

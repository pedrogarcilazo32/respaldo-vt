import { Component, OnInit } from '@angular/core';
import {
  FormControl,
  Validators,
  FormGroup,
  FormBuilder,
} from '@angular/forms';
import { Router } from '@angular/router';
import { ClienteModel } from 'src/containers/dashboard/modelos/ClienteModel';
import { ValidatorsUtils } from 'src/utils/validators.utils';
import { MessagingService } from '../../../../utils/messaging.service';
import { Usuario } from '../../../dashboard/models/usuario';
@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})
export class SignupPage implements OnInit {
  correoInvalid: boolean = false;
  textoCorreo: string = '';

  usuarioForm: FormGroup;
  validationMessages = {
    nombre: [{ type: 'required', message: 'El nombre es requerido' }],
    ape_paterno: [{ type: 'required', message: 'El apellido paterno es requerido' }],
    ape_materno: [{ type: 'required', message: 'El apellido materno es requerido' }],
    telefono: [{ type: 'required', message: 'El telefono es requerido' },
               {type: 'minlength', message: ' Formato Invalido'}],
    cp: [{ type: 'required', message: 'El codigo postal es requerido' },
         { type: 'minlength', message: 'Formato Invalido'}],
    calle: [{ type: 'required', message: 'La calle es requerida' }],
    ciudad: [{ type: 'required', message: 'La ciudad es requerida' }],
    no_exterior: [{ type: 'required', message: 'El numero exterior es requerido' }],
    correo: [{ type: 'required', message: 'El correo es requerido' },
             { type: 'pattern', message: 'Formato Invalido'}],
    contra: [{ type: 'required', message: 'La contraseña es requerida' }],
  };

  constructor(
    private formBuilder: FormBuilder,
    private messaginservice: MessagingService,
    private clienteModel: ClienteModel,
    private router: Router,
    private ValidatorsUtils: ValidatorsUtils,
  ) {
    this.usuarioForm = this.formBuilder.group({
      nombre: new FormControl('',Validators.compose([Validators.required])),
      ape_paterno: new FormControl('',Validators.compose([Validators.required])),
      ape_materno: new FormControl('',Validators.compose([Validators.required])),
      telefono: new FormControl('',Validators.compose([Validators.required,Validators.minLength(10)])),
      calle: new FormControl('',Validators.compose([Validators.required])),
      ciudad: new FormControl('',Validators.compose([Validators.required])),
      no_exterior: new FormControl('',Validators.compose([Validators.required])),
      correo: new FormControl('', Validators.compose([Validators.required,Validators.pattern(/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)])),
      contra: new FormControl('', Validators.compose([Validators.required])),
      cp: new FormControl('', Validators.compose([Validators.required,Validators.minLength(5)])),
    });

  }

  ngOnInit() {
  }

  signup(){
    let valores = this.usuarioForm.value;
    this.clienteModel.agregarUsuario(
      valores.correo,
      valores.contra,
      valores.nombre,
      valores.ape_paterno,
      valores.ape_materno,
      valores.telefono,
      valores.cp,
      valores.calle,
      valores.ciudad,
      valores.no_exterior)
      .then((response: any) => {
        if (response.isValid) {
          this.messaginservice.success('Usuario guardado con exito!');
          this.router.navigate(['login']);

        } else {
          this.messaginservice.warning('No se pudo guardar el Usuario');
        }
      })
      .catch((e) => {
        console.log(e);
      });
  }

  soloLetras(e, tipo: string) {
    return this.ValidatorsUtils.soloLetras(e, tipo);
  }
  comprobarCorreo() {

    if (this.usuarioForm.get('correo').valid) {
      this.clienteModel.validarCorreo(this.textoCorreo).then(
        (response: Array<Usuario>) => {
          if (response.length > 0) {
            this.correoInvalid = true;
          } else {
            this.correoInvalid = false;
          }
        }

      );
    }
  }
}

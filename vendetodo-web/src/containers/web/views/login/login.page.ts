import { Component, OnInit } from '@angular/core';
import {
  FormControl,
  Validators,
  FormGroup,
  FormBuilder,
} from '@angular/forms';
import { Router } from '@angular/router';
import { ClienteModel } from 'src/containers/dashboard/modelos/ClienteModel';
import { MessagingService } from 'src/utils/messaging.service';
import { Constants } from 'src/utils/constans.utils';
import { LoadingService } from 'src/utils/loading.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  usuarioForm: FormGroup;
  validationMessages = {
    correo: [
      { type: 'required', message: 'Favor de Introducir el Correo' },
      { type: 'pattern', message: 'Formato Invalido' },
    ],
    contra: [
      { type: 'required', message: 'Favor de Introducir la Contraseña' },
    ],
  };

  VENDETODO_SESSION: string;

  constructor(
    private formBuilder: FormBuilder,
    private clienteModel: ClienteModel,
    private router: Router,
    private messaginservice: MessagingService,
    private constants: Constants,
    private loadingService: LoadingService
  ) {
    this.constants = constants;
    this.loadingService = loadingService;
    this.usuarioForm = this.formBuilder.group({
      correo: new FormControl(
        '',
        Validators.compose([
          Validators.required,
          Validators.pattern(
            /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
          ),
        ])
      ),
      contra: new FormControl('', Validators.compose([Validators.required])),
    });

    this.VENDETODO_SESSION = this.constants.getConstants().VENDETODO_SESSION;
  }

  ngOnInit() {}

  logIn() {
    this.loadingService.presentLoading(true, 'Iniciando sesion');

    let valores = this.usuarioForm.value;
    this.clienteModel
      .comprobarUsuario(valores.correo, valores.contra)
      .then((response: any) => {
        if (response.isValid) {
          localStorage.setItem(this.VENDETODO_SESSION, response.data);
          this.loadingService.presentLoading(false, 'Iniciando sesion');
          this.messaginservice.success('Inicio Correcto');
          this.router.navigate(['']);
        } else {
          this.loadingService.presentLoading(false, 'Iniciando sesion');
          this.messaginservice.warning('Correo o contraseña incorrectos');
        }
      })
      .catch((e) => {
        console.log(e);
      });
  }


}

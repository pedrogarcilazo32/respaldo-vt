import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ManejadoraProducto } from 'src/containers/dashboard/modelos/manejadoraProducto';
import { ModeloCarrito } from 'src/containers/dashboard/modelos/ModeloCarrito';
import { Proveedor } from 'src/containers/dashboard/models/Proveedor';
import { MessagingService } from 'src/utils/messaging.service';
import { EventManager } from 'src/utils/event-manager';
import { Storage } from 'src/utils/storage';
import { Constants } from 'src/utils/constans.utils';
import { LoadingService } from 'src/utils/loading.service'


@Component({
  selector: 'app-detalle-producto',
  templateUrl: './detalle-producto.page.html',
  styleUrls: ['./detalle-producto.page.scss'],
})
export class DetalleProductoPage implements OnInit {
  private queryParams: any = {};
  private url: any;

  private producto: any ;
  private loading: boolean = false;
  private arrayProv: any;
  private selectedProv: Proveedor;
  private selectedCant: number = 1;
  private arrayCant: Array<number> = [1, 2, 3, 4, 5]//["1", "2", "3", "4", "5"];
  // private ModeloProducto: ManejadoraProducto;
  // private router: Router;
  constructor(
    private router: Router,
    private ModeloProducto: ManejadoraProducto,
    private ModeloCarrito: ModeloCarrito,
    private messaginService: MessagingService,
    private eventManager: EventManager,
    private storage: Storage,
    private constants: Constants,
    private loadingService: LoadingService
  ) {
    this.ModeloProducto = ModeloProducto;
    this.storage = storage;
    this.loadingService = loadingService;
    this.ModeloCarrito = ModeloCarrito;
    this.messaginService = messaginService;
    this.eventManager = eventManager;
    this.router = router;
    this.url = this.router.parseUrl(this.router.url);
    this.queryParams.id_producto = this.url.queryParams['id-producto'];
    this.selectedProv = null;
    this.constants = constants;
  }

  ngOnInit() {
    this.getDetalleProducto();
    this.getProveedor();
    this.eventManager.on('agregar-carrito', (data:  boolean) => {
      this.loading = data;
    });
  }

  async getDetalleProducto() {
    this.loadingService.presentLoading(true, 'Cargando');
    const detalleProducto: any = await this.ModeloProducto.obtenerDetalleProducto(
      this.queryParams.id_producto
    );
    this.producto = detalleProducto;
    this.loadingService.presentLoading(false, 'Buscando producto');
  }

  async getProveedor() {
    this.arrayProv = await this.ModeloProducto.getProveedor(this.queryParams.id_producto);
  }

  selectProveedor(itemProv){
    this.selectedProv = itemProv;
  }

  cantidadOnChange(itemCant){
    this.selectedCant = itemCant;
  }

  async agregarAlCarrito(){
    const  token = localStorage.getItem(this.constants.getConstants().VENDETODO_SESSION);
    if (token) {
      this.loadingService.presentLoading(true, 'Agregando a carrito');

      let proveedorMayorExistencia;
      if(!this.selectedProv) { // si no selecciona
        const id_producto = this.producto.idProducto;
        proveedorMayorExistencia = await this.ModeloCarrito.getProveedorMayorExistencia(id_producto);
      }

      await this.ModeloCarrito.crearLineaCarrito(
        this.producto,
        this.selectedCant,
        proveedorMayorExistencia || this.selectedProv
      );

      this.loadingService.presentLoading(false, 'Agregando a carrito');
      this.messaginService.success('Producto Agregado con exito');
      // window.location.reload();
    } else {
      this.router.navigateByUrl('/login');
    }
  }
}

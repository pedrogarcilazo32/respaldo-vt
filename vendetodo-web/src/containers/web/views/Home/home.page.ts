import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ManejadoraProducto } from 'src/containers/dashboard/modelos/manejadoraProducto';
import { Producto } from 'src/containers/dashboard/models/Producto';
import { EventManager } from 'src/utils/event-manager';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  private Productos = Array<Producto>();
  private OFERTAS = [
    {
      name: 'banner1',
      src: '../../../assets/frames/ofertas/Frame2.png',
    },
    {
      name: 'banner2',
      src: '../../../assets/frames/ofertas/Frame3.png',
    },
    {
      name: 'banner2',
      src: '../../../assets/frames/ofertas/Frame4.png',
    },
    {
      name: 'banner3',
      src: '../../../assets/frames/ofertas/Frame5.png',
    },
    {
      name: 'banner4',
      src: '../../../assets/frames/ofertas/Frame6.png',
    },
  ];

  private NUEVOS = [
    {
      name: 'banner1',
      src: '../../../assets/frames/nuevos/Frame 2.png',
    },
    {
      name: 'banner2',
      src: '../../../assets/frames/nuevos/Frame 3.png',
    },
    {
      name: 'banner2',
      src: '../../../assets/frames/nuevos/Frame 4.png',
    },
    {
      name: 'banner3',
      src: '../../../assets/frames/nuevos/Frame 5.png',
    },
    {
      name: 'banner4',
      src: '../../../assets/frames/nuevos/Frame 6.png',
    },
  ]

  private MAS_VENDIDOS = [
    {
      name: 'banner1',
      src: '../../../assets/frames/vendidos/Frame 2.png',
    },
    {
      name: 'banner2',
      src: '../../../assets/frames/vendidos/Frame 3.png',
    },
    {
      name: 'banner2',
      src: '../../../assets/frames/vendidos/Frame 4.png',
    },
    {
      name: 'banner3',
      src: '../../../assets/frames/vendidos/Frame 5.png',
    },
    {
      name: 'banner4',
      src: '../../../assets/frames/vendidos/Frame 6.png',
    },
  ]

  isSearching : boolean = false;
  private manejaProductos: ManejadoraProducto;
  private router: Router;
  constructor(manejaProductos: ManejadoraProducto, router: Router, private eventManager: EventManager) {
    this.manejaProductos = manejaProductos;
    this.eventManager = eventManager;
    this.router = router;
  }

  ngOnInit() {
    this.obtenerProductos(this.manejaProductos.Obtenerpagina(0));

    this.eventManager.on('searchProduct', (data : any) => {
      if (data) {
        console.log('recibo', data);
        this.isSearching = true;
        this.Productos = data;
      }
    });
  }

  async obtenerProductos(pagina: number) {
    const data = await this.manejaProductos.obtenerProductos(pagina);
    console.log("home", data);
    if (data.length) {
      this.Productos = data;
    }
  }

  verProducto(item: Producto) {
    const id_producto = item.getIdProducto;
    this.router.navigateByUrl(`detalle-producto?id-producto=${id_producto}`);
  }
}

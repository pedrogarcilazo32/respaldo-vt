import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeCategoriePage } from './home-categorie.page';

const routes: Routes = [
  {
    path: '',
    component: HomeCategoriePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomeCategoriePageRoutingModule {}

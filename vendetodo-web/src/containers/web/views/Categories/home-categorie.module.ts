import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HomeCategoriePageRoutingModule } from './home-categorie-routing.module';
import { HomeCategoriePage } from './home-categorie.page';

import { ComponentsModule } from '../../../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HomeCategoriePageRoutingModule,
    ComponentsModule
  ],
  declarations: [HomeCategoriePage]
})
export class HomeCategoriePageModule {}

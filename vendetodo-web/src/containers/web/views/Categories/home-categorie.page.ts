import { Component, OnInit } from '@angular/core';
import { ManejadoraProducto } from 'src/containers/dashboard/modelos/manejadoraProducto';
import { Producto } from '../../../dashboard/models/Producto';
import { Router } from '@angular/router';
import { EventManager } from 'src/utils/event-manager';

@Component({
  selector: 'app-home-categorie',
  templateUrl: './home-categorie.page.html',
  styleUrls: ['./home-categorie.page.scss'],
})
export class HomeCategoriePage implements OnInit {
  url: any;
  queryParams: any = {};
  Productos = Array<Producto>();
  nomCategoria: string;
  array = [1, 2, 3, 4];
  arrayFiltrar: any = [
    {
      title: 'Ofertas',
      options: { label: 'Oferta', prods: [] },
    },
    {
      title: 'Lo más nuevo',
      options: { label: 'Nuevo', prods: [] },
    },
    {
      title: 'Lo más vendido',
      options: { label: 'Vendido', prods: [] },
    },
    {
      title: 'Todo',
      options: { label: 'Todo', prods: [] },
    },
  ];

  constructor(
    private modeloProducto: ManejadoraProducto,
    private router: Router,
    private eventManager: EventManager
  ) {
    this.modeloProducto = this.modeloProducto;
    this.eventManager = eventManager;
    this.router = router;
    this.Productos = [];
    console.log("estoy en ", this.nomCategoria);
  }

  async ngOnInit() {
    this.url = this.router.parseUrl(this.router.url);
    this.queryParams.idCategoria = this.url.queryParams['idCat'];
    this.queryParams.nomCategoria = this.url.queryParams['nomCat'];
    await this.cargarDatosCategoria();
    console.log("noestoy en ", this.nomCategoria);
  }

  public async cargarDatosCategoria() {
    this.nomCategoria = this.queryParams.nomCategoria;
    //Obtener productos news

    await this.obtenerProductosNews();
    //Obtener todos los productos
    await this.obtenerProductos();
  }

  async obtenerProductos() {
    const data = await this.modeloProducto.obtenerProductosPorCategoria(
      this.queryParams.idCategoria
    );

    console.log("cata", data);
    if (data.length) {
      this.arrayFiltrar[this.arrayFiltrar.length - 1].options.prods = data;
    }
  }

  async obtenerProductosNews() {
    const data = await this.modeloProducto.obtenerProductosPorCategoriaNews(
      this.queryParams.idCategoria
    );
    if (data.length) {
      this.Productos = data;
      //Seccionar array
      this.Productos.forEach((prod) => {
        for (let i = 0; i < this.arrayFiltrar.length; i++) {
          if (prod.getEstatus === this.arrayFiltrar[i].options.label) {
            this.arrayFiltrar[i].options.prods.push(prod);
          }
        }
      });
    }
  }

  verProducto(item: Producto) {
    const id_producto = item.getIdProducto;
    this.router.navigateByUrl(`detalle-producto?id-producto=${id_producto}`);
  }
}

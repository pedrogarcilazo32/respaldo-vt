import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { ModeloCarrito } from 'src/containers/dashboard/modelos/ModeloCarrito';
import { LineaCarrito } from 'src/containers/dashboard/models/LineaCarrito';
import { Storage } from 'src/utils/storage';
import { Pago } from 'src/containers/dashboard/models/Pago';
import { LoadingService } from 'src/utils/loading.service';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.page.html',
  styleUrls: ['./payment.page.scss'],
})
export class PaymentPage implements OnInit {
  private LineasCarrito = new Array<LineaCarrito>();
  private cantidadProductos: number = 0;
  private totalCarrito: number = 0;
  private viewPago: boolean = false;
  private formGroup: FormGroup;
  private datosPago: Pago;

  constructor(
    private storage: Storage,
    private modeloCarrito: ModeloCarrito,
    private formBuilder: FormBuilder,
    private loadingService: LoadingService
  ) {
    this.loadingService = loadingService;
    this.modeloCarrito = modeloCarrito;
    this.storage = storage;
    this.LineasCarrito = [];
    this.formBuilder = formBuilder;
    this.formGroup = this.formBuilder.group({
      creditCard: new FormControl(
        '',
        Validators.compose([Validators.required, Validators.pattern('/[^d]/g')])
      ),
      nombreApellido: new FormControl(
        '',
        Validators.compose([Validators.required, Validators.pattern('/[^A-ZÑ a-zñ0-9]/g')])
      ),
      fechaExp: new FormControl(
        '',
        Validators.compose([Validators.required, Validators.pattern('/[^d]/g')])
      ),
      cvv: new FormControl(
        '',
        Validators.compose([Validators.required, Validators.pattern('/[^d]/g')])
      ),
    });
  }

  ngOnInit() {
    this.mostrarCarrito();
  }

  mostrarCarrito() {
    this.LineasCarrito = this.modeloCarrito.getLineasCarrito();
    this.cantidadProductos = this.LineasCarrito.length;
    this.totalCarrito = this.modeloCarrito.obtenerTotal();
  }


  ingresarDatosPago() {
    this.viewPago = true;
  }

  validarInput(e: any, type) {
    let regex: RegExp;
    if(type=== 'number'){
      regex = /[^\d]/g;
    } else {
      regex = /[^A-ZÑ a-zñ0-9]/g;
    }

    if (!regex.test(e.target.value)) {
      return true;
    }

    return false;
  }

  guardarDatosDePago(){
    this.viewPago = false;
    let valores = this.formGroup.value;
    this.datosPago = this.modeloCarrito.guardarDatosPago(valores);
  }

  async confirmarVenta() {
    if(this.datosPago){
      this.loadingService.presentLoading(true, 'Procesando Compra');
      await this.modeloCarrito.pagar(this.datosPago);
      this.loadingService.presentLoading(false, 'Procesando Compra');
    }

  }
}

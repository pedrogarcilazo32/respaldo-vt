import { Component, OnInit } from '@angular/core';
import { LineaCarrito } from 'src/containers/dashboard/models/LineaCarrito';
import { ModeloCarrito } from 'src/containers/dashboard/modelos/ModeloCarrito';
import { Router } from '@angular/router';
import { LoadingService } from 'src/utils/loading.service';


@Component({
  selector: 'app-carrito',
  templateUrl: './carrito.page.html',
  styleUrls: ['./carrito.page.scss'],
})
export class CarritoPage implements OnInit {
  private LineasCarrito = new Array<LineaCarrito>();
  private cantidad: number = 0;
  private totalCarrito: number = 0;

  constructor(private modeloCarrito: ModeloCarrito, private router: Router, private loadingService: LoadingService) {
    this.loadingService = loadingService;
    this.router = router;
    this.modeloCarrito = modeloCarrito;
    this.LineasCarrito = [];
  }
  ngOnInit() {
    this.mostrarCarrito();
  }

  public async mostrarCarrito() {
    this.LineasCarrito = this.modeloCarrito.getLineasCarrito();
    this.totalCarrito = this.modeloCarrito.obtenerTotal();
  }

  aumentarCantidad(LC) {
    this.LineasCarrito.forEach((fh: LineaCarrito) => {
      if (fh.getIdLineaCarrito === LC.getIdLineaCarrito) {
        const auxCantidad = fh.getCantidad + 1;
        fh.setCantidad = auxCantidad;
      }
    });
  }

  disminuirCantidad(LC) {
    this.LineasCarrito.forEach((fh: LineaCarrito) => {
      if (fh.getIdLineaCarrito === LC.getIdLineaCarrito) {
        if(LC.getCantidad > 1){
          const auxCantidad = fh.getCantidad - 1;
          fh.setCantidad = auxCantidad;
        }
      }
    });
  }

  async eliminarProducto(LineaCarrito: LineaCarrito) {
    this.loadingService.presentLoading(true, 'Eliminando producto');
    await this.modeloCarrito.eliminarProducto(LineaCarrito.getIdLineaCarrito);
    location.reload();
    this.loadingService.dismiss();
  }

  pagar(){
    this.router.navigateByUrl('/payment');
  }

}

import { Injectable } from '@angular/core';
import { Constants } from '../../../utils/constans.utils';
import { Producto } from '../../dashboard/models/Producto';
import { filtroProducts } from '../../dashboard/models/filtroProducts.model';
@Injectable({ providedIn: 'root' })
export class productService {
  constructor(private constants: Constants) {}
  public url = this.constants.getUrl();

  public async getProducts(pagina: number): Promise<Array<Producto>> {
    const params = {
      pagina: 1,
    };

    return new Promise(async (resolve) => {
      const arrayProductos = Array<Producto>();

      const response = await fetch(this.url + '/api/products', {
        body: JSON.stringify(params),
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
      });

      const respon = await response.json();
      console.log(respon);

      if (respon.isValid) {
        respon.data.forEach((fh: any) => {
          const aux = new Producto(
            fh.nombre,
            fh.descripcion,
            fh.precio,
            fh.volumen,
            fh.id_categoria,
            fh.id_marca,
            fh.image_url,
            fh.id_producto
          );

          arrayProductos.push(aux);
        });

        console.log('array', arrayProductos)
        resolve(arrayProductos);
      }
    });
  }

  public buscarProducto(filtro: filtroProducts) {
    return new Promise(async (resolve, reject) => {
      const response = await fetch(this.url + '/api/products/buscarProducto', {
        body: JSON.stringify(filtro),
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
      });

      if (response.status === 200) {
        const data = await response.json();
        if (data) {
          resolve(data);
        }
      } else {
        reject('No se pudo borrar el Producto');
      }
    });
  }
}




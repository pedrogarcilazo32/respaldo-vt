import { Injectable } from '@angular/core';
import { ReplaySubject } from 'rxjs';

@Injectable()
export class ComunicacionService {

  private mensajero = new ReplaySubject<string>(1)

  constructor() { }

  public get recibir() {
    return this.mensajero.asObservable();
  }

  public enviar(name: string): void {
    this.mensajero.next(name);
  }
}

import { Injectable } from '@angular/core';

@Injectable()
export class Storage {
  public dataSource = {};

  save(name: string, data = null) {
    this.dataSource[name] = data;
    // const callbacksIDs = Object.keys(this.dataSource[event]);

    // callbacksIDs.forEach((callbackID) => {
    //   const callback = this.dataSource[event][callbackID];
    //   callback(data);
    // });
  }

  clean(name: string) {
    if (this.dataSource[name]) {
      delete this.dataSource[name];
    }
  }

  getData(name: string) {
    if (this.dataSource[name]) {
      return this.dataSource[name];
      // const callbacksIDs = Object.keys(this.dataSource[event]);

      // callbacksIDs.forEach((callbackID) => {
      //   const callback = this.dataSource[event][callbackID];
      //   callback(data);
      // });
    }
  }
}


const VENDETODO_API = 'http://localhost:3001';


import { Injectable } from '@angular/core';
@Injectable({
  providedIn: 'root',
})
export class Constants {
  getUrl(){
    return VENDETODO_API;
  }

  getConstants() {
    return {
      VENDETODO_API: 'http://localhost:3001',
      VENDETODO_SESSION: 'vendetodo-session'
    };
  }
}

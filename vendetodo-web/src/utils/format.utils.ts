/* eslint-disable no-trailing-spaces */
/* eslint-disable curly */
/* eslint-disable no-var */
/* eslint-disable one-var */
import { Injectable } from '@angular/core';
@Injectable({
  providedIn: 'root',
})

export class FormatUtils {
  formatDate(date) {
    var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
  }

  formatDateD(date) {
    var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('/');
  }

  formatDateMXN(date) {
    var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [day, month, year].join('/');
  }

  formatDateArchivos(date) {
    var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('_');
  }

  getHora(date) {
    var d = new Date(date),
      hour = '' + d.getHours();

    if (hour.length < 2) hour = '0' + hour;

    return hour;
  }

  getMinutos(date) {
    var d = new Date(date),
      minutes = '' + d.getMinutes();

    if (minutes.length < 2) minutes = '0' + minutes;

    return minutes;
  }
  getSegundos(date) {
    var d = new Date(date),
      seconds = '' + d.getSeconds();
    if (seconds.length < 2) seconds = '0' + seconds;
    return seconds;
  }

  getHora24(date) {
    var time = new Date(date);
    var hour = '';
    hour = time.toLocaleString('en-US', {
      hour: 'numeric',
      minute: '2-digit',
      second: '2-digit',
      hour12: true,
    });

    return hour;
  }

  getDia(date) {
    var d = new Date(date),
      day = '' + d.getDate();

    if (day.length < 2) day = '0' + day;

    return day;
  }

  getMes(date) {
    var d = new Date(date),
      month = '' + (d.getMonth() + 1);

    if (month.length < 2) month = '0' + month;

    return month;
  }

  getAnio(date) {
    var d = new Date(date),
      year = d.getFullYear();

    return year;
  }

  getNombreDia(date) {
    var d = new Date(date),
      month = d.getDay() + 1;

    if (month == 1) return 'Domingo';
    if (month == 2) return 'Lunes';
    if (month == 3) return 'Martes';
    if (month == 4) return 'Miércoles';
    if (month == 5) return 'Jueves';
    if (month == 6) return 'Viernes';
    if (month == 7) return 'Sábado';
  }

  getNombreMesCorto(date) {
    var d = new Date(date),
      month = d.getMonth() + 1;

    if (month == 1) return 'Ene';
    if (month == 2) return 'Feb';
    if (month == 3) return 'Mar';
    if (month == 4) return 'Abr';
    if (month == 5) return 'May';
    if (month == 6) return 'Jun';
    if (month == 7) return 'Jul';
    if (month == 8) return 'Ago';
    if (month == 9) return 'Sep';
    if (month == 10) return 'Oct';
    if (month == 11) return 'Nov';
    if (month == 12) return 'Dic';
  }

  getNombreMes(date) {
    var d = new Date(date),
      month = d.getMonth() + 1;

    if (month == 1) return 'Enero';
    if (month == 2) return 'Febrero';
    if (month == 3) return 'Marzo';
    if (month == 4) return 'Abril';
    if (month == 5) return 'Mayo';
    if (month == 6) return 'Junio';
    if (month == 7) return 'Julio';
    if (month == 8) return 'Agosto';
    if (month == 9) return 'Septiembre';
    if (month == 10) return 'Octubre';
    if (month == 11) return 'Noviembre';
    if (month == 12) return 'Diciembre';
  }

  getPrimerDiaMes(date) {
    return new Date(date.getFullYear(), date.getMonth(), 1);
  }

  getUltimoDiaMes(date) {
    return new Date(date.getFullYear(), date.getMonth() + 1, 0);
  }

  acomodaDecimales(event) {
    return event.toLocaleString('en-US', {
      minimumFractionDigits: 2,
      maximumFractionDigits: 2,
    });
  }

  acomodaDecimalesM(event) {
    let num = event / 1000000;
    let numF = num.toLocaleString('en-US', {
      minimumFractionDigits: 2,
      maximumFractionDigits: 2,
    });
    return numF + ' M';
  }
}

import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes, CanActivate } from '@angular/router';
import { SkeletonComponent } from '../containers/web/layout/skeleton/skeleton.component';
import { SkeletonDashboardComponent } from 'src/containers/dashboard/layout/skeleton-dashboard/skeleton-dashboard.component';
import { SkeletonBlankComponent } from 'src/containers/web/layout/skeleton-blank/skeleton-blank.component';
import { AuthGuard } from './guards/auth.guard';
import { RoleGuard } from './guards/role.guard';
import { NoauthGuard } from './guards/noauth.guard';

const routes: Routes = [
  {
    //AuthGuard permite entrar si esta logeado y si no lo manda a logearse
    //RoleGuard permite entrar si esta logeado y aparte tiene un rol
    //NoAuthGuard no permite entrar a los logeados y te manda al home

    path: '',
    component: SkeletonComponent,
    children: [
      {
        path: '',
        loadChildren: () =>
          import('../containers/web/views/home/home.module').then(
            (m) => m.HomePageModule
          ),
      },
      {
        path: 'detalle-producto',
        loadChildren: () =>
          import(
            '../containers/web/views/detalle-producto/detalle-producto.module'
          ).then((m) => m.DetalleProductoPageModule),
      },
      {
        path: 'carrito',
        canActivate: [AuthGuard],
        loadChildren: () =>
          import('../containers/web/views/carrito/carrito.module').then(
            (m) => m.CarritoPageModule
          ),
      },
      {
        path: 'categorie',
        loadChildren: () =>
          import(
            '../containers/web/views/Categories/home-categorie.module'
          ).then((m) => m.HomeCategoriePageModule),
      },
      {
        path: 'payment',
        canActivate: [AuthGuard],
        loadChildren: () =>
          import('../../src/containers/web/views/payment/payment.module').then(
            (m) => m.PaymentPageModule
          ),
      },
    ],
  },
  {
    path: 'dashboard',
    component: SkeletonDashboardComponent,
    children: [
      {
        path: 'catalogo-productos',
        canActivate: [RoleGuard],
       data: { expectedRole: 'admin' },
        loadChildren: () =>
          import(
            '../containers/dashboard/views/catalogo-productos/catalogo-productos.module'
          ).then((m) => m.CatalogoProductosPageModule),
      },
      {
        path: 'ordenar-estante',
        //canActivate: [RoleGuard],
        //data: { expectedRole: 'estantero' },
        loadChildren: () =>
          import(
            '../containers/dashboard/views/ordenar-estante/ordenar-estante.module'
          ).then((m) => m.OrdenarEstantePageModule),
      },
      {
        path: 'catalogo-empleados',
       canActivate: [RoleGuard],
        data: { expectedRole: 'admin' },
        loadChildren: () =>
          import(
            '../containers/dashboard/views/catalogo-empleados/catalogo-empleados.module'
          ).then((m) => m.CatalogoEmpleadosPageModule),
      },
      {
        path: 'surtir-orden',
        loadChildren: () =>
         import('../containers/dashboard/views/surtir-orden/surtir-orden.module').then( (m) => m.SurtirOrdenPageModule)
      },
      {
        path: 'historial-ordenes',
        loadChildren: () => import('../containers/dashboard/views/historial-ordenes/historial-ordenes.module').then( m => m.HistorialOrdenesPageModule)
      },
      {
        path: 'historial-estante',
        loadChildren: () => import('../containers/dashboard/views/historial-estante/historial-estante.module').then( m => m.HistorialEstantePageModule)
      },
    ],
  },
  {
    path: 'login',
    component: SkeletonBlankComponent,
    canActivate: [NoauthGuard],
    children: [
      {
        path: '',
        loadChildren: () =>
          import('../containers/web/views/login/login.module').then(
            (m) => m.LoginPageModule
          ),
      },
    ],
  },
  {
    path: 'signup',
    component: SkeletonBlankComponent,
    canActivate: [NoauthGuard],
    children: [
      {
        path: '',
        loadChildren: () =>
          import('../containers/web/views/signup/signup.module').then(
            (m) => m.SignupPageModule
          ),
      },
    ],
  },






];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      preloadingStrategy: PreloadAllModules,
      useHash: true,
    }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}

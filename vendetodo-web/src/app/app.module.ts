import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { MatTooltipModule } from '@angular/material/tooltip';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { SkeletonComponent } from '../containers/web/layout/skeleton/skeleton.component';
import { NavigationComponent } from '../containers/web/layout/navigation/navigation.component';
import { FooterComponent } from '../containers/web/layout/footer/footer.component';
import { ComponentsModule } from '../components/components.module';
import { CommonModule } from '@angular/common';
import { SkeletonDashboardComponent } from 'src/containers/dashboard/layout/skeleton-dashboard/skeleton-dashboard.component';
import { NavigationDashboardComponent } from 'src/containers/dashboard/layout/navigation-dashboard/navigation-dashboard.component';
import { SidebarComponent } from 'src/containers/web/layout/sidebar/sidebar.component';
import { ComunicacionService } from 'src/utils/comunicacion.service';
import { EventManager } from 'src/utils/event-manager';
import { SideMenuComponent } from 'src/containers/dashboard/layout/side-menu/side-menu.component';
import { SkeletonBlankComponent } from 'src/containers/web/layout/skeleton-blank/skeleton-blank.component';
import { FormsModule } from '@angular/forms';
import { JwtHelperService } from '@auth0/angular-jwt';
import { JWT_OPTIONS } from '@auth0/angular-jwt';
import { Storage } from 'src/utils/storage';


@NgModule({
  declarations: [AppComponent, SkeletonComponent, NavigationComponent, FooterComponent, SkeletonDashboardComponent, NavigationDashboardComponent, SidebarComponent, SideMenuComponent, SkeletonBlankComponent],
  imports: [BrowserModule, IonicModule.forRoot(), AppRoutingModule,MatTooltipModule, ComponentsModule, CommonModule,FormsModule],
  providers: [{ provide: RouteReuseStrategy, useClass: IonicRouteStrategy }, ComunicacionService, EventManager,Storage, JwtHelperService,{provide: JWT_OPTIONS, useValue: JWT_OPTIONS}],
  bootstrap: [AppComponent],
})
export class AppModule {}

import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
  UrlTree,
} from '@angular/router';
import jwtDecode from 'jwt-decode';
import { Observable } from 'rxjs';
import { clienteService } from 'src/containers/dashboard/services/cliente.services';
import decode from 'jwt-decode';
import { Constants } from 'src/utils/constans.utils';

@Injectable({
  providedIn: 'root',
})
export class RoleGuard implements CanActivate {
  constructor(
    private clienteService: clienteService,
    private router: Router,
    private constants: Constants
  ) {
    this.constants = constants;
  }
  canActivate(route: ActivatedRouteSnapshot): boolean {
    const VENDETODO_SESSION = this.constants.getConstants().VENDETODO_SESSION;

    const expectedRole = route.data.expectedRole;
    const token = localStorage.getItem(VENDETODO_SESSION);
    const data = decode(token);
    const { email, nombre, contra } = data[0];
    console.log(email, nombre, contra)

    if (!this.clienteService.isAuth() || (nombre !== expectedRole && nombre !== 'cliente') ) {
      console.log('usuario no autorizado para la vista');
      return false;
    }

    return true;
  }
}

import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { clienteService } from 'src/containers/dashboard/services/cliente.services';

@Injectable({
  providedIn: 'root'
})
export class NoauthGuard implements CanActivate {
  constructor(
    private clienteService: clienteService,
    private router: Router,
  ){}

  canActivate(): boolean{
    //Para comprobar que esta logueadoo
    if(this.clienteService.isAuth()){
      this.router.navigate(['']);
      return false;
    }
    
    return true;
  }
  
}

import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { clienteService } from 'src/containers/dashboard/services/cliente.services';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(
    private clienteService: clienteService,
    private router: Router,
  ){}

  canActivate(): boolean{
    //Para comprobar que esta logueadoo
    if(!this.clienteService.isAuth()){
      this.router.navigate(['/login']);
      return false;
    }

    return true;
  }
}

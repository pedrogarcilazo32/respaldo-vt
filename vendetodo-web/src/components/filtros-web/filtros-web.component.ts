import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-filtros-web',
  templateUrl: './filtros-web.component.html',
  styleUrls: ['./filtros-web.component.scss'],
})
export class FiltrosWebComponent implements OnInit {

  nombreFiltros = ["Marcas", "Precio", "Proveedor"];
  arrayMarcas = ["Adidas", "Jordan", "Nike", "wtf"];
  arrayprecios = ["1", "2", "3"];
  arrayProveedores = ["José", "María", "Morelos", "Y", "Pavón"];
  arrayFiltros = [this.arrayMarcas, this.arrayprecios, this.arrayProveedores];

  arrayList: any = [
    {
      title: 'Marcas',
      options: [
        { id: 1, label: "Adidas", value: 'adidas' },
        { id: 2, label: "Aeropostale", value: 'adidas' },
        { id: 3, label: "Jordan" ,value: 'adidas' },
        { id: 4, label: "Nike" , value: 'adidas'},
        { id: 5, label: "Reebok", value: 'adidas' }
      ]
    },
    {
      title: 'Precios',
      options: [
        { id: 1, label: "0 - 500" , value: 'adidas'},
        { id: 2, label: "0 - 500 ", value: 'adidas' },
        { id: 3, label: "0 - 500 ", value: 'adidas' },
        { id: 4, label: "0 - 500 ", value: 'adidas' },
        { id: 5, label: "0 - 500 ", value: 'adidas' }
      ]
    },
    {
      title: 'Proveedores',
      options: [
        { id: 1, label: "José", value: 'adidas' },
        { id: 2, label: "María", value: 'adidas'},
        { id: 3, label: "Morelos", value: 'adidas' },
        { id: 4, label: "Y", value: 'adidas' },
        { id: 5, label: "Pavón", value: 'adidas' }
      ]
    }
  ];

  constructor() {
    console.log("sisoy", this.arrayFiltros);
  }

  ngOnInit() {}

}

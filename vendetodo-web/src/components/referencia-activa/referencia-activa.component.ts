import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-referencia-activa',
  templateUrl: './referencia-activa.component.html',
  styleUrls: ['./referencia-activa.component.scss'],
})
export class ReferenciaActivaComponent implements OnInit {
  @Input() categorieNav: string;
  @Input() currentProduct: string;
  @Input() idCat: number;
  homeNav: string;

  constructor(private router: Router) {
    this.homeNav = "Home";
    this.router = router;
  }

  ngOnInit() {}

  public goToCategorie(idCat: number, nomCat:string){
    this.router.navigateByUrl(`/categorie?idCat=${idCat}&nomCat=${nomCat}`);
  }

  public goToHome(){
    this.router.navigate(['/']);
  }


}

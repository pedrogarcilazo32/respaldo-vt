import { Component, OnInit } from '@angular/core';
import { ManejadoraProducto } from 'src/containers/dashboard/modelos/manejadoraProducto';
import { EventManager } from 'src/utils/event-manager';
import { LoadingService } from 'src/utils/loading.service';

@Component({
  selector: 'app-buscador',
  templateUrl: './buscador.component.html',
  styleUrls: ['./buscador.component.scss'],
})
export class BuscadorComponent implements OnInit {
  Categories : Array<any> = [
    { value: 'all', label: 'Todas las categorias'},
    { value: 'deportes', label: 'Deportes'},
    { value: 'tecnologia', label: 'Tecnologia'},
    { value: 'electrodomesticas', label: 'Electrodomesticas'}];;

  searchProduct: string;
  selected :any= this.Categories[0]

  constructor(private modeloProducto: ManejadoraProducto, private eventManager: EventManager, private loadingService: LoadingService) {
    this.modeloProducto = modeloProducto;
    this.loadingService = loadingService;
    this.eventManager = eventManager;
  }

  ngOnInit() {
  }

  async buscarProducto() {
    this.loadingService.presentLoading(true, 'Buscando producto');
    const data = await this.modeloProducto.buscarProducto({ search: this.searchProduct, category: this.selected.value });
    this.loadingService.presentLoading(false, 'Buscando producto');
    this.eventManager.emit('searchProduct', data);
  }

  getCategories() {
    const categories = [
      { value: 'deportes', label: 'Deportes' },
      { value: 'tecnologia', label: 'Tecnologia' },
      { value: 'electrodomesticas', label: 'Electrodomesticas' },
    ];

    return categories;
  }

  inputOnChange(item : any){
    this.selected = item;
  }
}

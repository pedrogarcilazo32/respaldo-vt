import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-product-card',
  templateUrl: './product-card.component.html',
  styleUrls: ['./product-card.component.scss'],
})
export class ProductCardComponent implements OnInit {
  @Input() product: any;
  Categories: [
    { value: 'deportes', label: 'Deportes'},
    { value: 'tecnologia', label: 'Tecnologia'},
    { value: 'electrodomesticas', label: 'Electrodomesticas'}];

  constructor() {}

  ngOnInit() {
    //console.log(this.product);
  }
}

import { Component, OnInit } from '@angular/core';
import { Producto } from 'src/containers/dashboard/models/Producto.model';
import { Constants } from 'src/utils/constans.utils';
import { ModalController } from '@ionic/angular';
import { ManejadoraProvedor } from 'src/containers/dashboard/modelos/manejadoraProvedor';
import { Proveedor } from '../../containers/dashboard/models/Proveedor';
import { LoadingService } from 'src/utils/loading.service';
import { MessagingService } from 'src/utils/messaging.service';
import { Batch } from 'src/containers/dashboard/models/batch.model';
import { ManejadoraBatch } from '../../containers/dashboard/modelos/manejadoraBatch';
import { FormatUtils } from 'src/utils/format.utils';

@Component({
  selector: 'app-aumentar-existencia',
  templateUrl: './aumentar-existencia.component.html',
  styleUrls: ['./aumentar-existencia.component.scss'],
})
export class AumentarExistenciaComponent implements OnInit {
  productoEdita: Producto;
  item1: any = '../../../assets/img/prodDefault.png';
  imagenSeleccionada: boolean;
  supplier_id: number = 0;
  supplierSelec: Proveedor;
  arraySuppliers = Array<Proveedor>();
  quantity: number = 0;
  arrayBatches = Array<Batch>();
  constructor(
    private constants: Constants,
    private modalController: ModalController,
    private ManejadoraProvedor: ManejadoraProvedor,
    private loadingService: LoadingService,
    private messaginService: MessagingService,
    private ManejadoraBatch: ManejadoraBatch,
    private format: FormatUtils
  ) {}

  ngOnInit() {
    if (this.productoEdita.image_url) {
      this.item1 = `${this.constants.getUrl()}${this.productoEdita.image_url}`;
      this.imagenSeleccionada = true;
    }
    this.getSuppliers();

    console.log(this.productoEdita);
  }
  cerrarSinData() {
    this.modalController.dismiss(false);
  }
  async getSuppliers() {
    const data = await this.ManejadoraProvedor.getSuppliers();
    if (data.length) {
      this.arraySuppliers = data;
      this.getProductsBatches();
    } else {
      this.messaginService.warning('No se encontraron proveedores');
    }
  }
  async getProductsBatches() {
    const data = await this.ManejadoraBatch.getProductsBatches(
      this.productoEdita
    );
    if (data.length) {
      this.arrayBatches = data;
    }
    console.log(this.arrayBatches);
  }
  comprobarSupplier(event: any) {
    this.supplier_id = Number(event.target.value);
  }
  saveBatch() {
    var batch = new Batch(
      0,
      this.supplier_id,
      this.productoEdita.idProducto,
      this.quantity
    );
    console.log(batch);
    this.ManejadoraBatch.addBatch(batch)
      .then((response: any) => {
        if (response.isValid) {
          this.messaginService.success('Lote registrado con exito!');
          this.reload();
        } else {
          this.messaginService.warning('No se pudo registrar el Lote');
        }
      })
      .catch((e) => {
        // arrayproductos = [];
        // console.log('erros =>>', e);
      });
  }

  formatearFecha(date: string) {
    return this.format.formatDateD(date) + ' ' + this.format.getHora24(date);
  }

  reload() {
    this.quantity = 0;
    this.supplier_id = 0;
    this.getProductsBatches();
  }
}

import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { ModalController } from '@ionic/angular';
import {
  FormControl,
  Validators,
  FormGroup,
  FormBuilder,
} from '@angular/forms';
import { Producto } from '../../containers/dashboard/models/Producto.model';
import { FormatUtils } from 'src/utils/format.utils';
import { marca } from '../../containers/dashboard/models/marca.models';
import { marcasService } from '../../containers/dashboard/services/marcas.services';
import { LoadingService } from 'src/utils/loading.service';
import { MessagingService } from 'src/utils/messaging.service';
import { manejadoraMarca } from '../../containers/dashboard/modelos/manejadoraMarca';
import { ManejadoraProducto } from '../../containers/dashboard/modelos/manejadoraProducto';
import { ValidatorsUtils } from 'src/utils/validators.utils';
import { DomSanitizer } from '@angular/platform-browser';
import { dataFile } from 'src/containers/dashboard/models/dataFile.model';
import { Constants } from 'src/utils/constans.utils';
import { categoria } from 'src/containers/dashboard/models/categoria.model';
import { manejadoraCategorias } from 'src/containers/dashboard/modelos/manejadoraCategories';

@Component({
  selector: 'app-agregar-Producto',
  templateUrl: './agregar-Producto.page.html',
  styleUrls: ['./agregar-Producto.page.scss'],
})
export class AgregarProductoPage implements OnInit {
  productoForm: FormGroup;
  arrayMarcas = new Array<marca>();
  arrayCategorias = new Array<categoria>();
  marcaSelec: number = 0;
  categoriaSelec: number = 0;
  edita: boolean;
  imagenSeleccionada: boolean;
  @ViewChild('file', { static: false }) inputFileListado: ElementRef;
  base64AvatarString: string;

  file: any;
  fileSeleccionado = {
    name: '',
    type: '',
  };
  fileBase64: string;
  dataFile: dataFile = {
    nombre: '',
    archivo: '',
    tipo: '',
    extencion: '',
  };
  item1: any = '../../../assets/img/prodDefault.png';
  //imageDefault: string = '../../../assets/img/prodDefault.png';

  productoEdita: Producto;
  validationMessages = {
    nombre: [{ type: 'required', message: 'El nombre es requerido' }],
    marca: [{ type: 'required', message: 'La marca es requerida' }],
    precio: [
      { type: 'required', message: 'El precio es requerido' },
      { type: 'pattern', message: 'El formato no coincide' },
    ],
    volumen: [
      { type: 'required', message: 'El volumen es requerido' },
      { type: 'pattern', message: 'El formato no coincide' },
    ],
    descripcion: [{ type: 'required', message: 'La descripción es requerida' }],
    categoria: [{ type: 'required', message: 'La categoria es requerida' }],
  };

  constructor(
    private modalController: ModalController,
    private formBuilder: FormBuilder,
    private format: FormatUtils,
    private constants: Constants,
    private marcasService: marcasService,
    private loadingService: LoadingService,
    private messaginService: MessagingService,
    private manejadoraMarca: manejadoraMarca,
    private ManejadoraProducto: ManejadoraProducto,
    private ValidatorsUtils: ValidatorsUtils,
    private sanitizer: DomSanitizer,
    private manejadoraCategorias: manejadoraCategorias
  ) {
    this.modalController = modalController;
    this.formBuilder = formBuilder;
    this.format = format;
    this.constants = constants;
    this.marcasService = marcasService;
    this.loadingService = loadingService;
    this.messaginService = messaginService;
    this.manejadoraCategorias = manejadoraCategorias;
    this.manejadoraMarca = manejadoraMarca;
    this.ManejadoraProducto = ManejadoraProducto;
    this.ValidatorsUtils = ValidatorsUtils;
    this.sanitizer = sanitizer;
    this.productoForm = this.formBuilder.group({
      nombre: new FormControl('', Validators.compose([Validators.required])),
      marca: new FormControl('', Validators.compose([Validators.required])),
      precio: new FormControl(
        '',
        Validators.compose([
          Validators.required,
          Validators.pattern(
            '(0.((0[1-9]{1})|([1-9]{1}([0-9]{1})?)))|(([1-9]+[0-9]*)(.([0-9]{1,2}))?)'
          ),
        ])
      ),
      volumen: new FormControl(
        '',
        Validators.compose([
          Validators.required,
          Validators.pattern(
            '(0.((0[1-9]{1})|([1-9]{1}([0-9]{1})?)))|(([1-9]+[0-9]*)(.([0-9]{1,2}))?)'
          ),
        ])
      ),
      descripcion: new FormControl(
        '',
        Validators.compose([Validators.required])
      ),
      categoria: new FormControl('', Validators.compose([Validators.required])),
    });
  }

  ngOnInit() {
    if (this.edita) {
      if (this.productoEdita.image_url) {
        this.item1 = `${this.constants.getUrl()}${this.productoEdita.image_url}`;
        this.imagenSeleccionada = true;
      }

      this.productoForm = this.formBuilder.group({
        nombre: this.productoEdita.nombreProducto,
        precio: this.productoEdita.precioProducto,
        volumen: this.productoEdita.volumenProducto,
        marca: this.productoEdita.Marca.id,
        descripcion: this.productoEdita.descripcion,
        categoria: this.productoEdita.Categoria.id,
      });
      this.marcaSelec = this.productoEdita.Marca.id;
      this.categoriaSelec = this.productoEdita.Categoria.id;
    }
    this.obtenerMarcas();
  }

  cerrarSinData() {
    this.modalController.dismiss(false);
  }

  agregarActualizarProducto() {
    let valores = this.productoForm.value;

    const aux = {
      id: this.edita ? this.productoEdita.idProducto : 0,
      id_marca: this.marcaSelec,
      nombre: valores.nombre,
      precio: Number(valores.precio),
      volumen: Number(valores.volumen),
      usuario_id: 1,
      descripcion: valores.descripcion,
      id_categoria: this.categoriaSelec,
    };
    var id = this.edita ? this.productoEdita.idProducto : 0;
    var i_usuario_id = 1;
    // let obj = new Producto(
    //   aux.v_nombre,
    //   aux.d_volumen,
    //   aux.d_precio,
    //   aux.i_usuario_id,
    //   aux.id_marca,
    //   aux.id_categoria,
    //   null,
    //   null,
    //   null,
    //   null,
    //   aux.id,
    //   aux.v_descripcion,
    //   null,
    //   this.dataFile
    // );


    // if (!valores.nombre.trim().length) {
    //   this.messaginService.warning('Por favor escriba un nombre');
    //   return;
    // } else if (!valores.descripcion.trim().length) {
    //   this.messaginService.warning('Por favor escriba una descripción');
    //   return;
    // }


    // const marcaObj = this.arrayMarcas.find(({ id }) => id === this.marcaSelec);
    // const categoriaObj = this.arrayCategorias.find(({ id }) => id === this.categoriaSelec);

    // valores.nombre = valores.nombre.trim();
    // if (this.edita) {
    //   this.actualizarProducto(id, valores.nombre, valores.descripcion, Number(valores.volumen),
    //     Number(valores.precio), i_usuario_id, this.dataFile, categoriaObj, marcaObj,this.productoEdita.image_url);
    // } else {
    //   this.guardarProducto(id, valores.nombre, valores.descripcion, Number(valores.volumen),
    //     Number(valores.precio), i_usuario_id, this.dataFile, categoriaObj, marcaObj);
    // }
  }

  // guardarProducto(id, nombre, descripcion, volumen, precio, usuarioId, dataFile, categoria: categoria, marca: marca) {
  //   this.ManejadoraProducto.agregarProductos(id, nombre, descripcion, volumen, precio, usuarioId, dataFile, categoria, marca)
  //     .then((response: any) => {
  //       if (response.isValid) {
  //         this.messaginService.success('Producto guardado con exito!');
  //         this.modalController.dismiss(true);
  //       } else {
  //         this.messaginService.warning('No se pudo guardar el Producto');
  //       }
  //     })
  //     .catch((e) => {
  //       console.log(e);
  //       // arrayproductos = [];
  //       // console.log('erros =>>', e);
  //     });
  // }

  // actualizarProducto(id, nombre, descripcion, volumen, precio, usuarioId, dataFile, categoria: categoria, marca: marca,imagen_url) {
  //   this.ManejadoraProducto.actualizarProductos(id, nombre, descripcion, volumen, precio, usuarioId, dataFile, categoria, marca,imagen_url)
  //     .then((response: any) => {
  //       if (response.isValid) {
  //         this.messaginService.success('Producto actualizado con exito!');
  //         this.modalController.dismiss(true);
  //       } else {
  //         this.messaginService.warning('No se pudo actualizar el Producto');
  //       }
  //     })
  //     .catch((e) => {
  //       // arrayproductos = [];
  //       console.log('erros =>>', e);
  //     });
  // }

  comprobarCategoria(event: any) {
    this.categoriaSelec = Number(event.target.value);
  }

  comprobarMarca(event: any) {
    this.marcaSelec = Number(event.target.value);
  }

  obtenerMarcas() {
    this.manejadoraMarca
      .obtenerMarcas()
      .then((response: any) => {
        if (response.isValid) {
          this.arrayMarcas = response.data;
          console.log('el array marcas',this.arrayMarcas)
          this.obtenerCategorias();
        } else {
          this.messaginService.warning('No se encontraron marcas');
        }
      })
      .catch((e) => {
        // arrayproductos = [];
        console.log('erros =>>', e);
      });
  }

  obtenerCategorias() {
    this.manejadoraCategorias
      .obtenerCategorias()
      .then((response: any) => {
        if (response.isValid) {
          this.arrayCategorias = response.data;
          console.log('el array categorias',this.arrayCategorias)
        } else {
          this.messaginService.warning('No se encontraron categorias');
        }
      })
      .catch((e) => {
        // arrayproductos = [];
        console.log('erros =>>', e);
      });
  }

  soloLetras(e, tipo: string) {
    return this.ValidatorsUtils.soloLetras(e, tipo);
  }

  abrirFoto() {
    this.limpiarInputFile();
    this.inputFileListado.nativeElement.click();
  }

  quitarImagen() {
    this.limpiarInputFile();
  }

  changeFile(item: any) {
    let itemFile = item.target.files[0];
    if (itemFile != null) {
      this.fileSeleccionado.name = itemFile.name;
      this.fileSeleccionado.type = itemFile.type;

      this.base64AvatarString = '';
      this.fileBase64 = '';
      this.file = item.target.files[0];
      if (this.file) {
        const reader = new FileReader();
        reader.onload = this.handleReaderLoaded.bind(this);
        reader.readAsBinaryString(this.file);
        var nombreArray = this.file.name.split('.');
        console.log(nombreArray);
        this.dataFile = {
          nombre: nombreArray[0],
          archivo: '',
          tipo: this.fileSeleccionado.type,
          extencion: '',
        };
      }
    } else {
      this.fileSeleccionado.name = '';
      this.fileSeleccionado.type = '';
    }
  }

  handleReaderLoaded(e) {
    this.base64AvatarString = `data:${this.fileSeleccionado.type};base64,${btoa(
      e.target.result
    )}`;

    console.log(this.base64AvatarString);
    this.dataFile.archivo = this.base64AvatarString;
    //let item2: any;
    let urlSafe = btoa(e.target.result);
    this.item1 = 'data:image/png;base64,' + urlSafe;
    this.sanitizer.bypassSecurityTrustUrl(urlSafe);
    //console.log(item2);
    this.imagenSeleccionada = true;
    this.inputFileListado.nativeElement.value = '';
  }

  limpiarInputFile() {
    this.inputFileListado.nativeElement.value = '';
    this.fileSeleccionado.name = '';
    this.fileSeleccionado.type = '';
    this.file = null;
    this.dataFile.archivo = '';
    this.dataFile.nombre = '';
    this.dataFile.tipo = '';
    this.imagenSeleccionada = false;
    this.item1 = '../../../assets/img/prodDefault.png';

    //this.ruta = '';
  }
}

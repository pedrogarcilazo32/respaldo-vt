import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.scss'],
})
export class CarouselComponent implements OnInit {
  @Input() images: any
  @Input() id: any

  img :string;
  count: number = 0;


  constructor() { }

  ngOnInit() {
    // this.count = this.images.length;
    // this.img = this.images[0].src;

    // console.log('recibi =>>', this.images);
  }

  ngOnChanges() {
    setInterval(() => {
      this.onClickNext();
    }, 3000);
  }

  onClickPrev() {
    const scroll = document.getElementById('containerImages');
    const currentSizeScroll = scroll.scrollLeft;
    const scrollMax = scroll.scrollWidth - scroll.clientWidth;

    if (currentSizeScroll === 0) {
      scroll.scrollLeft = scrollMax;

    } else {
      scroll.scrollLeft -= 1500 ;
    }
  }

  onClickNext() {
    const scroll = document.getElementById('containerImages');
    const currentSizeScroll = scroll.scrollLeft;
    const scrollMax = scroll.scrollWidth - scroll.clientWidth;

    if (currentSizeScroll >= scrollMax) {
      scroll.scrollLeft = 0;
    } else {
      scroll.scrollLeft += 1290;
    }
  }

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { ProductCardComponent } from './product-card/product-card.component';
import { CarouselComponent } from './carousel/carousel.component';
import { FiltrosWebComponent } from './filtros-web/filtros-web.component';
import { ReferenciaActivaComponent } from './referencia-activa/referencia-activa.component';
import { BuscadorComponent } from './buscador/buscador.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  exports: [
    ProductCardComponent,
    CarouselComponent,
    FiltrosWebComponent,
    ReferenciaActivaComponent,
    BuscadorComponent
  ],
  declarations: [
    ProductCardComponent,
    CarouselComponent,
    FiltrosWebComponent,
    ReferenciaActivaComponent,
    BuscadorComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule
  ]
})
export class ComponentsModule { }

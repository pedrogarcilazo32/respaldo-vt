import { Component, OnInit } from '@angular/core';
import {
  FormControl,
  Validators,
  FormGroup,
  FormBuilder,
} from '@angular/forms';
import { Empleado } from '../../containers/dashboard/models/empleado.model';
import { ModalController } from '@ionic/angular';
import { ValidatorsUtils } from '../../utils/validators.utils';
import { Rol } from 'src/containers/dashboard/models/rol';
import { RolModel } from '../../containers/dashboard/modelos/RolModel';
import { MessagingService } from '../../utils/messaging.service';
import { EmpleadoModel } from 'src/containers/dashboard/modelos/EmpleadoModel';
import { Estante } from 'src/containers/dashboard/models/estante.model';
import { EstanteModel } from 'src/containers/dashboard/modelos/EstanteModel';
import { Usuario } from 'src/containers/dashboard/models/usuario';
import { Direccion } from 'src/containers/dashboard/models/direccion';
import { Router } from '@angular/router';
import { modeloEstante } from 'src/containers/dashboard/modelos/modeloEstante';

@Component({
  selector: 'app-agregar-empleado',
  templateUrl: './agregar-empleado.page.html',
  styleUrls: ['./agregar-empleado.page.scss'],
})
export class AgregarEmpleadoPage implements OnInit {
  empleadoForm: FormGroup;
  empleadoEdita: Empleado;
  arrayRol = new Array<Rol>();
  arrayEstantes = new Array<Estante>();
  correoInvalid: boolean = false;
  textoCorreo: string = '';
  edita: boolean;
  rolSelec: string = '';
  estanteSelec: number = 0;
  validationMessages = {
    nombre: [{ type: 'required', message: 'El nombre es requerido' }],
    ape_paterno: [
      { type: 'required', message: 'El apellido paterno es requerido' },
    ],
    ape_materno: [
      { type: 'required', message: 'El apellido materno es requerido' },
    ],
    telefono: [{ type: 'required', message: 'El telefono es requerido' }],
    calle: [{ type: 'required', message: 'La calle es requerida' }],
    rol: [{ type: 'required', message: 'El rol es requerido' }],
    no_exterior: [
      { type: 'required', message: 'El numero exterior es requerido' },
    ],
    ciudad: [{ type: 'required', message: 'La ciudad es requerida' }],
    cp: [{ type: 'required', message: 'El codigo postal es requerido' }],
    correo: [
      { type: 'required', message: 'El correo es requerido' },
      { type: 'pattern', message: 'El correo no es valido' },
    ],
    contra: [{ type: 'required', message: 'La contraseña es requerida' }],
    no_estante: [
      { type: 'required', message: 'El numero de estante es requerido' },
    ],
  };
  constructor(
    private formBuilder: FormBuilder,
    private modalController: ModalController,
    private ValidatorsUtils: ValidatorsUtils,
    private RolModel: RolModel,
    private EstanteModel: modeloEstante,
    private EmpleadoModel: EmpleadoModel,
    private messaginService: MessagingService,
    private router: Router
  ) {
    this.empleadoForm = this.formBuilder.group({
      nombre: new FormControl('', Validators.compose([Validators.required])),
      ape_paterno: new FormControl(
        '',
        Validators.compose([Validators.required])
      ),
      ape_materno: new FormControl(
        '',
        Validators.compose([Validators.required])
      ),
      telefono: new FormControl('', Validators.compose([Validators.required])),

      rol: new FormControl('', Validators.compose([Validators.required])),
      correo: new FormControl(
        '',
        Validators.compose([
          Validators.required,
          Validators.pattern(
            /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
          ),
        ])
      ),
      contra: new FormControl('', Validators.compose([Validators.required])),
      no_estante: new FormControl(
        '',
        Validators.compose([Validators.required])
      ),
    });
  }

  ngOnInit() {
    this.obtenerRoles();
    this.obtenerEstante();
  }

  cerrarSinData() {
    this.modalController.dismiss(false);
  }
  soloLetras(e, tipo: string) {
    return this.ValidatorsUtils.soloLetras(e, tipo);
  }
  comprobarCorreo() {
    if (this.empleadoForm.get('correo').valid) {
      this.EmpleadoModel.validarCorreo(this.textoCorreo).then(
        (response: Array<Usuario>) => {
          if (response.length > 0) {
            this.correoInvalid = true;
          } else {
            this.correoInvalid = false;
          }
        }
      );
    }
  }
  async obtenerRoles() {
    const data = await this.RolModel.obtenerRol();

    if (data.length) {
      this.arrayRol = data;
    }
  }
  async obtenerEstante() {
    const data = await this.EstanteModel.obtenerEstantes();

    if (data.length) {
      this.arrayEstantes = data;
    }
  }
  comprobarEstante(event: any) {
    this.estanteSelec = Number(event.target.value);
  }

  comprobarRol(event: any) {
    this.rolSelec = event.target.value;
  }
  agregarActualizarProducto() {
    let valores = this.empleadoForm.value;

    let objRol = new Rol(null, this.rolSelec);

    let objUsu = new Usuario(
      this.edita ? this.empleadoEdita.Usuario.id_Usuario : 0,
      valores.correo,
      valores.contra,
      objRol
    );
    let objEstante = new Estante(this.estanteSelec, null);
    let objDir = new Direccion(
      null,
      Number(valores.cp),
      valores.calle,
      valores.ciudad,
      valores.no_exterior
    );
    let obj = new Empleado(
      objUsu,
      valores.nombre,
      valores.ape_paterno,
      valores.ape_materno,
      Number(valores.telefono),
      null,
      objDir,
      objEstante
    );
  }
  guardarProducto() {
    let valores = this.empleadoForm.value;
    this.EmpleadoModel.agregarEmpleado(
      valores.correo,
      valores.contra,
      valores.nombre,
      valores.ape_paterno,
      valores.ape_materno,
      valores.telefono,
      valores.cp,
      valores.calle,
      valores.ciudad,
      valores.no_exterior,
      valores.no_estante,
      valores.rol
    )
      .then((response: any) => {
        if (response.isValid) {
          this.messaginService.success('Empleado guardado con exito!');
          this.router.navigate(['/dashboard/catalogo-empleados']);
        } else {
          this.messaginService.warning('No se pudo guardar el Empleado');
        }
      })
      .catch((e) => {
        console.log(e);
      });
  }
  actualizarProducto(empleado: Empleado) {}
}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FiltroProductosPage } from './filtro-productos.page';

const routes: Routes = [
  {
    path: '',
    component: FiltroProductosPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FiltroProductosPageRoutingModule {}

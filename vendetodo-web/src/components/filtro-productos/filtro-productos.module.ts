import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FiltroProductosPageRoutingModule } from './filtro-productos-routing.module';

import { FiltroProductosPage } from './filtro-productos.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FiltroProductosPageRoutingModule
  ],
  declarations: [FiltroProductosPage]
})
export class FiltroProductosPageModule {}

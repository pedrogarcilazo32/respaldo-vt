import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { usuario } from '../../containers/dashboard/models/usuario.model';
import { marca } from '../../containers/dashboard/models/marca.models';
import { manejadoraMarca } from '../../containers/dashboard/modelos/manejadoraMarca';
import { MessagingService } from 'src/utils/messaging.service';
import { FormatUtils } from 'src/utils/format.utils';
import { ModeloUsuario } from '../../containers/dashboard/modelos/ModeloUsuario';
import { filtroProducts } from '../../containers/dashboard/models/filtroProducts.model';
import { manejadoraCategorias } from 'src/containers/dashboard/modelos/manejadoraCategories';
import { categoria } from 'src/containers/dashboard/models/categoria.model';

@Component({
  selector: 'app-filtro-productos',
  templateUrl: './filtro-productos.page.html',
  styleUrls: ['./filtro-productos.page.scss'],
})
export class FiltroProductosPage implements OnInit {
  arrayEmpleados = new Array<usuario>();
  arrayMarcas = new Array<marca>();
  arrayCategorias = new Array<categoria>();
  marca_id: number = 0;
  categoria_id: number = 0;
  empleado_id: number = 0;
  fecha_fin: string = '';
  fecha_inicio: string = '';
  marcaSelect: marca;
  categoriaSelect: categoria;
  empleadoSelect: usuario;
  maxDate: string;
  minDate: string;
  filtro:filtroProducts;
  constructor(
    private modalController: ModalController,
    private manejaMarca: manejadoraMarca,
    private messaginService: MessagingService,
    private format: FormatUtils,
    private ModeloUsuario: ModeloUsuario,
    private manejadoraCategorias: manejadoraCategorias
  ) {
    this.ModeloUsuario = ModeloUsuario;
    this.modalController = modalController;
    this.manejaMarca = manejaMarca;
    this.messaginService = messaginService;
    this.format = format;
    this.manejadoraCategorias = manejadoraCategorias;
  }

  ngOnInit() {
    this.obtenerMarcas();
  }

  cerrarSinData() {
    this.modalController.dismiss(false);
  }

  aplicarFiltros() {
    if (
      this.marca_id == 0 &&
      this.empleado_id == 0 &&
      this.fecha_inicio == '' &&
      this.fecha_fin == '' &&
      this.categoria_id == 0
    ) {
      this.messaginService.warning('Debe indicar al menos un filtro');
      return;
    } else if (
      (this.fecha_inicio != '' && this.fecha_fin == '') ||
      (this.fecha_inicio == '' && this.fecha_fin != '')
    ) {
      this.messaginService.warning('Debe seleccionar ambas fechas');
      return;
    }

    this.empleadoSelect = this.arrayEmpleados.find(
      (e) => e.id == this.empleado_id
    );
    this.marcaSelect = this.arrayMarcas.find((m) => m.id == this.marca_id);
    this.categoriaSelect = this.arrayCategorias.find(
      (c) => c.id == this.categoria_id
    );

    this.filtro = new filtroProducts(
      this.fecha_inicio,
      this.fecha_fin,
      this.empleado_id,
      this.marca_id,
      '',
      this.marca_id == 0 ? '' : this.marcaSelect.v_nombre,
      this.empleado_id == 0 ? '' : this.empleadoSelect.v_nombre,
      this.categoria_id,
      this.categoria_id == 0 ? '' : this.categoriaSelect.v_nombre
    );
    // this.filtro = new filtroProducts(
    //   f_registro_inicio: this.fecha_inicio,
    //   f_registro_fin: this.fecha_fin,
    //   i_usuario_id: this.empleado_id,
    //   id_marca: this.marca_id,
    //   v_nombreEmpleado:
    //     this.empleado_id == 0 ? '' : this.empleadoSelect.v_nombre,
    //   v_nombreMarca: this.marca_id == 0 ? '' : this.marcaSelect.v_nombre,
    //   v_nombreCategoria:
    //     this.categoria_id == 0 ? '' : this.categoriaSelect.v_nombre,
    //   id_categoria: this.categoria_id,
    // )

    this.modalController.dismiss(this.filtro);
  }

  validarFechaFin() {
    this.maxDate = this.fecha_fin;
  }

  validarFechaInicio() {
    this.minDate = this.fecha_inicio;
  }

  obtenerMarcas() {
    this.manejaMarca
      .obtenerMarcas()
      .then((response: any) => {
        if (response.isValid) {
          this.arrayMarcas = response.data;
        } else {
          this.messaginService.warning('No se encontraron marcas');
        }
      })
      .catch((e) => {
        // arrayproductos = [];
        console.log('erros =>>', e);
      });
  }

  obtenerCategorias() {
    this.manejadoraCategorias
      .obtenerCategorias()
      .then((response: any) => {
        if (response.isValid) {
          this.arrayCategorias = response.data;
        } else {
          this.messaginService.warning("No se encontraron categorias");
        }
      })
      .catch((e) => {
        // arrayproductos = [];
        console.log("erros =>>", e);
      });
  }

  // obtenerUsuarios() {
  //   this.manejaUsuario
  //     .obtenerUsuarios()
  //     .then((response: any) => {
  //       if (response.isValid) {
  //         this.arrayEmpleados = response.data;
  //         this.obtenerCategorias();
  //       } else {
  //         this.messaginService.warning('No se encontraron usuarios');
  //       }
  //     })
  //     .catch((e) => {
  //       // arrayproductos = [];
  //       console.log('erros =>>', e);
  //     });
  // }

  comprobarEmpleado(event: any) {
    this.empleado_id = Number(event.target.value);
  }

  comprobarMarca(event: any) {
    this.marca_id = Number(event.target.value);
  }

  comprobarCategoria(event: any) {
    this.categoria_id = Number(event.target.value);
  }
}
